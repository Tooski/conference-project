package View;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import swing.custom.JCustomButton;
import swing.custom.JCustomLabel;
import swing.custom.JCustomTextField;
import swing.custom.JDialogBox;


import db.UserDB;

@SuppressWarnings("serial")
public class CreateAccountGUI extends JPanel {
	/**
	 * The default width for text fields and text areas.
	 */
	private static final int TEXT_WIDTH = 15;

	/**
	 * Main frame.
	 */
	private final JFrame my_frame;

	/**
	 * Username text field.
	 */
	private final JTextField my_username_text;

	/**
	 * Actual name text field.
	 */
	private final JTextField my_actualname_text;

	/**
	 * Password text field.
	 */
	private final JTextField my_password_text;

	/**
	 * A button used to push an element on the Stack.
	 */
	private final JButton my_create_button;
	
	private LoginUI my_ui;

	public CreateAccountGUI(LoginUI ui) {
		super();
		my_frame = new JFrame("Login");
		my_actualname_text = new JCustomTextField(TEXT_WIDTH);
		my_username_text = new JCustomTextField(TEXT_WIDTH);
		my_password_text = new JCustomTextField(TEXT_WIDTH);
		my_create_button = new JCustomButton("Create");
		my_ui = ui;
		setupComponets();
	}

	/**
	 * Helper method to perform the work of setting up the GUI components.
	 */
	private void setupComponets() {
		my_create_button.addActionListener(new CreateListener());
		my_create_button.setMnemonic(KeyEvent.VK_C);
		final JButton cancel_button = new JCustomButton("Cancel");
		cancel_button.setMnemonic(KeyEvent.VK_N);
		cancel_button.addActionListener(new CancelListener());

		final JLabel actualname_label = new JCustomLabel("Actual name: ");
		final JLabel username_label = new JCustomLabel("Username: ");
		final JLabel password_label = new JCustomLabel("Password: ");

		my_actualname_text.setEditable(true);
		my_username_text.setEditable(true);
		my_password_text.setEditable(true);

		final JPanel master_panel = new JPanel(new BorderLayout());
		final JPanel create_panel = new JPanel(new GridLayout (3,2));
		final JPanel username_panel = new JPanel(new FlowLayout());
		final JPanel pass_panel = new JPanel(new FlowLayout());
		final JPanel actualname_panel = new JPanel(new FlowLayout());
		final JPanel button_panel = new JPanel(new BorderLayout());

		username_panel.add(username_label);
		username_panel.add(my_username_text);

		pass_panel.add(password_label);
		pass_panel.add(my_password_text);

		actualname_panel.add(actualname_label);
		actualname_panel.add(my_actualname_text);

		create_panel.add(actualname_panel, BorderLayout.NORTH);
		create_panel.add(username_panel, BorderLayout.CENTER);
		create_panel.add(pass_panel, BorderLayout.SOUTH);

		button_panel.add(my_create_button, BorderLayout.NORTH);
		button_panel.add(cancel_button, BorderLayout.CENTER);

		final JLabel label = new JLabel("Welcome to MSEE program.");
		label.setFont(new Font(label.getFont().getFontName(), Font.PLAIN, 20));

		master_panel.add(label, BorderLayout.NORTH);
		master_panel.add(create_panel, BorderLayout.CENTER);
		master_panel.add(button_panel, BorderLayout.SOUTH);
		add(master_panel);

	}

	/**
	 * Creates and displays the application frame.
	 */
	public void display() {
		// final JFrame frame = new JFrame("Login");
		my_frame.setSize(800, 600);
		my_frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		my_frame.setContentPane(this);

		// frame.pack();
		my_frame.setResizable(true);
		my_frame.setVisible(true);
		my_username_text.grabFocus();
		getRootPane().setDefaultButton(my_create_button);
		my_frame.pack();
		my_frame.setLocationRelativeTo(my_ui);



	}

	/**
	 * An action listener for the create button.
	 */
	private class CreateListener implements ActionListener {
		@SuppressWarnings("static-access")
		public void actionPerformed(final ActionEvent the_event) {
			final String username = my_username_text.getText();
			final String password = my_password_text.getText();
			final String name = my_actualname_text.getText();
			int value = UserDB.createUser(username, password, name);
			if (value == 0) {
				

				if (!JDialogBox.createDialogBox("Account successfully created!", null).isShowing()) {
					my_frame.dispose();
				}
			} else if (value == 1)
			{
				JDialogBox.createDialogBox("Invalid username, name or password", null);
			}
			else {
				JDialogBox.createDialogBox("Username has already been taken", null);
			}
			
		}
	}

	/**
	 * An action listener for the cancel button.
	 */
	private class CancelListener implements ActionListener {
		public void actionPerformed(final ActionEvent the_event) {
			my_frame.dispose();
		}
	}
}
