package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Model.TempConference;
import Model.TempFeedback;

public class FeedbackDB extends AbstractDB {

	static Map<Integer, List<TempFeedback>> my_feedbacks = getFeedback();
	static Map<Integer, TempFeedback> all_feedbacks;

	private static String CREATE = "create table if not exists FEEDBACK (FEEDBACK_ID INTEGER, CONFERENCE_ID INTEGER, PAPER_TITLE STRING, PAPER_AUTHOR STRING, PAPER_TEXT STRING, PRIMARY KEY (FEEDBACK_ID ASC))";

	static {
		try {
			Connection conn = getConnection();
			conn.createStatement().executeUpdate(CREATE);
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean createFeedback(int conference_id, String paper_title,
			String paper_author, String paper_text) {
		try {
			Connection conn = getConnection();
			PreparedStatement pstmt = conn
					.prepareStatement("INSERT INTO FEEDBACK(CONFERENCE_ID, PAPER_TITLE, PAPER_AUTHOR, PAPER_TEXT) VALUES (?, ?, ?, ?)");
			if (feedbackExists(conn, conference_id, paper_title, paper_author,
					paper_text))
				return false;
			pstmt.setInt(1, conference_id);
			pstmt.setString(2, paper_title);
			pstmt.setString(3, paper_author);
			pstmt.setString(4, paper_text);
			pstmt.executeUpdate();
			pstmt.close();

			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM FEEDBACK WHERE FEEDBACK_ID = (SELECT MAX(FEEDBACK_ID) FROM FEEDBACK) AND CONFERENCE_ID = "
							+ conference_id);
			ResultSet rd = st.executeQuery();

			if (rd.next()) {

				int index = rd.getInt(1);
				if (!my_feedbacks.containsKey(conference_id)) {
					List<TempFeedback> users = new ArrayList<>();
					TempFeedback temp = new TempFeedback(index);
					users.add(temp);
					my_feedbacks.put(conference_id, users);
					all_feedbacks.put(index, temp);
				} else {
					boolean contains = false;
					for (TempFeedback feed : my_feedbacks.get(rd.getInt(2))) {
						if (feed.getID() == rd.getInt(1)) {
							contains = true;
						}
					}
					if (!contains) {
						TempFeedback fb = new TempFeedback(rd.getInt(1));
						my_feedbacks.get(rd.getInt(2)).add(fb);
						all_feedbacks.put(rd.getInt(1), fb);
					}
				}
			}

			conn.close();
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	private static Object query(int conference_id, String column) {
		Object obj = null;
		try {

			Connection conn = getConnection();
			PreparedStatement st = conn.prepareStatement("SELECT " + column
					+ " FROM FEEDBACK WHERE FEEDBACK_ID = ?");
			st.setInt(1, conference_id);
			ResultSet rd = st.executeQuery();
			if (rd.next()) {
				obj = rd.getObject(column);
			}

			st.close();
			rd.close();
			conn.close();
			return obj;
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public static String getPaper(int feedback_id) {
		return (String) query(feedback_id, "PAPER_TEXT");
	}

	public static String getAuthor(int feedback_id) {
		return (String) query(feedback_id, "PAPER_AUTHOR");
	}

	public static String getTitle(int feedback_id) {
		return (String) query(feedback_id, "PAPER_TITLE");
	}

	public static TempConference getConference(int feedback_id) {
		return ConferenceDB.getConference((int) query(feedback_id,
				"CONFERENCE_ID"));
	}

	private static boolean feedbackExists(Connection conn, int conference_id,
			String paper_title, String paper_author, String paper_text)
			throws SQLException {
		boolean name_exists = false;
		PreparedStatement st = conn
				.prepareStatement("SELECT * FROM FEEDBACK WHERE CONFERENCE_ID = ? AND PAPER_TITLE = ? AND PAPER_AUTHOR = ? AND PAPER_TEXT = ?");
		st.setInt(1, conference_id);
		st.setString(2, paper_title);
		st.setString(3, paper_author);
		st.setString(4, paper_text);

		ResultSet rd = st.executeQuery();
		if (rd.next()) {

			if (rd.getInt("CONFERENCE_ID") == conference_id
					&& rd.getString("PAPER_TITLE").equals(paper_title)
					&& rd.getString("PAPER_AUTHOR").equals(paper_author)
					&& rd.getString("PAPER_TEXT").equals(paper_text)) {
				name_exists = true;
				conn.close();
			}
		}
		st.close();

		rd.close();
		return name_exists;
	}

	public static boolean editPaper(int feedback_id, String string) {
		try {

			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("UPDATE FEEDBACK SET PAPER_TEXT = '"
							+ string + "' WHERE FEEDBACK_ID = " + feedback_id);
			st.executeUpdate();
			st.close();
			conn.close();
			return true;

		} catch (Exception e) {
		}
		return false;
	}

	public static boolean deletePaper(int feedback_id) {
		try {
			int conference_id = getConference(feedback_id).getID();
			List<TempFeedback> list = my_feedbacks.get(conference_id);
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getID() == feedback_id) {
					my_feedbacks.get(conference_id).remove(i);
				}
			}
			Connection conn = getConnection();

			PreparedStatement st = conn
					.prepareStatement("DELETE FROM FEEDBACK WHERE FEEDBACK_ID = "
							+ feedback_id);
			st.executeUpdate();
			st.close();
			conn.close();
			return true;

		} catch (Exception e) {
		}
		return false;

	}

	/**
	 * DELETE THIS IF YOU ARE NOT DEBUGGING
	 */
	public static void clearTable() {
		try {
			getConnection().createStatement().executeUpdate(
					"drop table if exists FEEDBACK");

			Connection conn = getConnection();
			conn.createStatement().executeUpdate(CREATE);
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static Map<Integer, List<TempFeedback>> getFeedback() {
		Map<Integer, List<TempFeedback>> feedbacks = new HashMap<>();
		all_feedbacks = new HashMap<>();
		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM FEEDBACK");
			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				if (!feedbacks.containsKey(rd.getInt(2))) {
					List<TempFeedback> feedback = new ArrayList<>();
					TempFeedback temp = new TempFeedback(rd.getInt(1));
					feedback.add(temp);
					feedbacks.put(rd.getInt(2), feedback);
					all_feedbacks.put(rd.getInt(1), temp);
				} else {
					boolean contains = false;
					for (TempFeedback feed : feedbacks.get(rd.getInt(2))) {
						if (feed.getID() == rd.getInt(1)) {
							contains = true;
						}
					}
					if (!contains) {
						TempFeedback fb = new TempFeedback(rd.getInt(1));
						feedbacks.get(rd.getInt(2)).add(fb);
						all_feedbacks.put(rd.getInt(1), fb);
					}

				}

			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return feedbacks;
	}

	public static List<TempFeedback> getFeedbackList(int conference_id) {
		return my_feedbacks.get(conference_id);
	}

	public static TempFeedback getFeedback(int feedback_id) {
		return all_feedbacks.get(feedback_id);

	}

}
