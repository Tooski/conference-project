package Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/*
 * User rough draft
 * Author: Josh Keaton
 */
public class User {

	// the map of conferences and roles a user has

	protected Map<Conference, ConferenceRole> my_conference;
	protected static List<Conference> list_of_conferences = new ArrayList<>();
	Conference temp_conference; // temp variable since map is not set up
								// properly
	List<Feedback> my_feedback;
	String my_username;
	String my_actualname;
	String my_password;
	ConferenceRole is_Author;

	public User(String the_username, String the_actualname,
			String the_password) {
		this(the_username, the_actualname, the_password,
				ConferenceRole.USER);
	}

	public User(String the_username, String the_actualname,
			String the_password,
			ConferenceRole the_conference_role) {

		my_username = the_username;
		my_actualname = the_actualname;
		my_password = the_password;
		my_conference = new HashMap<>();

		is_Author = the_conference_role;
	}

	public User() {
		my_username = "";
		my_actualname = "";
		my_password = "";
	}

	// public boolean addConference(Conference the_con, Role the_role) {
	//
	// my_conference.put(the_con,the_role);
	//
	//
	//
	// return true;
	// }
	/**
	 * This method logs the user into a pre-existing user object
	 * 
	 * @param the_username
	 *            the username entered into the username field
	 * @param the_password
	 *            the password entered into the password field
	 * @return boolean whether or not the user name AND passwords matched those
	 *         of the User object
	 */
	public boolean login(String the_username, String the_password) {
		if (!my_username.equals(the_username)
				&& !my_password.equals(the_password)) {
			return false;
		}
		return true;
	}

	/**
	 * This method allows the user to submit a paper to a conference it also
	 * sets the user to Author status
	 * 
	 * @param the_title
	 *            the title of the paper
	 * @param the_text
	 *            the contents of the paper itself
	 * @return boolean on whether or not creation of the paper was successful
	 */
	public boolean submitPaper(String the_title, String the_paper) {
		if (the_title == null || the_paper == null) {
			return false;
		}
		temp_conference.createFeedback(the_title, my_actualname, the_paper);
		is_Author = ConferenceRole.AUTHOR;

		return true;
	}

	/**
	 * This method is designed to delete a paper submitted by a user who is an
	 * author
	 * 
	 * @return boolean on whether or not paper was successfully deleted
	 */
	public boolean deletePaper(Feedback the_feedback) {
		if (is_Author != ConferenceRole.AUTHOR) {
			return false;
		}

		the_feedback = null;

		return true;
	}

	/**
	 * This method allows a user who is an author to edit a paper they have
	 * written
	 * 
	 * @param the_feedback
	 *            the feedback object that contains their paper
	 * @param new_paper
	 *            the edited paper they wish to submit
	 * @return boolean on whether or not the paper was successfully edited
	 */
	public boolean editPaper(Feedback the_feedback, String new_paper) {

		return the_feedback.editPaper(new_paper);
	}

	/**
	 * this method returns the feedback associated with this user
	 * 
	 * @return the list of this user's associated feedback
	 */
	public List<Feedback> getFeedback() {

		return my_feedback;
	}

	/**
	 * Assigns a user
	 * @param the_conference 
	 * 
	 * @param the_feedback
	 * @return
	 */
	public boolean beAssigned(Feedback the_feedback) {

		return my_feedback.add(the_feedback);
	}

	public List<Conference> getListOfConferences() {
		return list_of_conferences;
	}

	public ConferenceRole getRole(Conference the_conference) {
		return my_conference.get(the_conference);
	}

	public boolean setRole(Conference the_conference, ConferenceRole the_role) {

		my_conference.put(the_conference, the_role);
		return true;
	}

}
