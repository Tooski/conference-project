package model;

import db.ConferenceDB;
import db.FeedbackDB;
import db.FeedbackUserDB;

/*
 * Feedback rough draft
 * Author: Josh Keaton
 */
/**
 * Feedback stores submitted papers and all reviews associated with them.
 * @author Josh
 * @version 2
 *
 */
public class Feedback {
/**
 * defines the current acceptance status of the Paper.
 * 
 *
 */
	public enum Status {
		REJECTED_BY_SUBPROGRAM_CHAIR(0), REJECTED_BY_PROGRAM_CHAIR(1), ASSIGNED_TO_SUBPROGRAM_CHAIR(
				2), ASSIGNED_TO_REVIEWER(3), REVIEWED_BY_REVIEWER(4), RECOMMENDED_BY_SUBPROGRAM_CHAIR(
				5), APPROVED_BY_PROGRAM_CHAIR(6), PENDING(-1);
		private int my_value;
		private StringBuilder toString;

		/**
		 * Constructs the status of a paper
		 * @param value the 
		 */
		private Status(int value) {
			my_value = value;
			toString = new StringBuilder();
			String sb = super.toString();

			for (int i = 0; i < sb.length(); i++)
				if (i == 0 || sb.substring(i - 1, i).equals("_"))
					toString.append(sb.substring(i, i + 1).toUpperCase());
				else if (sb.substring(i, i + 1).equals("_"))
					toString.append(" ");
				else
					toString.append(sb.substring(i, i + 1).toLowerCase());
		}

		/**
		 * gets the status value of the feedback
		 * @return the status value
		 */
		public int getValue() {
			return my_value;
		}

		/**
		 * translates the status values into Enumerators 
		 * @param i the Status value
		 * @return the Status of the current Feedback
		 */
		public static Status translate(int i) {
			if (i >= -1 && i <= 6) {
				switch (i) {
				case 0:
					return REJECTED_BY_SUBPROGRAM_CHAIR;
				case 1:
					return REJECTED_BY_PROGRAM_CHAIR;
				case 2:
					return ASSIGNED_TO_SUBPROGRAM_CHAIR;
				case 3:
					return ASSIGNED_TO_REVIEWER;
				case 4:
					return REVIEWED_BY_REVIEWER;
				case 5:
					return RECOMMENDED_BY_SUBPROGRAM_CHAIR;
				case 6:
					return APPROVED_BY_PROGRAM_CHAIR;
				case -1:
					return PENDING;
				}
			}
			return null;

		}

		/**
		 * the status of the current feedback as a string
		 */
		public String toString() {
			return toString.toString();

		}

	}

	private int my_feedback_id;
	private int author_id;
	private String author_name;
	private String paper_abstract;
	private String paper_title;
	private Conference my_conference;
	private Object my_blob;


	/**
	 * Constructs a new feedback object
	 * @param feedback_id the ID of this feedback object
	 * @param the_author_id the ID of the author who submitted the paper
	 * @param the_author_name the name of the author
	 * @param the_paper_abstract the abstract of the paper
	 * @param the_paper_title the title of the paper
	 * @param conference the conference it it linked to
	 * @param blob the blob
	 */
	public Feedback(int feedback_id, int the_author_id, String the_author_name,
			String the_paper_abstract, String the_paper_title, int conference,
			Object blob) {
		my_feedback_id = feedback_id;
		author_id = the_author_id;
		author_name = the_author_name;
		paper_abstract = the_paper_abstract;
		paper_title = the_paper_title;
		my_conference = ConferenceDB.getConference(conference);	
		my_blob = blob;
		
	}

	/**
	 * Allows a paper to be edited
	 * 
	 * @param new_paper
	 *            the new paper
	 * @return if the paper was successfully edited
	 */
	public boolean editPaper(Object new_paper) {
		my_blob = new_paper;
		return FeedbackDB.editPaper(my_feedback_id, my_conference.getID(), new_paper);
	}

	/**
	 * gets the paper object
	 * @return the paper object
	 */
	public Object getPaper()
	{
		return my_blob;
	}
	
	/**
	 * Sets the status of the paper
	 * 
	 * @param new_status
	 *            the new status of the paper
	 * @return
	 * @return if the status was updated properly
	 */
	public boolean setStatus(int user_id, Status new_status, int conference_id,
			int user_role) {
		return FeedbackUserDB.setStatus(my_feedback_id, user_id, new_status,
				conference_id, user_role);

	}

	/**
	 * Submits a review to the feedback object
	 * 
	 * @param reviewer
	 *            the reviewer
	 * @param review
	 *            the review
	 * @param conference_title
	 *            the title of the conference
	 * @return the index of the added review
	 */
	public boolean createReview(int user_id, String review, int[] values,
			int conference_id, int user_role) {
		return FeedbackUserDB.setReview(my_feedback_id, user_id, review,
				values, conference_id, user_role);
	}

	/**
	 * gets the review of a particular paper
	 * @param user_id the id of the user who wrote the review
	 * @param conference_id the id of the conference the review is in
	 * @param user_role the role of the user trying to view the review
	 * @return the review
	 */
	public String getReview(int user_id, int conference_id, int user_role) {
		return FeedbackUserDB.getReview(my_feedback_id, user_id, conference_id,
				user_role);
	}

	/**
	 * Gets the score of the feedback
	 * @param user_id the id of the user
	 * @param conference_id the id of the conference
	 * @param user_role the role of the user trying to get the score
	 * @return the score
	 */
	public int[] getScore(int user_id, int conference_id, int user_role) {
		return FeedbackUserDB.getScores(my_feedback_id, user_id, conference_id,
				user_role);
	}

	/**
	 * Gets the status of the feedback
	 * @param user_id the id of the user
	 * @param conference_id the id of the conference
	 * @param user_role the role of the user trying to get the status
	 * @return the status of the feedback
	 */
	public Status getStatus(int user_id, int conference_id, int user_role) {
		return FeedbackUserDB.getStatus(my_feedback_id, user_id, conference_id,
				user_role);
	}

	/**
	 * gets the title of the paper
	 * @return the title of the paper
	 */
	public String getTitle() {
		return paper_title;
	}

	/**
	 * gets the name of the author
	 * @return the name of the authro
	 */
	public String getAuthor() {
		return author_name;
	}
 /**
  * gets the paper's abstract
  * @return the paper's abstract
  */
	public String getPaperAbstract() {
		return paper_abstract;
	}

	/**
	 * gets the ID of the author
	 * @return the author's ID
	 */
	public int getAuthorID() {
		return author_id;
	}

	// public ArrayList<Review> getReviewList() {
	// return my_reviews;
	// }

	/**
	 * gets the conference
	 * @return the conference
	 */
	public Conference getConference() {
		return my_conference;
	}

	// @Override
	// public boolean equals(Object object) {
	// boolean sameSame = false;
	//
	// if (object != null && object instanceof TempFeedback) {
	// sameSame = this.my_feedback_id == ((TempFeedback) object).my_feedback_id;
	// }
	//
	// return sameSame;
	// }
	//
	// @Override
	// public int hashCode() {
	// return my_feedback_id * 37 + toString().hashCode();
	// }

	/**
	 * gets the ID
	 * @return the ID
	 */
	public int getID() {
		return my_feedback_id;
	}

	/**
	 * the title of the feedback ie. the paper as a String
	 */
	public String toString() {
		return getTitle();
	}

}
