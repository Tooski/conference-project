package View;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import swing.custom.JCustomButton;
import swing.custom.JCustomComboBox;
import swing.custom.JCustomLabel;
import swing.custom.JCustomTable;
import swing.custom.JDefaultFont;
import swing.custom.JDialogBox;

import db.ConferenceDB;
import db.ConferenceUserDB;
import db.UserDB;

import Model.Conference;
import Model.ConferenceRole;

@SuppressWarnings("serial")
public class OverlayUI extends JFrame implements Observer {

	private JPanel left_panel;
	private JPanel top_panel;
	private int my_user_id;
	@SuppressWarnings("rawtypes")
	private JCustomComboBox role;
	private Conference selected_conference;
	private JPanel center_panel;
	private Font default_font;
	private OverlayUI this_class;

	public OverlayUI(int user_id) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		this_class = this;
		default_font = new JDefaultFont().getFont(12);
		left_panel = new JPanel(new BorderLayout());

		center_panel = new JPanel(new BorderLayout());
		top_panel = new JPanel(new BorderLayout())
		// {
		// public void paintComponent(Graphics grphcs) {
		// super.paintComponent(grphcs);
		// Graphics2D g2d = (Graphics2D) grphcs;
		// g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		// RenderingHints.VALUE_ANTIALIAS_ON);
		// GradientPaint gp = new GradientPaint(0, 0, getBackground()
		// .brighter(), 0, getHeight(), getBackground()
		// .darker());
		// g2d.setPaint(gp);
		// g2d.fillRect(0, 0, getWidth(), getHeight());
		//
		// }
		//
		// }
		;
		my_user_id = user_id;
		role = new JCustomComboBox(0, false);

		final JLabel conference_name = new JCustomLabel();
		final JLabel conference_date = new JCustomLabel();
		final JLabel conference_role;
		if (UserDB.isAdmin(my_user_id)) {
			conference_role = new JCustomLabel("User Role: Admin");
			center_panel = new ConferenceDBUI(this_class);
		} else
			conference_role = new JCustomLabel("User Role: User");
		final JLabel user_name = new JCustomLabel("Username: "
				+ UserDB.getUserName(my_user_id));
		JPanel panel = new JPanel();
		JButton logout = new JCustomButton("Logout");
		logout.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new LoginUI().display();
				dispose();
			}

		});

		panel.add(logout);
		panel.setOpaque(false);
		JPanel label_row = new JPanel(new GridLayout(2, 2));
		label_row.add(conference_name);
		label_row.add(conference_role);
		label_row.add(conference_date);
		label_row.add(user_name);
		label_row.setOpaque(false);

		top_panel.add(label_row, BorderLayout.CENTER);
		top_panel.add(panel, BorderLayout.EAST);
		top_panel.setBackground(Color.WHITE);

		left_panel.add(role, BorderLayout.NORTH);
		left_panel.add(initTable(), BorderLayout.CENTER);
		//Use back to display Users. (for now)
		JButton back = new JCustomButton("Back");
		back.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new UserGUI(selected_conference, UserDB.getUser(my_user_id)).display();
				
			}
		});
		
		left_panel.add(back, BorderLayout.SOUTH);
		left_panel.setBackground(Color.WHITE);
		role.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (role.getItemCount() > 0) {
					conference_name.setText("Conference Name: "
							+ selected_conference.getConferenceName());
					conference_date.setText("Conference Date: "
							+ selected_conference.getConferenceDate()
									.toString());
					conference_role.setText("User Role: "
							+ role.getSelectedItem().toString());

					UserDB.getUser(my_user_id).setCurrentRole(
							(ConferenceRole) role.getSelectedItem());
				}

			}

		});

		JSplitPane jsp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				left_panel, center_panel);

		jsp.setDividerSize(3);
		jsp.setContinuousLayout(true);
		add(jsp, BorderLayout.CENTER);

		left_panel.setBorder(new EmptyBorder(6, 6, 6, 6));

		add(top_panel, BorderLayout.NORTH);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new OverlayUI(2).display();
	}

	public void display() {
		setSize(800, 600);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(true);
		setVisible(true);
		setLocationRelativeTo(null);

	}

	/**
	 * Updates the roles.
	 * 
	 * @param conference_id
	 *            the conference ID.
	 */
	@SuppressWarnings("unchecked")
	private void updateRoles(int conference_id) {
		role.removeAllItems();
		for (ConferenceRole r : ConferenceUserDB.getRole(conference_id,
				my_user_id)) {
			role.addItem(r);
		}
	}

	JTable tb;

	/**
	 * Creates the table panel;
	 * 
	 * @return the panel holding the table.
	 */
	private JPanel initTable() {

		JPanel panel = new JPanel(new BorderLayout());
		tb = new JCustomTable(new Object[] { "Conference Name" }, ConferenceDB.getConferenceList());
		JButton select = new JCustomButton("Select");
		select.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				if (tb.getSelectedRow() != -1) {
					Object o = tb.getModel().getValueAt(tb.getSelectedRow(),
							tb.getSelectedColumn());
					if (o != null) {
						selected_conference = (Conference) o;
						updateRoles(selected_conference.getID());
					}
				}
			}
		});
		
		JButton info = new JCustomButton("Info");
		info.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (tb.getSelectedRow() != -1) {
					Conference selected_conference = ((Conference) tb.getModel()
							.getValueAt(tb.getSelectedRow(),
									tb.getSelectedColumn()));
					final JComponent[] inputs = new JComponent[] {

							new JCustomLabel("Conference Name: "
									+ selected_conference.getConferenceName()),
							new JCustomLabel("Conference Date: "
									+ selected_conference.getConferenceDate()),
							new JCustomLabel("Program Chair: "
									+ selected_conference.getProgramChair()
											.getActualUserName()),

					};
					JDialogBox.createDialogBox(inputs,
							selected_conference.getConferenceName());

				}

			}

		});

		JPanel info_panel = new JPanel(new BorderLayout());
		info_panel.setBorder(new EmptyBorder(6, 3, 6, 0));
		info_panel.add(info, BorderLayout.CENTER);
		JPanel select_panel = new JPanel(new BorderLayout());
		select_panel.setBorder(new EmptyBorder(6, 0, 6, 3));
		select_panel.add(select, BorderLayout.CENTER);
		info_panel.setOpaque(false);
		select_panel.setOpaque(false);
		JPanel button_panel = new JPanel(new GridLayout(1, 0));
		button_panel.add(select_panel);
		button_panel.add(info_panel);
		button_panel.setBackground(Color.WHITE);
		JScrollPane jsp = new JScrollPane(tb);
		jsp.setPreferredSize(new Dimension(125, 0));
		panel.add(jsp, BorderLayout.CENTER);
		panel.add(button_panel, BorderLayout.SOUTH);
		return panel;
	}

	@Override
	public void update(Observable arg0, Object arg1) {

		((DefaultTableModel) tb.getModel()).addRow(new Object[] { ConferenceDB
				.getConference((int) arg1) });

	}

}
