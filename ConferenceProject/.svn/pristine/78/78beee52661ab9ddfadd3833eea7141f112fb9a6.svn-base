package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Model.ConferenceRole;
import Model.User;

public class ConferenceUserDB extends AbstractDB {

	static Map<Integer, List<Integer>> my_users = getUsers();

	static {
		try {
			Connection conn = getConnection();
			conn.createStatement()
					.executeUpdate(
							"create table if not exists USER_CONFERENCE (CONFERENCE_ID INTEGER, USER_ID INTEGER, USER_ROLE INTEGER)");
			conn.createStatement()
					.executeUpdate(
							"create table if not exists USER_USER (CONFERENCE_ID INTEGER, USER_ID INTEGER, PARTICIPANT_ID INTEGER)");
			conn.close();
		} catch (Exception e) {
		}
	}

	public static boolean addUser(int conference_id, int user_id,
			int participant_id, ConferenceRole role) {
		try {
			Connection conn = getConnection();
			if (usernameExists(conn, conference_id, participant_id, role.getRole()))
				return false;
			PreparedStatement pstmt = conn
					.prepareStatement("INSERT INTO USER_CONFERENCE(CONFERENCE_ID, USER_ID, USER_ROLE) VALUES (?, ?, ?)");

			pstmt.setInt(1, conference_id);
			pstmt.setInt(2, participant_id);
			pstmt.setInt(3, role.getRole());
			pstmt.executeUpdate();
			pstmt.close();

			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM USER_CONFERENCE WHERE CONFERENCE_ID = (SELECT MAX(CONFERENCE_ID) FROM USER_CONFERENCE)");
			ResultSet rd = st.executeQuery();
			if (rd.next()) {
				if (!my_users.containsKey(rd.getInt(1))) {
					List<Integer> users = new ArrayList<>();
					users.add(rd.getInt(2));
					my_users.put(rd.getInt(1), users);
				} else {
					if (!my_users.get(rd.getInt(1)).contains(rd.getInt(2)))
						my_users.get(rd.getInt(1)).add(rd.getInt(2));
				}
			}

			st.close();
			rd.close();

			if (!userNameExists(conn, conference_id, user_id, participant_id)) {
				PreparedStatement pst = conn
						.prepareStatement("INSERT INTO USER_USER(CONFERENCE_ID, USER_ID, PARTICIPANT_ID) VALUES (?, ?, ?)");
				pst.setInt(1, conference_id);
				pst.setInt(2, user_id);
				pst.setInt(3, participant_id);
				pst.executeUpdate();
				pst.close();
			}
			conn.close();
			return true;
		} catch (Exception e) {
		}
		return false;

	}

	private static boolean userNameExists(Connection conn, int conference_id,
			int user_id, int participant_id) throws SQLException {
		boolean name_exists = false;
		PreparedStatement st = conn
				.prepareStatement("SELECT * FROM USER_USER WHERE CONFERENCE_ID = "
						+ conference_id
						+ " AND USER_ID = "
						+ user_id
						+ " AND PARTICIPANT_ID =" + participant_id);

		ResultSet rd = st.executeQuery();
		if (rd.next()) {
			name_exists = true;
		}
		rd.close();
		st.close();
		return name_exists;
	}

	private static boolean usernameExists(Connection conn, int conference_id,
			int user_id, int role) throws SQLException {
		boolean name_exists = false;
		PreparedStatement st = conn
				.prepareStatement("SELECT * FROM USER_CONFERENCE WHERE CONFERENCE_ID = ? AND USER_ID = ? AND USER_ROLE = ?");
		st.setInt(1, conference_id);
		st.setInt(2, user_id);
		st.setInt(3, role);

		ResultSet rd = st.executeQuery();
		if (rd.next() && rd.getInt("USER_ID") == user_id) {
			name_exists = true;
			conn.close();
		}
		rd.close();
		st.close();
		return name_exists;
	}

	/**
	 * DELETE THIS IF YOU ARE NOT DEBUGGING
	 */
	public static void clearTable() {
		try {
			Connection conn = getConnection();
			conn.createStatement().executeUpdate(
					"drop table if exists USER_CONFERENCE");
			conn.createStatement().executeUpdate(
					"drop table if exists USER_USER");
			conn.createStatement()
					.executeUpdate(
							"create table if not exists USER_CONFERENCE (CONFERENCE_ID INTEGER, USER_ID INTEGER, USER_ROLE INTEGER)");
			conn.createStatement()
					.executeUpdate(
							"create table if not exists USER_USER (CONFERENCE_ID INTEGER, USER_ID INTEGER, PARTICIPANT_ID INTEGER)");
			conn.close();

		} catch (SQLException e) {
		}
	}

	public static List<ConferenceRole> getRole(int conference_id, int user_id) {
		List<ConferenceRole> role = new ArrayList<>();
		try {

			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT USER_ROLE FROM USER_CONFERENCE WHERE USER_ID = "
							+ user_id + " AND CONFERENCE_ID = " + conference_id);
			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				switch (rd.getInt(1)) {
				case 0:
					role.add(ConferenceRole.ADMIN);
					break;
				case 1:
					role.add(ConferenceRole.PROGRAM_CHAIR);
					break;
				case 2:
					role.add(ConferenceRole.SUBPROGRAM_CHAIR);
					break;
				case 3:
					role.add(ConferenceRole.REVIEWER);
					break;
				case 4:
					role.add(ConferenceRole.AUTHOR);
					break;
				}
			}
			st.close();
			rd.close();
			conn.close();
		} catch (Exception e) {
		}
		return role;
	}

	private static Map<Integer, List<Integer>> getUsers() {
		Map<Integer, List<Integer>> my_users = new HashMap<>();

		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM USER_CONFERENCE");
			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				if (!my_users.containsKey(rd.getInt(1))) {
					List<Integer> users = new ArrayList<>();
					users.add(rd.getInt(2));
					my_users.put(rd.getInt(1), users);
				} else {
					if (!my_users.get(rd.getInt(1)).contains(rd.getInt(2)))
						my_users.get(rd.getInt(1)).add(rd.getInt(2));
				}

			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
		}

		return my_users;
	}

	public static boolean promoteUser(int conference_id, int user_id,
			int participant_id, ConferenceRole role) {
		try {
			Connection conn = getConnection();

			if (!usernameExists(conn, conference_id, participant_id, role.getRole())) {
				PreparedStatement pstmt = conn
						.prepareStatement("INSERT INTO USER_CONFERENCE(CONFERENCE_ID, USER_ID, USER_ROLE) VALUES (?, ?, ?)");

				pstmt.setInt(1, conference_id);
				pstmt.setInt(2, participant_id);
				pstmt.setInt(3, role.getRole());
				pstmt.executeUpdate();
				pstmt.close();
			}

			if (!userNameExists(conn, conference_id, user_id, participant_id)) {
				PreparedStatement pst = conn
						.prepareStatement("INSERT INTO USER_USER(CONFERENCE_ID, USER_ID, PARTICIPANT_ID) VALUES (?, ?, ?)");
				pst.setInt(1, conference_id);
				pst.setInt(2, user_id);
				pst.setInt(3, participant_id);
				pst.executeUpdate();
				pst.close();
			}
			conn.close();
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	public static List<User> getParticipants(int conference_id, int user_id) {
		List<User> participants = new ArrayList<>();
		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT PARTICIPANT_ID FROM USER_USER WHERE CONFERENCE_ID = "
							+ conference_id + " AND USER_ID = " + user_id);
			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				participants.add(UserDB.getUser(rd.getInt(1)));
			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return participants;

	}

	public static List<User> getUserList(int conference_id) {
		List<User> users = new ArrayList<>();
		for (int i : my_users.get(conference_id)) {
			users.add(UserDB.getUser(i));
		}
		return users;
	}
}
