/*
 * CreateAccountGUI.
 * Quan Le
 */
package View;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Observable;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import swing.custom.JCustomButton;
import swing.custom.JCustomLabel;
import swing.custom.JCustomTextField;
import swing.custom.JDefaultFont;
import swing.custom.JDialogBox;


import db.UserDB;

@SuppressWarnings("serial")
public class CreateAccountGUI extends Observable implements GUI {
	/**
	 * The default width for text fields and text areas.
	 */
	private static final int TEXT_WIDTH = 15;

	/**
	 * Username text field.
	 */
	private final JTextField my_username_text;

	/**
	 * Actual name text field.
	 */
	private final JTextField my_actualname_text;

	/**
	 * Password text field.
	 */
	private final JTextField my_password_text;

	/**
	 * A button used to create account.
	 */
	private final JButton my_create_button;
	

	public CreateAccountGUI() {
		super();
		my_actualname_text = new JCustomTextField(TEXT_WIDTH);
		my_username_text = new JCustomTextField(TEXT_WIDTH);
		my_password_text = new JCustomTextField(TEXT_WIDTH);
		my_username_text.setFont(new JDefaultFont().getFont(16));
		my_password_text.setFont(new JDefaultFont().getFont(16));
		my_actualname_text.setFont(new JDefaultFont().getFont(16));
		my_create_button = new JCustomButton("Create");
		
		setupComponents();
	}

	/**
	 * Helper method to perform the work of setting up the GUI components.
	 */
	private void setupComponents() {
		my_create_button.addActionListener(new CreateListener());
		my_create_button.setMnemonic(KeyEvent.VK_C);
		final JButton cancel_button = new JCustomButton("Cancel");
		cancel_button.setMnemonic(KeyEvent.VK_N);
		cancel_button.addActionListener(new CancelListener());

		final JLabel actualname_label = new JCustomLabel("Actual name:  ");
		final JLabel username_label = new JCustomLabel("Username:  ");
		final JLabel password_label = new JCustomLabel("Password:  ");
		username_label.setFont(new JDefaultFont().getFont(15));
		password_label.setFont(new JDefaultFont().getFont(15));
		actualname_label.setFont(new JDefaultFont().getFont(15));

		my_actualname_text.setEditable(true);
		my_username_text.setEditable(true);
		my_password_text.setEditable(true);

		final JPanel master_panel = new JPanel();
		master_panel.setName("Account Creation");

		final JPanel button_panel = new JPanel(new GridLayout(1,2));
		master_panel.setLayout(new BoxLayout(master_panel, BoxLayout.Y_AXIS));

		JPanel login_panel = new JPanel();
		login_panel.setLayout(new BoxLayout(login_panel, BoxLayout.Y_AXIS));
		JPanel username_fields = new JPanel(new BorderLayout());
		username_fields.add(username_label, BorderLayout.WEST);
		username_fields.add(my_username_text, BorderLayout.CENTER);
		
		JPanel password_fields = new JPanel(new BorderLayout());
		password_fields.add(password_label, BorderLayout.WEST);
		password_fields.add(my_password_text, BorderLayout.CENTER);
		
		JPanel actual_name = new JPanel(new BorderLayout());
		actual_name.add(actualname_label, BorderLayout.WEST);
		actual_name.add(my_actualname_text, BorderLayout.CENTER);
		
		login_panel.add(username_fields);
		login_panel.add(password_fields);
		login_panel.add(actual_name);
		JPanel login_p = new JPanel(new BorderLayout());
		login_p.add(login_panel, BorderLayout.NORTH);
		login_p.setBorder(new EmptyBorder(0,10,0,10));

		button_panel.add(my_create_button, BorderLayout.NORTH);
		button_panel.add(cancel_button, BorderLayout.CENTER);
		button_panel.setBorder(new EmptyBorder(10,10,10,10));


		final JLabel label = new JLabel("MSEE", JLabel.CENTER);
		label.setFont(new JDefaultFont().getFont(72));
		final JLabel bottom_label = new JLabel("Account Creation", JLabel.CENTER);
		bottom_label.setFont(new JDefaultFont().getFont(18));

		JPanel p = new JPanel(new BorderLayout());
		p.add(label, BorderLayout.CENTER);
		p.add(bottom_label, BorderLayout.SOUTH);
		p.setBorder(new EmptyBorder(5,0,10,0));
		
		
		
		master_panel.add(p);
		master_panel.add(login_p);
		master_panel.add(button_panel);
		panel = master_panel;

	}
	JPanel panel;
	
	public JPanel display()
	{
		return panel;
	}

	/**
	 * An action listener for the create button.
	 */
	private class CreateListener implements ActionListener {
		public void actionPerformed(final ActionEvent the_event) {
			final String username = my_username_text.getText();
			final String password = my_password_text.getText();
			final String name = my_actualname_text.getText();
			int value = UserDB.createUser(username, password, name);
			if (value == 0) {
				

				if (!JDialogBox.createDialogBox("Account successfully created!", null).isShowing()) {
					setChanged();
					notifyObservers();
				}
			} else if (value == 1)
			{
				JDialogBox.createDialogBox("Invalid username, name or password", null);
			}
			else {
				JDialogBox.createDialogBox("Username has already been taken", null);
			}
			
		}
	}

	/**
	 * An action listener for the cancel button.
	 */
	private class CancelListener implements ActionListener {
		public void actionPerformed(final ActionEvent the_event) {
			setChanged();
			notifyObservers();
		}
	}
}
