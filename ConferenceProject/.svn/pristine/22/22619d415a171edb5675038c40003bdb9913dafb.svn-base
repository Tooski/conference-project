package View;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import swing.custom.JCustomButton;
import swing.custom.JCustomComboBox;
import swing.custom.JCustomLabel;
import swing.custom.JCustomList;
import swing.custom.JCustomTable;
import swing.custom.JDefaultFont;
import swing.custom.JDialogBox;

import db.ConferenceDB;
import db.ConferenceUserDB;
import db.FeedbackDB;
import db.UserDB;

import Model.Conference;
import Model.ConferenceRole;
import Model.User;

/**
 * 
 * @author Josef Nosov
 * 
 */
@SuppressWarnings("serial")
public class OverlayUI extends JFrame implements Observer {

	private JPanel left_panel;
	private JPanel top_panel;
	private int my_user_id;
	private JCustomComboBox<ConferenceRole> role;
	private Conference selected_conference;
	private JPanel center_panel;
	private OverlayUI this_class;
	private User current_user;
	private Stack<JPanelHolder> back_history;
	final JButton back = new JCustomButton("Back");

	public OverlayUI(int user_id) {
		super("MSEE Conference Organizer");

		this_class = this;
		left_panel = new JPanel(new BorderLayout());
		current_user = UserDB.getUser(user_id);
		center_panel = new JPanel(new BorderLayout());
		top_panel = new JPanel(new BorderLayout());
		my_user_id = user_id;
		role = new JCustomComboBox<>(0, false);
		back_history = new Stack<>();

		final JLabel conference_name = new JCustomLabel();
		final JLabel conference_date = new JCustomLabel();
		final JLabel conference_role;
		if (UserDB.isAdmin(my_user_id)) {
			conference_role = new JCustomLabel("User Role: Admin");
			center_panel.add(new ConferenceDBUI(this_class, my_user_id));

		} else
			conference_role = new JCustomLabel("User Role: User");
		final JLabel user_name = new JCustomLabel("Username: "
				+ UserDB.getUserName(my_user_id));
		JPanel panel = new JPanel();
		JButton logout = new JCustomButton("Logout");
		logout.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new LoginUI().display();
				dispose();
			}

		});

		panel.add(logout);
		panel.setOpaque(false);
		JPanel label_row = new JPanel(new GridLayout(2, 2));
		label_row.add(conference_name);
		label_row.add(conference_role);
		label_row.add(conference_date);
		label_row.add(user_name);
		label_row.setOpaque(false);
		label_row.setBorder(new EmptyBorder(5, 10, 5, 10));
		top_panel.add(label_row, BorderLayout.CENTER);
		top_panel.add(panel, BorderLayout.EAST);
		top_panel.setBackground(Color.WHITE);

		left_panel.add(role, BorderLayout.NORTH);
		left_panel.add(initTable(), BorderLayout.CENTER);
		// Use back to display Users. (for now)

		back.setEnabled(false);

		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (back_history.size() > 0) {

					center_panel.removeAll();
					JPanelHolder jph = back_history.pop();
					jph.getUser().setCurrentRole(jph.getCurrentRole());
					if (role.getItemCount() > 0) {
						conference_name.setText("Conference Name: "
								+ jph.getConference().getConferenceName());
						conference_date.setText("Conference Date: "
								+ jph.getConference().getConferenceDate()
										.toString());
						conference_role.setText("User Role: "
								+ jph.getUser().getCurrentRole());

						current_user.setCurrentRole((ConferenceRole) role
								.getSelectedItem());
					}
					center_panel.add(jph.display());
					validate();
					repaint();
					back.setEnabled(!back_history.isEmpty());
				}

			}
		});

		left_panel.add(back, BorderLayout.SOUTH);
		left_panel.setBackground(Color.WHITE);
		role.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (role.getItemCount() > 0) {
					conference_name.setText("Conference Name: "
							+ selected_conference.getConferenceName());
					conference_date.setText("Conference Date: "
							+ selected_conference.getConferenceDate()
									.toString());
					conference_role.setText("User Role: "
							+ role.getSelectedItem().toString());

					if (center_panel.getComponentCount() >= 1) {
						back_history.addElement(new JPanelHolder(
								(JPanel) center_panel.getComponent(0),
								current_user, selected_conference));
						back.setEnabled(true);
						center_panel.removeAll();
					}

					current_user.setCurrentRole((ConferenceRole) role
							.getSelectedItem());

					GUI options = null;
					switch (current_user.getCurrentRole()) {
					case PROGRAM_CHAIR:
						current_user
								.setCurrentRole(ConferenceRole.PROGRAM_CHAIR);
						options = new OptionsUI(selected_conference,
								current_user);
						break;
					case SUBPROGRAM_CHAIR:
						current_user
								.setCurrentRole(ConferenceRole.SUBPROGRAM_CHAIR);

						options = new OptionsUI(selected_conference,
								current_user);

						break;

					case REVIEWER:
						current_user.setCurrentRole(ConferenceRole.REVIEWER);

						options = new PaperGUI(selected_conference,
								current_user);
						break;
					case USER:
					case AUTHOR:
					default:
						current_user.setCurrentRole(ConferenceRole.USER);
						options = new PaperGUI(selected_conference,
								current_user);
						break;
					}
					((Observable) options).addObserver(this_class);
					center_panel.add(options.display());
					// add(center_panel);
					validate();
					repaint();

				}

			}

		});

		JSplitPane jsp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				left_panel, center_panel);

		jsp.setDividerSize(3);
		jsp.setContinuousLayout(true);
		add(jsp, BorderLayout.CENTER);

		left_panel.setBorder(new EmptyBorder(6, 6, 6, 6));

		add(top_panel, BorderLayout.NORTH);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new OverlayUI(4).display();
	}

	public void display() {
		setSize(800, 600);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(true);
		setVisible(true);
		setLocationRelativeTo(null);

	}

	/**
	 * Updates the roles.
	 * 
	 * @param conference_id
	 *            the conference ID.
	 */
	private void updateRoles(int conference_id) {
		role.removeAllItems();
		List<ConferenceRole> c = ConferenceUserDB.getRole(conference_id,
				my_user_id);

		for (ConferenceRole r : c) {
			role.addItem(r);
		}
		if (!ConferenceUserDB.getRole(conference_id, my_user_id).contains(
				ConferenceRole.PROGRAM_CHAIR)) {
			if (FeedbackDB.isAuthor(my_user_id))
				role.addItem(ConferenceRole.AUTHOR);
			else
				role.addItem(ConferenceRole.USER);
		}

	}

	JCustomList<Conference> tb;

	/**
	 * Creates the table panel;
	 * 
	 * @return the panel holding the table.
	 */
	private JPanel initTable() {

		JPanel panel = new JPanel(new BorderLayout());

		tb = new JCustomList<Conference>();
		tb.addElements(ConferenceDB.getConferenceList());

		final JButton info = new JCustomButton("Info");
		info.setEnabled(false);
		info.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (tb.getSelectedIndex() != -1) {
					Conference selected_conference = tb.getSelectedValue();
					final JComponent[] inputs = new JComponent[] {

							new JCustomLabel("Conference Name: "
									+ selected_conference.getConferenceName()),
							new JCustomLabel("Conference Date: "
									+ selected_conference.getConferenceDate()),
							new JCustomLabel("Program Chair: "
									+ selected_conference.getProgramChair()
											.getActualName()),

					};
					JDialogBox.createDialogBox(inputs,
							selected_conference.getConferenceName());

				}

			}

		});

		JPanel info_panel = new JPanel(new BorderLayout());

		JPanel select_panel = new JPanel(new BorderLayout());
		final JButton select_button = new JCustomButton("Select");
		final JPanel button_panel = new JPanel(new BorderLayout());
		final Conference[] previous_selected = { null };
		if (!UserDB.isAdmin(my_user_id)) {

			button_panel.setLayout(new GridLayout(1, 0));
			select_button.setEnabled(false);
			select_button.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					if (tb.getSelectedIndex() != -1) {
						Object o = tb.getSelectedValue();
						if (o != null) {
							selected_conference = (Conference) o;

							previous_selected[0] = (Conference) o;
							if (center_panel.getComponentCount() > 0) {
								back_history.addElement(new JPanelHolder(
										(JPanel) center_panel.getComponent(0),
										current_user, selected_conference));
								back.setEnabled(true);
								center_panel.removeAll();
							}
							updateRoles(selected_conference.getID());
							select_button.setEnabled(false);
						}
					}
				}
			});
			select_panel.add(select_button, BorderLayout.CENTER);
			info_panel.setBorder(new EmptyBorder(6, 3, 6, 0));
		} else
			info_panel.setBorder(new EmptyBorder(6, 0, 6, 0));

		tb.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				boolean b = true;
				if (previous_selected[0] != null)
					b = !previous_selected[0].equals(tb.getSelectedValue());
				select_button.setEnabled(tb.getSelectedIndex() != -1 && b);
				info.setEnabled(tb.getSelectedIndex() != -1);
			}

		});

		info_panel.add(info, BorderLayout.CENTER);
		select_panel.setBorder(new EmptyBorder(6, 0, 6, 3));
		info_panel.setOpaque(false);
		select_panel.setOpaque(false);
		button_panel.add(select_panel);
		button_panel.add(info_panel);
		button_panel.setBackground(Color.WHITE);
		JScrollPane jsp = new JScrollPane(tb);
		jsp.setPreferredSize(new Dimension(125, 0));
		panel.add(jsp, BorderLayout.CENTER);
		panel.add(button_panel, BorderLayout.SOUTH);
		return panel;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg0 instanceof CreateConUI) {
			tb.addElement(ConferenceDB.getConference((int) arg1));
		} else if (arg1 instanceof ConferenceRole) {
			role.addItem((ConferenceRole) arg1);
		} else {
			// if (!(arg0 instanceof ReviewFormUI)) {

			if (arg0 instanceof PaperGUI) {
				int user =((DefaultComboBoxModel)role.getModel()).getIndexOf(ConferenceRole.USER);

				int author =((DefaultComboBoxModel)role.getModel()).getIndexOf(ConferenceRole.AUTHOR);
				if (FeedbackDB.isAuthor(my_user_id) && user != -1) {			
					role.addItem(ConferenceRole.AUTHOR);

					((DefaultComboBoxModel)role.getModel()).removeElementAt(user);

					role.setSelectedItem(ConferenceRole.AUTHOR);
				} else if (!FeedbackDB.isAuthor(my_user_id)&& author != -1) {

					role.addItem(ConferenceRole.USER);
					((DefaultComboBoxModel)role.getModel()).removeElementAt(author);

					role.setSelectedItem(ConferenceRole.USER);
				}
			} else {
				if (arg1 instanceof Observable)
					((Observable) arg1).addObserver(this);
				back_history.addElement(new JPanelHolder((JPanel) center_panel
						.getComponent(0), current_user, selected_conference));
				back.setEnabled(true);
				center_panel.removeAll();
				center_panel.add(((GUI) arg1).display());
				validate();
				repaint();
			}
			// }
		}

	}

}
