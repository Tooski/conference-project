package View;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import javax.swing.JPanel;

import swing.custom.JCustomButton;
import swing.custom.JCustomComboBox;
import swing.custom.JCustomLabel;
import swing.custom.JCustomTextField;
import swing.custom.JDialogBox;

import Model.Conference;
import Model.Date;
import Model.User;

import db.ConferenceDB;
import db.UserDB;

public class CreateConUI extends Observable {

	private JCustomTextField con_name;

	private int[] year;
	private int[] month;
	private int[] day;

	private JComboBox<Integer> rev_year;
	private JComboBox<Integer> rev_month;
	private JComboBox<Integer> rev_day;

	private JComboBox<Integer> au_year;
	private JComboBox<Integer> au_month;
	private JComboBox<Integer> au_day;

	private JComboBox<Integer> con_year;
	private JComboBox<Integer> con_month;
	private JComboBox<Integer> con_day;

	private JComboBox<Integer> sub_year;
	private JComboBox<Integer> sub_month;
	private JComboBox<Integer> sub_day;
	private final JCustomTextField combo;
	private JButton finish;
	private JPanel frame;

	public CreateConUI() {
		super();
		
		con_name = new JCustomTextField(30);
		finish = new JCustomButton("Finish");
		finish.setEnabled(false);
		combo = new JCustomTextField(20);
		year = new int[50];
		month = new int[12];
		day = new int[31];
		populateDates();

		rev_year = new JCustomComboBox<Integer>();
		rev_month = new JCustomComboBox<Integer>();
		rev_day = new JCustomComboBox<Integer>();

		au_year = new JCustomComboBox<Integer>();
		au_month = new JCustomComboBox<Integer>();
		au_day = new JCustomComboBox<Integer>();

		con_year = new JCustomComboBox<Integer>();
		con_month = new JCustomComboBox<Integer>();
		con_day = new JCustomComboBox<Integer>();

		sub_year = new JCustomComboBox<Integer>();
		sub_month = new JCustomComboBox<Integer>();
		sub_day = new JCustomComboBox<Integer>();
		
		rev_year.addActionListener(setActionListener(rev_day, rev_month, rev_year));
		rev_month.addActionListener(setActionListener(rev_day, rev_month, rev_year));
		
		au_year.addActionListener(setActionListener(au_day, au_month, au_year));
		au_month.addActionListener(setActionListener(au_day, au_month, au_year));
		
		con_year.addActionListener(setActionListener(con_day, con_month, con_year));
		con_month.addActionListener(setActionListener(con_day, con_month, con_year));
		
		sub_year.addActionListener(setActionListener(sub_day, sub_month, sub_year));
		sub_month.addActionListener(setActionListener(sub_day, sub_month, sub_year));
		

		fillYear(rev_year);
		fillMonth(rev_month);
//		fillDay(rev_day, rev_month.getSelectedIndex()+1, rev_year.getSelectedIndex()+1);
		

		fillYear(au_year);
		fillMonth(au_month);
//		fillDay(au_day, au_month.getSelectedIndex()+1, au_year.getSelectedIndex()+1);

		fillYear(con_year);
		fillMonth(con_month);
//		fillDay(con_day, con_month.getSelectedIndex()+1, con_year.getSelectedIndex()+1);

		fillYear(sub_year);
		fillMonth(sub_month);
//		fillDay(sub_day, sub_month.getSelectedIndex()+1, sub_year.getSelectedIndex()+1);
		
		rev_year.addActionListener(setActionListener(rev_day, rev_month, rev_year));
		rev_month.addActionListener(setActionListener(rev_day, rev_month, rev_year));
		
		au_year.addActionListener(setActionListener(au_day, au_month, au_year));
		au_month.addActionListener(setActionListener(au_day, au_month, au_year));
		
		con_year.addActionListener(setActionListener(con_day, con_month, con_year));
		con_month.addActionListener(setActionListener(con_day, con_month, con_year));
		
		sub_year.addActionListener(setActionListener(sub_day, sub_month, sub_year));
		sub_month.addActionListener(setActionListener(sub_day, sub_month, sub_year));

		setupComponents();

	}

	private ActionListener setActionListener(final JComboBox<Integer> day, final JComboBox<Integer> month, final JComboBox<Integer> year) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				fillDay(day, month.getSelectedIndex()+1,
						year.getSelectedIndex() + 1);
			}

		};
	}
	int user_id;

	private void setupComponents() {
		con_name.setEditable(true);

		finish.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ConferenceDB.createConference(con_name.getText(), user_id,
						new Date((Integer) con_month.getSelectedItem(),
								(Integer) con_day.getSelectedItem(),
								(Integer) con_year.getSelectedItem()),
						new Date((Integer) au_month.getSelectedItem(),
								(Integer) au_day.getSelectedItem(),
								(Integer) au_year.getSelectedItem()), new Date(
								(Integer) rev_month.getSelectedItem(),
								(Integer) rev_day.getSelectedItem(),
								(Integer) rev_year.getSelectedItem()),
						new Date((Integer) sub_month.getSelectedItem(),
								(Integer) sub_day.getSelectedItem(),
								(Integer) sub_year.getSelectedItem()));
				setChanged();
				notifyObservers();
			}

		});

		JPanel master_panel = new JPanel(new BorderLayout());
		JPanel rev_panel = new JPanel(new BorderLayout());
		JPanel au_panel = new JPanel(new BorderLayout());
		JPanel con_panel = new JPanel(new BorderLayout());
		JPanel sub_panel = new JPanel(new BorderLayout());
		JPanel name_panel = new JPanel(new BorderLayout());
		JPanel dates_panel = new JPanel(new GridLayout(0, 1));
		JPanel button_panel = new JPanel(new BorderLayout());
		JPanel PC_panel = new JPanel(new BorderLayout());

		rev_panel.add(new JCustomLabel("Reviewer deadline"), BorderLayout.WEST);
		JPanel rev = new JPanel();
		rev.add(rev_month, BorderLayout.WEST);
		rev.add(rev_day, BorderLayout.CENTER);
		rev.add(rev_year, BorderLayout.EAST);
		rev_panel.add(rev, BorderLayout.EAST);

		au_panel.add(new JCustomLabel("Author deadline"), BorderLayout.WEST);
		JPanel au = new JPanel();
		au.add(au_month, BorderLayout.CENTER);
		au.add(au_day, BorderLayout.CENTER);
		au.add(au_year, BorderLayout.EAST);
		au_panel.add(au, BorderLayout.EAST);

		con_panel.add(new JCustomLabel("Conference date"), BorderLayout.WEST);
		JPanel con = new JPanel();
		con.add(con_month, BorderLayout.WEST);
		con.add(con_day, BorderLayout.CENTER);
		con.add(con_year, BorderLayout.EAST);
		con_panel.add(con, BorderLayout.EAST);

		sub_panel.add(new JCustomLabel("Subprogram Chair deadline"),
				BorderLayout.WEST);
		JPanel sub = new JPanel();
		sub.add(sub_month, BorderLayout.WEST);
		sub.add(sub_day, BorderLayout.CENTER);
		sub.add(sub_year, BorderLayout.EAST);
		sub_panel.add(sub, BorderLayout.EAST);

		combo.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				finish.setEnabled(false);

			}

			@Override
			public void keyReleased(KeyEvent arg0) {

			}

			@Override
			public void keyTyped(KeyEvent arg0) {

			}
		});

		final JButton button = new JCustomButton("Search");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				for (User u : UserDB.getAllUsers()) {
					if (u.getUserName().equals(combo.getText())) {
						finish.setEnabled(true);
						user_id = u.getID();
					}
				}
				if (!finish.isEnabled())
					JDialogBox.createDialogBox(new Object[] {"Account not found!"}, null);
			}

		});
		PC_panel.add(new JCustomLabel("Program Chair "), BorderLayout.WEST);
		JPanel pc = new JPanel();
		pc.add(combo.getPanel(), BorderLayout.WEST);
		pc.add(button, BorderLayout.EAST);
		PC_panel.add(pc, BorderLayout.EAST);

		dates_panel.add(con_panel);
		dates_panel.add(au_panel);
		dates_panel.add(rev_panel);
		dates_panel.add(sub_panel);
		dates_panel.add(PC_panel);
		button_panel.add(finish, BorderLayout.EAST);

		name_panel.add(new JCustomLabel("Conference name "), BorderLayout.WEST);
		name_panel.add(con_name.getPanel(), BorderLayout.EAST);

		

		master_panel.add(name_panel, BorderLayout.NORTH);
		master_panel.add(dates_panel, BorderLayout.CENTER);
		master_panel.add(button_panel, BorderLayout.SOUTH);

		main.add(master_panel);

	}
	JPanel main = new JPanel();
	

	public JPanel display() {

		main.setVisible(true);
		return main;
	}

	private void fillDay(JComboBox<Integer> day2, int month, int year) {
		day2.removeAllItems();
		for (int i = 1; i < Date.getDays(month, year)+1; i++) {
			day2.addItem(i);
		}
//		day2.setSelectedItem(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

	}

	private void fillMonth(JComboBox<Integer> month2) {
		for (int x : month) {
			month2.addItem(x);
		}
//		month2.setSelectedItem(Calendar.getInstance().get(Calendar.MONTH) + 1);

	}

	private void fillYear(JComboBox<Integer> year2) {
		for (int x : year) {
			year2.addItem(x);
		}

	}

	private void populateDates() {
		for (int i = 0; i < year.length; i++) {
			year[i] = Calendar.getInstance().get(Calendar.YEAR) + i;
		}

		for (int i = 1; i <= day.length; i++) {
			day[i - 1] = i;
		}
		for (int i = 1; i <= month.length; i++) {
			month[i - 1] = i;
		}

	}

	public Component getPanel() {
		// TODO Auto-generated method stub
		return null;
	}

}
