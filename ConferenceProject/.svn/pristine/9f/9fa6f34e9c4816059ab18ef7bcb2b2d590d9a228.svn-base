package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import db.ConferenceUserDB;
import db.FeedbackDB;
import db.FeedbackUserDB;
import db.UserDB;
import db.UserRole;
import db.UserRoleFeedback;

import swing.custom.JCustomButton;
import swing.custom.JCustomLabel;
import swing.custom.JCustomTable;
import swing.custom.JDefaultFont;

import Model.Conference;
import Model.ConferenceRole;
import Model.Feedback;
import Model.User;

public class PaperGUI extends Observable implements GUI {
	private JPanel jp;
	private Conference my_conference;
	private User my_user;
	private ConferenceRole my_current_role;
	private JScrollPane scroll;
	private final JPanel center_panel;
	private Map<Feedback, List<UserRole>> contains = new HashMap<>();

	public PaperGUI(Conference conference, User user) {
		jp = new JPanel(new BorderLayout());
		my_conference = conference;
		my_user = user;
		my_current_role = user.getCurrentRole();
		JPanel center_parent = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTH;
		gbc.weighty = 1;
		gbc.weightx = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		center_panel = new JPanel();
		center_parent.add(center_panel, gbc);

		scroll = new JScrollPane(center_parent);
		createCenter(center_panel);

		if (my_current_role == ConferenceRole.SUBPROGRAM_CHAIR
				|| my_current_role == ConferenceRole.PROGRAM_CHAIR) {
			chairAssignPanel();
		} else {
			jp.add(scroll);
		}

	}

	private void chairAssignPanel() {

		JPanel assign_papers = new JPanel(new GridBagLayout());
		JLabel assign_paper = new JLabel("Assign Papers", JLabel.CENTER);
		assign_paper.setFont(new JDefaultFont().getFont(12));
		List<Feedback> list = FeedbackDB.getFeedbackList(my_conference.getID());
		int value = 0;
		if (list != null)
			value = list.size();
		Object[][] title_author = new Object[value][2];

		for (int i = 0; i < title_author.length; i++) {
			title_author[i][0] = list.get(i);
			title_author[i][1] = list.get(i).getAuthor();
		}
		JCustomTable<Feedback> feedbacks = new JCustomTable<>(new String[] {
				"Paper Title", "Author Name" }, title_author);
		JScrollPane scroll1 = new JScrollPane(feedbacks);
		scroll1.setPreferredSize(new Dimension(200, 100));

		Map<User, List<ConferenceRole>> users = ConferenceUserDB
				.getParticipants(my_conference.getID(), my_user.getID(),
						my_current_role.getRole());
		int value1 = 0;

		if (users != null)
			for (List<ConferenceRole> cr : users.values()) {
				for (@SuppressWarnings("unused")
				ConferenceRole r : cr)
					value1++;
			}

		Object[][] user_list = new Object[value1][3];

		int i = 0;
		for (User u : users.keySet()) {
			for (ConferenceRole role : users.get(u)) {
				user_list[i][0] = u;
				user_list[i][1] = role;
				user_list[i][2] = FeedbackUserDB.getFeedbackSize(
						my_conference.getID(), u.getID(), role.getRole(), my_user.getID());
				i++;
			}

		}

		JCustomTable<User> roles = new JCustomTable<>(new String[] {
				"User Name", "Role", "Assigned Papers" }, user_list);
		JScrollPane scroll2 = new JScrollPane(roles);
		scroll2.setPreferredSize(new Dimension(300, 100));

		JCustomButton assign_button = new JCustomButton("Assign Paper");
		assign_button.setEnabled(false);
		assign_button.addActionListener(buttonClick(assign_button, feedbacks,
				roles));
		feedbacks.getSelectionModel().addListSelectionListener(
				listener(assign_button, feedbacks, roles));
		roles.getSelectionModel().addListSelectionListener(
				listener(assign_button, feedbacks, roles));

		JPanel users_panel = new JPanel(new BorderLayout());
		users_panel.add(scroll2, BorderLayout.CENTER);
		users_panel.add(assign_button, BorderLayout.SOUTH);

		JPanel assignment_panel = new JPanel(new BorderLayout());
		assignment_panel.add(scroll1, BorderLayout.WEST);
		assignment_panel.add(users_panel, BorderLayout.EAST);
		JPanel centered_panel = new JPanel(new BorderLayout());
		centered_panel.add(assign_paper, BorderLayout.NORTH);
		centered_panel.add(assignment_panel, BorderLayout.CENTER);
		assign_papers.add(centered_panel, new GridBagConstraints());
		JScrollPane scroll3 = new JScrollPane(assign_papers);
		scroll3.setPreferredSize(new Dimension(200, 150));
		scroll.setPreferredSize(new Dimension(200, 200));
		JSplitPane jsp = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scroll3,
				scroll);

		jsp.setDividerSize(3);
		jsp.setContinuousLayout(true);

		jp.add(jsp);

	}

	@Override
	public JPanel display() {
		return jp;
	}

	
	private JPanel chairPanel(Feedback f, User user, ConferenceRole role)
	{
		JPanel panel = new JPanel(new BorderLayout());


		
		panel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY), new EmptyBorder(10,10,10,10)));
		
		JPanel paper_info = new JPanel(new BorderLayout());
		JTextPane jtp = new JTextPane();
		jtp.setPreferredSize(new Dimension(500, 100));
		jtp.setText("Abstract: " + f.getPaperAbstract());
		jtp.setEditable(false);
		jtp.setFont(new JDefaultFont().getFont(12));
		

		JButton button_1 = new JCustomButton("Approve");
		button_1.setFont(new JDefaultFont().getFont(16));
		JButton button_2 = new JCustomButton("Reject");
		button_2.setFont(new JDefaultFont().getFont(16));
		JButton button_3 = new JCustomButton("View");
		button_3.setFont(new JDefaultFont().getFont(16));

		JPanel approve = new JPanel(new BorderLayout());
		approve.add(button_1);
		approve.setBorder(new EmptyBorder(10,10,10,10));
		JPanel reject = new JPanel(new BorderLayout());
		reject.add(button_2);
		reject.setBorder(new EmptyBorder(10,10,10,10));
		JPanel view = new JPanel(new BorderLayout());
		view.add(button_3);
		view.setBorder(new EmptyBorder(10,10,10,10));

		
		JPanel left_panel = new JPanel();
		left_panel.setLayout(new BoxLayout(left_panel, BoxLayout.Y_AXIS));
		left_panel.add(new JCustomLabel("Author: " + f.getAuthor()));
		left_panel.add(new JCustomLabel("Title: " + f.getTitle()));
		left_panel.add(new JCustomLabel("Assigned to: " + user + " (" + role + ")"));
		left_panel.add(new JCustomLabel("Status: " + FeedbackUserDB.getStatus(f.getID(), user.getID(), my_conference.getID(), role.getRole())));
//		left_panel.add(new JCustomLabel("Deadline: " + my_conference.getConferenceDate()));
		paper_info.add(left_panel, BorderLayout.WEST);

		
		JPanel right_panel = new JPanel();
		right_panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridy = 0;

		right_panel.add(approve,gbc);
		right_panel.add(reject,gbc);
		right_panel.add(view,gbc);
		paper_info.add(right_panel, BorderLayout.EAST);
	
		panel.add(new JScrollPane(jtp), BorderLayout.SOUTH) ;

		panel.add(paper_info);
		return panel;
		
	}
	

	private void createCenter(final JPanel center_panel) {
		center_panel.setLayout(new BoxLayout(center_panel, BoxLayout.Y_AXIS));
		for (UserRoleFeedback f : FeedbackUserDB.getFeedbackFromUser(
				my_conference.getID(), my_user.getID())) {
			center_panel.add(chairPanel(FeedbackDB.getFeedback(f.getFeedback()), UserDB.getUser(f.getID()), ConferenceRole.translate(f.getRole())));
		}
	}

	private ListSelectionListener listener(final JCustomButton button,
			final JCustomTable<Feedback> feedback,
			final JCustomTable<User> my_users) {

		return new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if (my_users.getSelectedRow() != -1
						&& feedback.getSelectedRow() != -1) {

					User user = ((User) my_users.getValueAt(
							my_users.getSelectedRow(), 0));
					ConferenceRole role = ((ConferenceRole) my_users
							.getValueAt(my_users.getSelectedRow(), 1));
					Feedback fb = ((Feedback) feedback.getValueAt(
							feedback.getSelectedRow(), 0));
					button.setEnabled((int) my_users.getValueAt(
							my_users.getSelectedRow(), 2) < 4
							&& user.getID() != fb.getAuthorID()
							&& !FeedbackUserDB.getFeedbackFromUser(
									my_conference.getID(), user.getID(),
									role.getRole(), my_user.getID()).contains(fb));

				} else {
					button.setEnabled(false);
				}
			}

		};

	}

	private ActionListener buttonClick(final JCustomButton button,
			final JCustomTable<Feedback> feedback,
			final JCustomTable<User> my_users) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (my_users.getSelectedRow() != -1
						&& feedback.getSelectedRow() != -1) {
					Feedback fb = (Feedback) ((DefaultTableModel) feedback
							.getModel()).getValueAt(feedback.getSelectedRow(),
							0);
					User users = (User) ((DefaultTableModel) my_users
							.getModel()).getValueAt(my_users.getSelectedRow(),
							0);
					ConferenceRole role = (ConferenceRole) ((DefaultTableModel) my_users
							.getModel()).getValueAt(my_users.getSelectedRow(),
							1);
					my_user.assignPaper(my_conference, fb, users, role);
					
					((DefaultTableModel) my_users.getModel())
							.setValueAt(FeedbackUserDB.getFeedbackSize(
									my_conference.getID(), users.getID(), role.getRole(), my_user.getID()), my_users
									.getSelectedRow(), 2);
				
					button.setEnabled((int) my_users.getValueAt(
							my_users.getSelectedRow(), 2) < 4
							&& users.getID() != fb.getAuthorID()
							&& !FeedbackUserDB.getFeedbackFromUser(
									my_conference.getID(), users.getID(),
									role.getRole(), my_user.getID()).contains(fb)
									);
					center_panel.add(chairPanel(fb, users, role));

					center_panel.validate();
					center_panel.repaint();
					scroll.validate();
					scroll.repaint();

				} else {
					button.setEnabled(false);
				}
			}

		};
	}

}
