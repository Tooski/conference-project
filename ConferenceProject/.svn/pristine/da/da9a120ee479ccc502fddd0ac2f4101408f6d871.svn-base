package Model;

import java.util.ArrayList;

/*
 * Feedback rough draft
 * Author: Josh Keaton
 */
public class Feedback {

        public enum FeedbackStatus {
                APPROVED, PENDING, REJECTED
        }

        Paper my_manuscript;
        ArrayList<Review> my_reviews;
        int review_count; // may not be needed
        private FeedbackStatus subprogramchair_status;
        private FeedbackStatus programchair_status;
        private FeedbackStatus reviewer_status;
        private Conference my_conference;

        /**
         * Created a new Feedback object
         * 
         * @param author
         *            the author of the paper
         * @param title
         *            the title of the paper
         * @param the_paper
         *            the paper itself
         */
        public Feedback(String author, String title, String the_paper,
                        Conference the_conference) {
                my_manuscript = new Paper(title, author, the_paper);
                subprogramchair_status = FeedbackStatus.PENDING;// means just created
                programchair_status = FeedbackStatus.PENDING;
                reviewer_status = FeedbackStatus.PENDING;
                review_count = 0;
                my_conference = the_conference;
                my_reviews = new ArrayList<>();
        }

        /**
         * Allows a paper to be edited
         * 
         * @param new_paper
         *            the new paper
         * @return if the paper was successfully edited
         */
        public boolean editPaper(String new_paper) {
                return my_manuscript.editpaper(new_paper);
        }

        /**
         * Sets the status of the paper
         * 
         * @param new_status
         *            the new status of the paper
         * @return
         * @return if the status was updated properly
         */
        public boolean setStatus(ConferenceRole role, FeedbackStatus new_status) {
                switch (role) {
                case PROGRAM_CHAIR:
                        if (programchair_status == FeedbackStatus.PENDING)
                                programchair_status = new_status;
                        else
                                return false;
                        break;
                case SUBPROGRAM_CHAIR:
                        if (subprogramchair_status == FeedbackStatus.PENDING)
                                subprogramchair_status = new_status;
                        else
                                return false;
                        break;
                case REVIEWER:
                        if (reviewer_status == FeedbackStatus.PENDING)
                                reviewer_status = new_status;
                        else
                                return false;
                        break;
                default:
                        return false;
                }
                return true;
        }

        /**
         * Submits a review to the feedback object
         * 
         * @param reviewer
         *            the reviewer
         * @param review
         *            the review
         * @param conference_title
         *            the title of the conference
         * @return the index of the added review
         */
        public int submitReview(String reviewer, String review, int [] rating) {
                if (reviewer == null || review == null) {
                        return -1;// null parameters detected
                }
                for (Review r : getReviewList()) {
                        if (r.getReviewer().equals(reviewer)) {
                                return -1;
                        }
                }

                my_reviews.add(new Review(reviewer, review, my_conference.getConferenceName(), rating));
                review_count++;
                return my_reviews.size() - 1;// the index of this unique review
        }

        /**
         * Replaces a review at a specific index
         * 
         * @param my_actual_name
         *            the new review
         * @param the_new_review
         *            the index of the old review to replace
         * @return if the review was successfully replaced
         */
        public boolean editReview(String my_actual_name, String the_new_review) {
                int has_review = -1;
                for (int i = 0; i < getReviewList().size(); i++) {
                        if (getReviewList().get(i).getReviewer().equals(my_actual_name)) {
                                has_review = i;
                        }
                }
                if (my_actual_name == null || the_new_review == null
                                || has_review == -1) {
                        return false;
                }

                return my_reviews.get(has_review).editReviewComments(the_new_review);
        }

        /**
         * Deletes a Review at a specific index
         * 
         * @param my_actualname
         * 
         * @return if the review was deleted
         */
        public boolean deleteReview(String my_actualname) {
                int has_review = -1;
                for (int i = 0; i < getReviewList().size(); i++) {
                        if (getReviewList().get(i).getReviewer().equals(my_actualname)) {
                                has_review = i;
                        }
                }
                if (my_actualname == null || has_review == -1 || review_count == 0) {
                        return false;
                }
                my_reviews.remove(has_review);
                review_count--;
                return true;
        }

        public FeedbackStatus getStatus(ConferenceRole role) {
                switch (role) {
                case PROGRAM_CHAIR:
                        return programchair_status;
                case SUBPROGRAM_CHAIR:
                        return subprogramchair_status;
                case REVIEWER:
                        return reviewer_status;
                default:
                        return null;
                }
        }

        public Paper getPaper() {
                return my_manuscript;
        }

        public ArrayList<Review> getReviewList() {
                return my_reviews;
        }

        public Review getReview(int index) {
                return my_reviews.get(index);
        }

        public Conference getConference() {
                return my_conference;
        }

}
