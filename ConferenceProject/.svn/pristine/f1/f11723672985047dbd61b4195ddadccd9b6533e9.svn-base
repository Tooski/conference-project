/*
 * LoginUI.
 * Quan Le
 */
package View;

import java.awt.BorderLayout;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Model.User;
import View.ConferenceDBUI;

import db.UserDB;


@SuppressWarnings("serial")
public class LoginUI extends JPanel {
	/**
	 * The default width for text fields and text areas.
	 */
	private static final int TEXT_WIDTH = 15;

	private LoginUI login;
	
	/**
	 * Username text field.
	 */
	private final JTextField my_username_text;

	/**
	 * Password text field.
	 */
	private final JTextField my_password_text;

	/**
	 * A button used to push an element on the Stack.
	 */
	private final JButton my_login_button;
	
	private JFrame frame;
	

	public LoginUI() {
		super();
		my_username_text = new JTextField(TEXT_WIDTH);
		my_password_text = new JTextField(TEXT_WIDTH);
		my_login_button = new JButton("Login");
		login = this;
		frame = new JFrame();
		setupComponets();
	}

	/**
	 * Helper method to perform the work of setting up the GUI components.
	 */
	private void setupComponets() {
		my_login_button.addActionListener(new LoginListener());
		my_login_button.setMnemonic(KeyEvent.VK_L);
		final JButton create_button = new JButton("Create account?");
		create_button.setMnemonic(KeyEvent.VK_C);
		create_button.addActionListener(new CreateAccountListener());

		final JLabel username_label = new JLabel("Username: ");
		final JLabel password_label = new JLabel("Password: ");

		my_username_text.setEditable(true);
		my_password_text.setEditable(true);

		final JPanel master_panel = new JPanel(new BorderLayout());
		final JPanel login_panel = new JPanel(new BorderLayout());
		final JPanel username_panel = new JPanel(new FlowLayout());
		final JPanel pass_panel = new JPanel(new FlowLayout());

		username_panel.add(username_label);
		username_panel.add(my_username_text);

		pass_panel.add(password_label);
		pass_panel.add(my_password_text);

		login_panel.add(username_panel, BorderLayout.NORTH);
		login_panel.add(pass_panel, BorderLayout.CENTER);
		login_panel.add(my_login_button, BorderLayout.SOUTH);

		final JLabel label = new JLabel("Welcome to MSEE program.");
		label.setFont(new Font(label.getFont().getFontName(), Font.PLAIN, 20));

		master_panel.add(label, BorderLayout.NORTH);
		master_panel.add(login_panel, BorderLayout.CENTER);
		master_panel.add(create_button, BorderLayout.SOUTH);
		add(master_panel);


	}

	/**
	 * Creates and displays the application frame.
	 */
	public void display() {
		frame = new JFrame("Login");
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(this);

		//frame.pack(); 
		frame.setResizable(true);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		my_username_text.grabFocus();
		getRootPane().setDefaultButton(my_login_button);
	}

	/**
	 * An action listener for the login button.
	 */
	private class LoginListener implements ActionListener {
		public void actionPerformed(final ActionEvent the_event) {
			final String username = my_username_text.getText();
			final String password = my_password_text.getText();
			User user = UserDB.login(username, password);
			if (user != null) {		//Access the database instead of hard coding.
				
				JOptionPane.showMessageDialog(null, "Login successful!");
				frame.dispose();
				new ConferenceDBUI().display();
				
				
			} else {
				JOptionPane.showMessageDialog(null, "Username/password is incorrect.");
//				int option = JOptionPane.showConfirmDialog(null, "Username/password is incorrect.", "Error", JOptionPane.YES_NO_OPTION);
//				if (option == JOptionPane.YES_OPTION) {
//					new CreateAccountGUI().display();
//				}
			}
			

		}
	}

	/**
	 * An action listener for the create account button.
	 */
	private class CreateAccountListener implements ActionListener {
	
		public void actionPerformed(final ActionEvent the_event) {
			new CreateAccountGUI(login).display();
		}
	}
}
