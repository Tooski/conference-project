
package view;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import javax.swing.AbstractButton;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Conference;
import model.User;

import swing.custom.JCustomButton;
import swing.custom.JDefaultFont;
import view.paper.PaperGUI;

/**
 * OptionUI class.
 * 
 * @author Josef Nosov
 * @version 5/31/13
 */
public class OptionsUI extends Observable implements GUI {

  /**
   * The main panel.
   */
  private JPanel my_main_panel;

  /**
   * Constructor of OptionsUI class.
   * 
   * @param conf the conference
   * @param user the user
   */
  public OptionsUI(final Conference conf, final User user) {
    my_main_panel = new JPanel(new GridBagLayout());
    final JPanel jp = new JPanel(new GridLayout(2, 1));
    final JPanel users = new JPanel(new BorderLayout());
    final JPanel papers = new JPanel(new BorderLayout());
    users.setBorder(new EmptyBorder(10, 10, 10, 10));
    papers.setBorder(new EmptyBorder(10, 10, 10, 10));
    users.add(new JCustomButton("Users"));
    papers.add(new JCustomButton("Papers"));
    users.getComponent(0).setFont(new JDefaultFont().getFont(32));
    papers.getComponent(0).setFont(new JDefaultFont().getFont(32));
    ((AbstractButton) users.getComponent(0)).addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent the_arguament) {
        setChanged();
        notifyObservers(new UserGUI(conf, user));
      }
    });

    ((AbstractButton) papers.getComponent(0)).addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        setChanged();
        notifyObservers(new PaperGUI(conf, user));
      }
    });
    jp.add(users);
    jp.add(papers);
    my_main_panel.add(jp, new GridBagConstraints());

  }

  @Override
  public JPanel display() {
    my_main_panel.setVisible(true);
    return my_main_panel;
  }

}
