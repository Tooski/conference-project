package Model;

import java.util.ArrayList;

import db.FeedbackDB;

/*
 * Feedback rough draft
 * Author: Josh Keaton
 */
public class TempFeedback {

        public enum FeedbackStatus {
                APPROVED, PENDING, REJECTED
        }

        ArrayList<Review> my_reviews;

        
        private int my_feedback_id;



        
        public TempFeedback(int feedback_id) {
        	my_feedback_id = feedback_id;
        }

		/**
         * Allows a paper to be edited
         * 
         * @param new_paper
         *            the new paper
         * @return if the paper was successfully edited
         */
        public boolean editPaper(String new_paper) {
                return FeedbackDB.editPaper(my_feedback_id, new_paper);
        }

        /**
         * Sets the status of the paper
         * 
         * @param new_status
         *            the new status of the paper
         * @return
         * @return if the status was updated properly
         */
        public boolean setStatus(ConferenceRole role, FeedbackStatus new_status) {
        	return FeedbackDB.setStatus(my_feedback_id, role, new_status);

              
        }

        /**
         * Submits a review to the feedback object
         * 
         * @param reviewer
         *            the reviewer
         * @param review
         *            the review
         * @param conference_title
         *            the title of the conference
         * @return the index of the added review
         */
        public int submitReview(String reviewer, String review, int [] values) {
                if (reviewer == null || review == null) {
                        return -1;// null parameters detected
                }
                for (Review r : getReviewList()) {
                        if (r.getReviewer().equals(reviewer)) {
                                return -1;
                        }
                }

                my_reviews.add(new Review(reviewer, review, my_conference.getConferenceName(), values));
                return my_reviews.size() - 1;// the index of this unique review
        }

        /**
         * Replaces a review at a specific index
         * 
         * @param my_actual_name
         *            the new review
         * @param the_new_review
         *            the index of the old review to replace
         * @return if the review was successfully replaced
         */
        public boolean editReview(String my_actual_name, String the_new_review) {
                int has_review = -1;
                for (int i = 0; i < getReviewList().size(); i++) {
                        if (getReviewList().get(i).getReviewer().equals(my_actual_name)) {
                                has_review = i;
                        }
                }
                if (my_actual_name == null || the_new_review == null
                                || has_review == -1) {
                        return false;
                }

                return my_reviews.get(has_review).editReviewComments(the_new_review);
        }

        /**
         * Deletes a Review at a specific index
         * 
         * @param my_actualname
         * 
         * @return if the review was deleted
         */
        public boolean deleteReview(String my_actualname) {
                int has_review = -1;
                for (int i = 0; i < getReviewList().size(); i++) {
                        if (getReviewList().get(i).getReviewer().equals(my_actualname)) {
                                has_review = i;
                        }
                }
                if (my_actualname == null || has_review == -1) {
                        return false;
                }
                my_reviews.remove(has_review);
                return true;
        }

        public FeedbackStatus getStatus(ConferenceRole role) {
                switch (role) {
                case PROGRAM_CHAIR:
                        return programchair_status;
                case SUBPROGRAM_CHAIR:
                        return subprogramchair_status;
                case REVIEWER:
                        return reviewer_status;
                default:
                        return null;
                }
        }
        
    	public String getTitle() {
    		return FeedbackDB.getTitle(my_feedback_id);
    	}
    	
    	public String getAuthor() {
    		return FeedbackDB.getAuthor(my_feedback_id);
    	}
    	
    	public String getPaper() {
    		return FeedbackDB.getPaper(my_feedback_id);
    	}
    	

        public ArrayList<Review> getReviewList() {
                return my_reviews;
        }

        public Review getReview(int index) {
                return my_reviews.get(index);
        }

        public Conference getConference() {
                return FeedbackDB.getConference(my_feedback_id);
        }
        
        
        
        
        
        
        
        
        
        
        @Override
        public boolean equals(Object object)
        {
            boolean sameSame = false;

            if (object != null && object instanceof TempFeedback)
            {
                sameSame = this.my_feedback_id == ((TempFeedback) object).my_feedback_id;
            }

            return sameSame;
        }
        
        @Override
        public int hashCode()
        {
        	return my_feedback_id * 37 + toString().hashCode();
        }

}
