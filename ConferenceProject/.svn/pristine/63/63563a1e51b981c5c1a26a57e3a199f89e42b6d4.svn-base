package swing.custom;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicComboBoxUI;

/**
 * Creates a custom Combo Box for the MSEE conference.
 * 
 * @author Josef Nosov
 * @version 05/30/13
 */
@SuppressWarnings({ "serial", "unchecked", "rawtypes"})
public class JCustomComboBox extends JPanel {

	/**
	 * combobox.
	 */
	private JComboBox my_combobox;

	/**
	 * No parameters for this combobox.
	 * 
	 * @param the_combobox
	 *            are the objects being passed.
	 * @param the_default_index
	 *            is the index that will be shown first.
	 * @param the_boolean
	 *            checks to see if editable.
	 */
	public JCustomComboBox(final Integer the_default_index, boolean the_boolean) {
		super();
		setLayout(new BorderLayout());
		my_combobox = new JComboBox();

		my_combobox.setUI(new ComboBoxUI());
		add(my_combobox);
		setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		my_combobox.setFont(JDefaultFont.font(12));
		my_combobox.setFocusable(the_boolean);
		my_combobox.setEditable(the_boolean);
		UIManager.put("ComboBox.background", new ColorUIResource(Color.WHITE));

		my_combobox.setRenderer(customRender(my_combobox.getRenderer()));

	}

	private DefaultListCellRenderer customRender(final ListCellRenderer render) {
		DefaultListCellRenderer my_renderer = new DefaultListCellRenderer() {
			@Override
			public Component getListCellRendererComponent(JList list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				Component component = render.getListCellRendererComponent(list, value, index,
								isSelected, cellHasFocus);
				component.setForeground(Color.BLACK);
				component.setFont(JDefaultFont.font(12));

				if (index % 2 == 1) {
					component.setBackground(new Color(243, 246, 250));
				} else {
					component.setBackground(Color.WHITE);
				}

				if (isSelected) {
					component.setBackground(new Color(56, 117, 215));
					component.setForeground(Color.WHITE);
				}

				return component;
			}

		};
		return my_renderer;

	};

	public void setFontSize(int the_font_size) {
		my_combobox.setFont(JDefaultFont.font(the_font_size));
	}

	public JComboBox getComboBox() {
		return my_combobox;
	}

	/**
	 * Creates a new GUI for the combobox.
	 * 
	 * @author Joe
	 * 
	 */
	private static class ComboBoxUI extends BasicComboBoxUI {

		/**
		 * Creates a button that can move left and right.
		 * 
		 * @param the_component
		 *            is the component being passed.
		 * @return returns the new spinner.
		 */
		public static ComponentUI createUI(final JComponent the_component) {
			return new ComboBoxUI();
		}

		/**
		 * Creates the GUI.
		 * 
		 * @param the_component
		 *            the component that is being passed.
		 */
		public void installUI(final JComponent the_component) {
			super.installUI(the_component);
			arrowButton.setBorder(BorderFactory.createLineBorder(null));
			arrowButton.setBackground(Color.WHITE);
			final DefaultListCellRenderer cell_renderer = new DefaultListCellRenderer();
			cell_renderer.setHorizontalAlignment(DefaultListCellRenderer.CENTER);
			cell_renderer.setForeground(Color.WHITE);

			comboBox.setRenderer(cell_renderer);
			comboBox.setBorder(new EmptyBorder(1, 2, 0, 2));
			comboBox.setBackground(Color.WHITE);

			listBox.setBorder(new EmptyBorder(0, 2, 0, 2));
			listBox.setBackground(Color.WHITE);
		}

	}




}