package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Model.ConferenceRole;
import Model.Date;
import Model.Conference;
import Model.User;

public class ConferenceDB extends AbstractDB {

	static Map<Integer, Conference> my_conference = getConferences();

	static final String SELECT = "SELECT * FROM CONFERENCE WHERE CONFERENCE_ID = ?";
	static final String CREATE_USER = "INSERT INTO CONFERENCE(CONFERENCE_NAME, PROGRAMCHAIR_ID, CONFERENCE_DATE, AUTHOR_DEADLINE, REVIEWER_DEADLINE, SUBPROGRAM_DEADLINE) VALUES (?, ?, ?, ?, ?, ?)";
	private static String create = "create table if not exists CONFERENCE (CONFERENCE_ID INTEGER, CONFERENCE_NAME STRING, PROGRAMCHAIR_ID INTEGER, CONFERENCE_DATE INTEGER, AUTHOR_DEADLINE INTERGER, REVIEWER_DEADLINE INTERGER, SUBPROGRAM_DEADLINE INTERGER, PRIMARY KEY (CONFERENCE_ID ASC))";

	static {
		try {
			Connection conn = getConnection();
			conn.createStatement().executeUpdate(create);

			conn.close();
		} catch (Exception e) {
		}
	}

	public static int createConference(String conference_name,
			int programchair_id, Date conference_date, Date author_deadline,
			Date review_deadline, Date subprogram_deadline) {
		
		int conf_id = -1;
		try {
			Connection conn = getConnection();
			PreparedStatement pstmt = conn.prepareStatement(CREATE_USER);
			pstmt.setString(1, conference_name);
			pstmt.setInt(2, programchair_id);
			pstmt.setInt(3, conference_date.getDate());
			pstmt.setInt(4, author_deadline.getDate());
			pstmt.setInt(5, review_deadline.getDate());
			pstmt.setInt(6, subprogram_deadline.getDate());
			pstmt.executeUpdate();
			pstmt.close();

			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM CONFERENCE WHERE CONFERENCE_ID = (SELECT MAX(CONFERENCE_ID) FROM CONFERENCE)");
			ResultSet rd = st.executeQuery();

			while (rd.next()) {
				conf_id =  rd.getInt(1);
				if(!my_conference.containsKey(conf_id))
					my_conference.put(conf_id, new Conference(conf_id, conference_name,  programchair_id, conference_date, author_deadline, review_deadline, subprogram_deadline));
			}

			st.close();
			rd.close();
			conn.close();
			
			ConferenceUserDB.addUser(conf_id, new User(0, "",""), programchair_id, ConferenceRole.PROGRAM_CHAIR);

			return conf_id;
		} catch (Exception e) {
		}
		return conf_id;
	}

	private static Object query(int conference_id, String column) {
		Object obj = null;
		try {

			Connection conn = getConnection();
			
			PreparedStatement st = conn.prepareStatement("SELECT " + column
					+ " FROM CONFERENCE WHERE CONFERENCE_ID = ?");
			st.setInt(1, conference_id);
			ResultSet rd = st.executeQuery();
			if (rd.next()) {
				obj = rd.getObject(column);
			}

			st.close();
			rd.close();
			conn.close();
			return obj;
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public static User getProgramChair(int conference_id) {
		return UserDB.getUser((int) query(conference_id, "PROGRAMCHAIR_ID"));
	}
	
	public static String getConferenceName(int conference_id) {
		return String.valueOf(query(conference_id, "CONFERENCE_NAME"));
	}

	public static Date getConferenceDate(int conference_id) {
		int date = (int) query(conference_id, "CONFERENCE_DATE");
		return new Date((date % 10000) / 100, date % 100, date / 10000);
	}

	public static Date getAuthorDeadline(int conference_id) {
		int date = (int) query(conference_id, "AUTHOR_DEADLINE");
		return new Date((date % 10000) / 100, date % 100, date / 10000);
	}

	public static Date getReviewerDeadline(int conference_id) {
		int date = (int) query(conference_id, "REVIEWER_DEADLINE");
		return new Date((date % 10000) / 100, date % 100, date / 10000);
	}

	public static Date getSubProgramDeadline(int conference_id) {
		int date = (int) query(conference_id, "SUBPROGRAM_DEADLINE");
		return new Date((date % 10000) / 100, date % 100, date / 10000);
	}

	public static Conference getConference(int conference_id) {
		return my_conference.get(conference_id);
	}

	/**
	 * DELETE THIS IF YOU ARE NOT DEBUGGING
	 */
	public static void clearTable() {
		try {
			Connection conn = getConnection();

			conn.createStatement().executeUpdate(
					"drop table if exists CONFERENCE");
			my_conference.clear();
			conn.createStatement().executeUpdate(create);
			conn.close();

		} catch (SQLException e) {
		}
	}

	private static Map<Integer, Conference> getConferences() {
		Map<Integer, Conference> conferences = new HashMap<>();
		try {
			Connection conn = getConnection();
//			conn.createStatement().executeUpdate(create);

			PreparedStatement st;
			st = conn.prepareStatement("SELECT * FROM CONFERENCE");
			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				if(!conferences.containsKey(rd.getInt("CONFERENCE_ID")))
				conferences.put(rd.getInt("CONFERENCE_ID"), new Conference(rd.getInt("CONFERENCE_ID"), rd.getString("CONFERENCE_NAME"), rd.getInt("PROGRAMCHAIR_ID"),  rd.getInt("CONFERENCE_DATE"), rd.getInt("AUTHOR_DEADLINE"), rd.getInt("REVIEWER_DEADLINE"), rd.getInt("SUBPROGRAM_DEADLINE")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conferences;
	}

	public static Collection<Conference> getConferenceList() {
		return  my_conference.values();
	}

}