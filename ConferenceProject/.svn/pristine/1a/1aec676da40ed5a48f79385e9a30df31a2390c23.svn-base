package View;

/*
 * UserGUI.
 * Quan Le and Josef Nosov
 */
import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import db.ConferenceDB;
import db.ConferenceUserDB;
//import db.ConferenceUserDBTest;
import db.UserDB;

import swing.custom.JCustomButton;
import swing.custom.JCustomComboBox;
import swing.custom.JCustomLabel;
import swing.custom.JCustomList;
import swing.custom.JCustomTable;

import Model.Conference;
import Model.ConferenceRole;
import Model.User;

@SuppressWarnings("serial")
public class UserGUI extends Observable implements GUI {
	private ConferenceRole[] program_chair = { ConferenceRole.SUBPROGRAM_CHAIR,
			ConferenceRole.REVIEWER };

	private JComboBox<ConferenceRole> rolebox;

	/**
	 * Main frame.
	 */
	private final JFrame my_frame;

	/**
	 * A button used to push an element on the Stack.
	 */
	private final JCustomButton my_promote_button;

	private Collection<User> users;

	private JCustomList<User> user_list;

	private List<ConferenceRole> roles;

	private JCustomList<ConferenceRole> role_list;

	private User my_user;
	private final ConferenceRole current_role;


	// private int my_user_id;

	private Conference my_conference;

	/**
	 * UserGUI constructor.
	 */
	public UserGUI(final Conference the_conference, final User the_user) {
		super();
		my_frame = new JFrame("Users");


		my_user = the_user;
		current_role = my_user.getCurrentRole();
		my_conference = the_conference;
		rolebox = new JCustomComboBox<ConferenceRole>(program_chair);
		if (current_role == ConferenceRole.SUBPROGRAM_CHAIR)
			my_promote_button = new JCustomButton("Promote to Reviewer");
		else
			my_promote_button = new JCustomButton("Promote");
		my_promote_button.setEnabled(false);

		users = UserDB.getAllUsers();
		user_list = new JCustomList<>();

		user_list.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent arg0) {

						if (user_list.getSelectedIndex() != -1) {
							((DefaultListModel<ConferenceRole>) role_list
									.getModel()).removeAllElements();

							List<ConferenceRole> roles = ConferenceUserDB
									.getRole(the_conference.getID(), user_list
											.getSelectedValue().getID());
							for (ConferenceRole r : roles) {
								((DefaultListModel<ConferenceRole>) role_list
										.getModel()).addElement(r);
							}

							programChairCheck();
							subprogramChairCheck();
						} else
							my_promote_button.setEnabled(false);
					}

				});
		// roles = ConferenceUserDB.getRole(my_conference.getID(),
		// my_user.getID()); //NullPointerException
		// role_list = new JList<>(roles.toArray());
		role_list = new JCustomList<>();
		setupComponents();
		
		ConferenceUserDB.getParticipants(my_conference.getID(), the_user.getID(), current_role.getRole());
		for (User u : users) {
			if (!UserDB.isAdmin(u.getID()))
				((DefaultListModel<User>) user_list.getModel()).addElement(u);
		}

		rolebox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent arg0) {
				programChairCheck();
				subprogramChairCheck();
			}
		});
	}

	private void programChairCheck() {
		if (current_role == ConferenceRole.PROGRAM_CHAIR) {
			if ((ConferenceRole) rolebox.getSelectedItem() == ConferenceRole.SUBPROGRAM_CHAIR) {
				my_promote_button.setEnabled(false);
				boolean containsReviewer = false;
				boolean containsSPC = false;
				for (int i = 0; i < role_list.getModel().getSize(); i++) {
					if (role_list.getModel().getElementAt(i) == ConferenceRole.REVIEWER) {
						containsReviewer = true;
					} else if (role_list.getModel().getElementAt(i) == ConferenceRole.SUBPROGRAM_CHAIR) {
						containsSPC = true;
					}
				}
				my_promote_button.setEnabled(containsReviewer && !containsSPC);
			} else {
				my_promote_button.setEnabled(true);
				for (int i = 0; i < role_list.getModel().getSize(); i++) {
					if (role_list.getModel().getElementAt(i) == ConferenceRole.REVIEWER) {
						my_promote_button.setEnabled(false);
					}
				}
			}
		}
	}

	private void subprogramChairCheck() {
		if (current_role == ConferenceRole.SUBPROGRAM_CHAIR) {
			my_promote_button.setEnabled(true);
			for (int i = 0; i < role_list.getModel().getSize(); i++) {
				if (role_list.getModel().getElementAt(i) == ConferenceRole.REVIEWER) {
					my_promote_button.setEnabled(false);

				}
			}
		}
	}

	/**
	 * Helper method to perform the work of setting up the GUI components.
	 */
	private void setupComponents() {
		my_promote_button.setMnemonic(KeyEvent.VK_P);
		my_promote_button.addActionListener(new PromoteListener());

		// JScrollPane + JList for users.

		user_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// user_list.setLayoutOrientation(JList.VERTICAL);
		// user_list.setVisibleRowCount(-1);
		JScrollPane scroll_panel1 = new JScrollPane(user_list);
		scroll_panel1.setPreferredSize(new Dimension(150, 300));

		// JScrollPane + JList for roles.
		// Collection<ConferenceRole> roles =
		// UserDB.getUser(my_user_id).getRoles(my_conference);

		JScrollPane scroll_panel2 = new JScrollPane(role_list);
		scroll_panel2.setPreferredSize(new Dimension(150, 300));

		rolebox.setSelectedIndex(0);
		// rolelist.addActionListener(l);

		JPanel master_panel = new JPanel(new BorderLayout());
		JPanel empty_panel = new JPanel();
		JPanel button_panel = new JPanel(new FlowLayout());
		JPanel top = new JPanel(new BorderLayout());

		JCustomLabel usernames = new JCustomLabel("Usernames");
		JCustomLabel roles = new JCustomLabel("Roles");
		top.add(usernames, BorderLayout.WEST);
		top.add(roles, BorderLayout.EAST);

		if (current_role == ConferenceRole.PROGRAM_CHAIR)
			button_panel.add(rolebox);
		button_panel.add(my_promote_button);

		master_panel.add(top, BorderLayout.NORTH);
		master_panel.add(scroll_panel1, BorderLayout.WEST);
		master_panel.add(empty_panel, BorderLayout.CENTER);
		master_panel.add(scroll_panel2, BorderLayout.EAST);
		master_panel.add(button_panel, BorderLayout.SOUTH);
		panel.add(master_panel);
	}

	JPanel panel = new JPanel();

	/**
	 * Creates and displays the application frame.
	 */
	public JPanel display() {
		panel.setVisible(true);

		return panel;

	}

	private class PromoteListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {

			ConferenceRole selected_role = ConferenceRole.REVIEWER;
			if (current_role == ConferenceRole.PROGRAM_CHAIR) {
				selected_role = (ConferenceRole) rolebox.getSelectedItem();

			}

			my_user.promoteUser(my_conference, user_list.getSelectedValue(),
					selected_role);
			((DefaultListModel<ConferenceRole>) role_list.getModel())
					.addElement(selected_role);

			if (user_list.getSelectedValue().getID() == my_user.getID()) {
				setChanged();
				notifyObservers(selected_role);
			}

			programChairCheck();
			subprogramChairCheck();
		}

	}

	// @Override
	// public void update(Observable arg0, Object arg1) {
	// repaint();
	//
	// }

}
