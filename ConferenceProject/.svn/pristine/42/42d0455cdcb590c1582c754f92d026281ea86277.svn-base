package db;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Model.Conference;
import Model.Feedback;
import Model.Feedback.Status;

public class FeedbackUserDB extends AbstractDB {

	static Map<UserRole, List<Integer>> my_user_feedback = getFeedbackUsers();

	static final String SELECT = "SELECT * FROM FEEDBACK_USER WHERE FEEDBACK_ID = ?";
	static final String CREATE_USER = "INSERT INTO FEEDBACK_USER(FEEDBACK_ID, USER_ID, USER_ROLE INTEGER, STATUS, REVIEW, SCORES) VALUES (?, ?, ?, ?, ?, ?)";
	private static String create = "create table if not exists FEEDBACK_USER (FEEDBACK_ID INTEGER, USER_ID INTEGER, USER_ROLE INTEGER, STATUS INTEGER, REVIEW STRING, SCORES BLOB)";

	static {
		try {
			Connection conn = getConnection();
			conn.createStatement().executeUpdate(create);

			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean assignFeedback(int feedback_id, int user_id, int role) {
		try {
			Connection conn = getConnection();
			if (feedbackuserExists(conn, feedback_id, user_id))
				return false;

			PreparedStatement pstmt = conn
					.prepareStatement("INSERT INTO FEEDBACK_USER(FEEDBACK_ID, USER_ID, USER_ROLE) VALUES (?, ?, ?)");
			pstmt.setInt(1, feedback_id);
			pstmt.setInt(2, user_id);
			pstmt.setInt(3, role);

			pstmt.executeUpdate();
			pstmt.close();
			conn.close();

			UserRole r = new UserRole(user_id, role);

			if (!my_user_feedback.containsKey(r)) {
				List<Integer> feedbacks = new ArrayList<>();
				feedbacks.add(feedback_id);
				my_user_feedback.put(r, feedbacks);
			} else {
				if (!my_user_feedback.get(r).contains(feedback_id))
					my_user_feedback.get(r).add(feedback_id);
			}

			return true;
		} catch (Exception e) {
		}
		return false;
	}

	private static boolean feedbackuserExists(Connection conn, int feedback_id,
			int user_id) throws SQLException {
		boolean name_exists = false;
		PreparedStatement st = conn
				.prepareStatement("SELECT FEEDBACK_ID AND USER_ID FROM FEEDBACK_USER WHERE FEEDBACK_ID = "
						+ feedback_id + " AND USER_ID = " + user_id);
		ResultSet rd = st.executeQuery();
		if (rd.next() && rd.getInt(1) == feedback_id && rd.getInt(2) == user_id) {
			name_exists = true;
			conn.close();
		}
		rd.close();
		st.close();
		return name_exists;

	}

	private static Map<UserRole, List<Integer>> getFeedbackUsers() {
		Map<UserRole, List<Integer>> my_users = new HashMap<>();

		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM FEEDBACK_USER");
			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				UserRole r = new UserRole(rd.getInt(2), rd.getInt(3));
				if (!my_users.containsKey(r)) {
					List<Integer> users = new ArrayList<>();
					users.add(rd.getInt(1));
					my_users.put(r, users);
				} else {
					if (!my_users.get(r).contains(rd.getInt(1)))
						my_users.get(r).add(rd.getInt(1));
				}

			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return my_users;
	}

	public static Status getStatus(int my_feedback_id, int user_id) {
		Status status = null;
		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT STATUS FROM FEEDBACK_USER WHERE FEEDBACK_ID = "
							+ my_feedback_id + " AND USER_ID = " + user_id);
			ResultSet rd = st.executeQuery();
			if (rd.next()) {
				switch (rd.getInt(1)) {
				case -1:
					status = Status.REJECTED;
					break;
				case 0:
					status = Status.PENDING;
					break;
				case 1:
					status = Status.APPROVED;
					break;
				}
			}
			rd.close();
			st.close();
			conn.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static boolean setReview(int my_feedback_id, int user_id,
			String review, int[] values) {
		try {
			Connection conn = getConnection();
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(Integer.toString(my_feedback_id * 37
							+ user_id * 19)));
			oos.writeObject(values);
			oos.flush();
			PreparedStatement st = conn
					.prepareStatement("UPDATE FEEDBACK_USER SET REVIEW = '"
							+ review + "', SCORES = '" + oos
							+ "' WHERE FEEDBACK_ID = " + my_feedback_id
							+ " AND USER_ID = " + user_id);
			st.executeUpdate();
			oos.close();
			st.close();
			conn.close();
			return true;

		} catch (Exception e) {
		}

		return false;
	}

	public static boolean editReview(int my_feedback_id, int user_id,
			String review) {
		try {

			Connection conn = getConnection();

			PreparedStatement st = conn
					.prepareStatement("UPDATE FEEDBACK_USER SET REVIEW = '"
							+ review + "' WHERE FEEDBACK_ID = "
							+ my_feedback_id + " AND USER_ID = " + user_id);
			st.executeUpdate();
			st.close();
			conn.close();
			return true;

		} catch (Exception e) {
		}

		return false;
	}

	public static String getReview(int my_feedback_id, int user_id) {
		String the_review = null;
		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT REVIEW FROM FEEDBACK_USER WHERE FEEDBACK_ID = "
							+ my_feedback_id + " AND USER_ID = " + user_id);
			ResultSet rd = st.executeQuery();
			if (rd.next()) {
				the_review = rd.getString(1);
			}
			st.close();
			rd.close();
			conn.close();

		} catch (Exception e) {
		}

		return the_review;
	}

	/**
	 * Reads objects from this locations into an array.
	 */
	public static int[] getScores(int my_feedback_id, int user_id) {
		int[] readAllValues = null;

		try {
			Connection conn = getConnection();

			PreparedStatement pstmt = conn
					.prepareStatement("SELECT SCORES FROM FEEDBACK_USER WHERE FEEDBACK_ID ="
							+ my_feedback_id + " AND USER_ID = " + user_id);

			ResultSet rd = pstmt.executeQuery();
			if (rd.next()) {

				ObjectInputStream oos = new ObjectInputStream(
						new FileInputStream(Integer.toString(my_feedback_id
								* 37 + user_id * 19)));
				readAllValues = (int[]) oos.readObject();
				oos.close();
			}
			rd.close();
			pstmt.close();
		} catch (Exception e) {

		}
		return readAllValues;
	}

	public static boolean setStatus(int my_feedback_id, int user_id,
			Status new_status) {
		boolean isAlterable = false;
		try {

			int status = getStatus(my_feedback_id, user_id).getValue();
			Connection conn = getConnection();

			if (status == 0) {
				PreparedStatement st = conn
						.prepareStatement("UPDATE FEEDBACK_USER SET STATUS = '"
								+ new_status.getValue()
								+ "' WHERE FEEDBACK_ID = " + my_feedback_id
								+ " AND USER_ID = " + user_id);
				st.executeUpdate();
				st.close();
				isAlterable = true;
			}

			conn.close();

		} catch (Exception e) {
		}

		return isAlterable;
	}

	public static List<Feedback> getUserFeedback(int user_id) {
		List<Feedback> user_feedbacks = new ArrayList<>();
		if (my_user_feedback.containsKey(user_id))
			for (int i : my_user_feedback.get(user_id)) {
				user_feedbacks.add(FeedbackDB.getFeedback(i));
			}

		return user_feedbacks;
	}

	/**
	 * DELETE THIS IF YOU ARE NOT DEBUGGING
	 */
	public static void clearTable() {
		try {
			Connection conn = getConnection();
			conn.createStatement().executeUpdate(
					"drop table if exists FEEDBACK_USER");
			conn.createStatement().executeUpdate(create);
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Feedback> getFeedbackFromUser(int conference_id,
			int user_id, int role) {
		List<Feedback> feedback = new ArrayList<>();
		UserRole r = new UserRole(user_id, role);
		if (my_user_feedback.containsKey(r))
			for (int i : my_user_feedback.get(r)) {
				Feedback fb = FeedbackDB.getFeedback(i);
				if (fb != null) {
					if (fb.getConference().getID() == conference_id)
						feedback.add(fb);
				}
			}

		return feedback;

	}

	public static int getFeedbackSize(int conference_id, int user_id, int role) {
		int z = 0;
		UserRole r = new UserRole(user_id, role);
		if (my_user_feedback.containsKey(r))
			for (int i : my_user_feedback.get(r)) {
				Feedback fb = FeedbackDB.getFeedback(i);

				if (fb != null)
					if (fb.getConference().getID() == conference_id)
						z++;
			}

		return z;

	}

}
