package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import Model.Admin;
import Model.User;
import Model.User;

public class UserDB extends AbstractDB {

	static final String AUTHENTICATION = "SELECT * FROM ACCOUNT WHERE USERNAME = ? AND PASSWORD = ?";
	static final String CREATE_USER = "INSERT INTO ACCOUNT(USERNAME, PASSWORD, NAME, IS_ADMIN) VALUES (?, ?, ?, ?)";
	static String create = "create table if not exists ACCOUNT (ID INTEGER, USERNAME STRING, PASSWORD STRING, NAME STRING, IS_ADMIN BOOLEAN,  PRIMARY KEY (ID ASC))";

	private static Map<Integer, User> user_id = getUsers();

	public static User getUser(int user) {
		return user_id.get(user);
	}

	public static Collection<User> getAllUsers() {
		return user_id.values();
	}

	static {
		try {
			Connection conn = getConnection();
			conn.createStatement().executeUpdate(create);
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int createUser(String username, String password, String name) {
		if (username.equals(null) || username.length() == 0
				|| password.equals(null) || password.length() == 0
				|| name.equals(null) || name.length() == 0) {
			return 1;
		}

		try {
			Connection conn = getConnection();
			if (usernameExists(conn, username))
				return 2;
			PreparedStatement pstmt = conn.prepareStatement(CREATE_USER);
			pstmt.setString(1, username);
			pstmt.setString(2, password);
			pstmt.setString(3, name);
			if (username.toLowerCase().equals("admin"))
				pstmt.setBoolean(4, true);
			else
				pstmt.setBoolean(4, false);
			pstmt.executeUpdate();
			pstmt.close();
			
			PreparedStatement st = conn.prepareStatement("SELECT * FROM ACCOUNT WHERE USERNAME = ? AND PASSWORD = ? AND NAME = ?");
			st.setString(1, username);
			ResultSet rd = st.executeQuery();
			if (rd.next()) {
				if (rd.getInt("IS_ADMIN") == 1)
					user_id.put(rd.getInt("ID"),new Admin(rd.getInt("ID"), username, name));
				else
					user_id.put(rd.getInt("ID"),new User(rd.getInt("ID"), username, name));

			}
			rd.close();
			st.close();
			conn.close();

			return 0;
		} catch (Exception e) {
		}
		return 3;

	}

	private static boolean usernameExists(Connection conn, String username)
			throws SQLException {
		boolean name_exists = false;
		PreparedStatement st = conn
				.prepareStatement("SELECT * FROM ACCOUNT WHERE USERNAME = ?");
		st.setString(1, username);
		ResultSet rd = st.executeQuery();
		
		if (rd.next() && rd.getString("USERNAME").equals(username)) {
			name_exists = true;
			conn.close();
			
		} 

		rd.close();
		st.close();

		return name_exists;

	}

	public static User login(String username, String password) {
		User user = null;
		try {
			Connection conn = getConnection();
			PreparedStatement st = conn.prepareStatement(AUTHENTICATION);
			st.setString(1, username);
			st.setString(2, password);
			ResultSet rd = st.executeQuery();
			if (rd.next()) {
				if (rd.getInt("IS_ADMIN") == 1)
					user = new Admin(rd.getInt("ID"), username, rd.getString("NAME"));
				else
					user = new User(rd.getInt("ID"), username, rd.getString("NAME"));
				user_id.put(rd.getInt("ID"), user);

			}
			rd.close();
			st.close();

			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	static Map<Integer, User> getUsers() {
		Map<Integer, User> my_users = new HashMap<>();

		try {
			Connection conn = getConnection();

			conn.createStatement().executeUpdate(create);

			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM ACCOUNT");
			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				if (rd.getInt("IS_ADMIN") == 1)
					my_users.put(rd.getInt(1), new Admin(rd.getInt(1), rd.getString("USERNAME"), rd.getString("NAME")));
				else
					my_users.put(rd.getInt(1), new User(rd.getInt(1), rd.getString("USERNAME"), rd.getString("NAME")));
			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return my_users;

	}

	public static String getActualName(int user_id) {
		String name = null;
		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT NAME FROM ACCOUNT WHERE ID = "
							+ user_id);
			ResultSet rd = st.executeQuery();
			if (rd.next()) {
				name = rd.getString(1);
			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return name;

	}

	/**
	 * DELETE THIS IF YOU ARE NOT DEBUGGING
	 */
	public static void clearTable() {
		try {
			Connection conn = getConnection();

			conn.createStatement()
					.executeUpdate("drop table if exists ACCOUNT");

			conn.createStatement().executeUpdate(create);
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static boolean isAdmin(int my_id) {
		boolean b = false;
		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT IS_ADMIN FROM ACCOUNT WHERE ID = "
							+ my_id);
			ResultSet rd = st.executeQuery();
			if (rd.next()) {
				b = rd.getBoolean(1);
			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}

	public static String getUserName(int my_id) {
		String name = null;
		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT USERNAME FROM ACCOUNT WHERE ID = "
							+ my_id);
			ResultSet rd = st.executeQuery();
			if (rd.next()) {
				name = rd.getString(1);
			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return name;
	}

}