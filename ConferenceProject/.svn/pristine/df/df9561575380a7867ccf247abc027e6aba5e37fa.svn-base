package Model;
import java.util.ArrayList;

/*
 * Feedback rough draft
 * Author: Josh Keaton
 */
public class Feedback {

	Paper my_manuscript;
	ArrayList<Review> my_reviews;
	int review_count; //may not be needed
	int my_status;
	private Conference my_conference;
	
	/**
	 * Created a new Feedback object
	 * 
	 * @param author the author of the paper
	 * @param title the title of the paper
	 * @param the_paper the paper itself
	 */
	public Feedback(String author, String title, String the_paper, Conference the_conference) {
		my_manuscript = new Paper(title,author,the_paper);
		my_status = 0;//means just created
		review_count = 0;
		my_conference = the_conference;
	}
	
	/**
	 * Allows a paper to be edited
	 * @param new_paper the new paper
	 * @return if the paper was successfully edited
	 */
	public boolean editPaper(String new_paper) {
		return my_manuscript.editpaper(new_paper);
	}
	
	/**
	 * Sets the status of the paper
	 * @param new_status the new status of the paper
	 * @return if the status was updated properly
	 */
	public boolean setStatus(int new_status) {
		if(new_status < my_status) {
			return false; //new status cannot be less than current status
		}
		my_status = new_status;
		
		return true;
	}
	
	/**
	 * Submits a review to the feedback object
	 * 
	 * @param reviewer the reviewer
	 * @param review the review
	 * @param conference_title the title of the conference
	 * @return the index of the added review
	 */
	public int submitReview(String reviewer, String review) {
		if(reviewer == null || review == null) {
			return -1;//null parameters detected
		}
		my_reviews.add(new Review(reviewer, review, my_conference.getConferenceName()));
		review_count++;
		return review_count-1;//the index of this unique review
	}
	
	/**
	 * Replaces a review at a specific index
	 * @param review the new review
	 * @param review_index the index of the old review to replace
	 * @return if the review was successfully replaced
	 */
	public boolean editReview(String review, int review_index) {
		if(review == null || review_index < 0) {
			return false;
		}
		return my_reviews.get(review_index).editReview(review);
	}
	
	/**
	 * Deletes a Review at a specific index
	 * 
	 * @param review_index the index of the review to delete
	 * @return if the review was deleted
	 */
	public boolean deleteReview(int review_index) {
		if(review_index < 0) {
			return false;
		}
		my_reviews.remove(review_index);
		review_count--;
		return true;
	}
	
	public int getStatus() {
		return my_status;
	}
	
	public Paper getPaper() {
		return my_manuscript;
	}
	
	public ArrayList<Review> getReviewList() {
		return my_reviews;
	}
	
	public Review getReview(int index) {
		return my_reviews.get(index);
	}

	
	public Conference getConference()
	{
		return my_conference;
	}
	
}
