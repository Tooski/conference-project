package Tests.TestJoe;

import static org.junit.Assert.*;

import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

import Model.Admin;
import Model.Conference;
import Model.ConferenceRole;
import Model.Feedback;
import Model.Feedback.FeedbackStatus;
import Model.ProgramChair;
import Model.Reviewer;
import Model.SubProgramChair;

public class SubProgramChairTest {


	private Admin my_admin;
	private ProgramChair my_programchair;
	private SubProgramChair my_spc;
	private Reviewer my_reviewer;
	private Conference my_conference;
	
	
	@Before
	public void setUp() throws Exception {
		my_admin = new Admin("Guy52", "Guy Smith",
				"Password52");
		my_programchair = new ProgramChair("Johnny52", "John Smith",
				"Password52");
		my_admin.getListOfConferences().clear();
		my_admin.createConference("New conference", new GregorianCalendar(),
				my_programchair);
		my_conference = my_admin.getListOfConferences().get(0);
		my_reviewer = new Reviewer("ReviewerMan", "Reviewy", "Cool123");
		my_spc = new SubProgramChair("SubProgramGuy", "Subby", "Run112");

		my_conference.createFeedback("Author", "Text", "Other");
	}


	@Test
	public void testAssignPaper() {
		assertTrue(my_spc.promoteUser(my_conference, my_reviewer));
		assertTrue(my_reviewer.getFeedback().size() == 0);
		assertTrue(my_spc.assignPaper(my_spc.getListOfConferences().get(0).getFeedbackList().get(0), my_reviewer));
		assertTrue(my_reviewer.getFeedback().size() == 1);
		assertFalse(my_spc.assignPaper(null, my_reviewer));
		assertTrue(my_reviewer.getFeedback().size() == 1);
		assertFalse(my_spc.assignPaper(my_spc.getListOfConferences().get(0).getFeedbackList().get(0), null));
		assertTrue(my_reviewer.getFeedback().size() == 1);
		assertFalse(my_spc.assignPaper(null, null));
		assertTrue(my_reviewer.getFeedback().size() == 1);

	}

	@Test
	public void testSubmitRecommendation() {
		Feedback fb = my_spc.getListOfConferences().get(0).getFeedbackList().get(0);
		assertTrue(my_programchair.promoteUser(my_spc.getListOfConferences().get(0), my_spc));
		assertTrue(fb.getStatus(ConferenceRole.SUBPROGRAM_CHAIR) == FeedbackStatus.PENDING);
		assertTrue(my_spc.submitRecommendation(fb));
		assertTrue(fb.getStatus(ConferenceRole.SUBPROGRAM_CHAIR) == FeedbackStatus.APPROVED);
		assertTrue(fb.getStatus(ConferenceRole.PROGRAM_CHAIR) == FeedbackStatus.PENDING);
		assertTrue(fb.getStatus(ConferenceRole.REVIEWER) == FeedbackStatus.PENDING);
		assertFalse(my_spc.submitRecommendation(null));


	}

	@Test
	public void testPromoteUser() {
		assertTrue(my_spc.promoteUser(my_conference, my_reviewer));
		assertFalse(my_spc.promoteUser(my_conference, my_reviewer));
		assertFalse(my_spc.promoteUser(my_conference, my_programchair));

		assertFalse(my_spc.promoteUser(null, my_reviewer));
		assertFalse(my_spc.promoteUser(my_conference, null));
		assertFalse(my_spc.promoteUser(null, null));


	}

	@Test
	public void testGetStatus() {
		assertTrue(my_spc.promoteUser(my_conference, my_reviewer));
		assertTrue(my_spc.getStatus(my_reviewer).size() == 0);
		assertTrue(my_spc.assignPaper(my_spc.getListOfConferences().get(0).getFeedbackList().get(0), my_reviewer));

		assertTrue(my_spc.getStatus(my_reviewer).size() == 1);
		assertNull(my_spc.getStatus(null));

	}

}
