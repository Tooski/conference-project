package db;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Model.Conference;
import Model.ConferenceRole;
import Model.Feedback;
import Model.Feedback.Status;

public class FeedbackUserDB extends AbstractDB {

	static Map<UserRole, List<Integer>> my_user_feedback = getFeedbackUsers();
	static Map<Integer, List<UserRole>> my_feedback_user = getUsersFeedback();

	static final String SELECT = "SELECT * FROM FEEDBACK_USER WHERE FEEDBACK_ID = ?";
	static final String CREATE_USER = "INSERT INTO FEEDBACK_USER(CONFERENCE_ID, FEEDBACK_ID, ASSIGNED_BY, ASSIGNED_ROLE, USER_ID, USER_ROLE INTEGER, STATUS, REVIEW, SCORES) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static String create = "create table if not exists FEEDBACK_USER (CONFERENCE_ID INTEGER, FEEDBACK_ID INTEGER, ASSIGNED_BY INTEGER, ASSIGNED_ROLE INTEGER, USER_ID INTEGER, USER_ROLE INTEGER, STATUS INTEGER, REVIEW STRING, SCORES BLOB)";

	static {
		try {
			Connection conn = getConnection();
			conn.createStatement().executeUpdate(create);

			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean assignFeedback(int conference_id, int feedback_id,
			int assigned_id, int assigned_role, int user_id, int role) {
		try {
			Connection conn = getConnection();
			if (feedbackuserExists(conn, conference_id, feedback_id, user_id,
					role, assigned_id, assigned_role))
				return false;

			PreparedStatement pstmt = conn
					.prepareStatement("INSERT INTO FEEDBACK_USER(CONFERENCE_ID, FEEDBACK_ID, ASSIGNED_BY, ASSIGNED_ROLE, USER_ID, USER_ROLE, STATUS) VALUES (?, ?, ?, ?, ?, ?, ?)");
			pstmt.setInt(1, conference_id);
			pstmt.setInt(2, feedback_id);
			pstmt.setInt(3, assigned_id);
			pstmt.setInt(4, assigned_role);

			pstmt.setInt(5, user_id);
			pstmt.setInt(6, role);
			
			if(role == ConferenceRole.SUBPROGRAM_CHAIR.getRole())
				pstmt.setInt(7, Status.ASSIGNED_TO_SUBPROGRAM_CHAIR.getValue());
			else if (role == ConferenceRole.REVIEWER.getRole())
				pstmt.setInt(7, Status.ASSIGNED_TO_REVIEWER.getValue());

			pstmt.executeUpdate();
			pstmt.close();
			conn.close();

			UserRole r = new UserRole(user_id, role, assigned_id);

			if (!my_user_feedback.containsKey(r)) {
				List<Integer> feedbacks = new ArrayList<>();
				feedbacks.add(feedback_id);
				my_user_feedback.put(r, feedbacks);
			} else {
				if (!my_user_feedback.get(r).contains(feedback_id))
					my_user_feedback.get(r).add(feedback_id);
			}

			if (!my_feedback_user.containsKey(feedback_id)) {
				List<UserRole> users = new ArrayList<>();
				users.add(r);
				my_feedback_user.put(feedback_id, users);
			} else {
				if (!my_feedback_user.get(feedback_id).contains(r))
					my_feedback_user.get(feedback_id).add(r);
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private static boolean feedbackuserExists(Connection conn,
			int conference_id, int feedback_id, int user_id, int role,
			int assigned_id, int assigned_role) throws SQLException {
		boolean name_exists = false;
		PreparedStatement st = conn
				.prepareStatement("SELECT * FROM FEEDBACK_USER WHERE CONFERENCE_ID = ? AND FEEDBACK_ID = ? AND USER_ID = ? AND USER_ROLE = ? AND ASSIGNED_BY = ? AND ASSIGNED_ROLE = ?");
		st.setInt(1, conference_id);

		st.setInt(2, feedback_id);
		st.setInt(3, user_id);

		ResultSet rd = st.executeQuery();
		if (rd.next() && rd.getInt("CONFERENCE_ID") == conference_id
				&& rd.getInt("FEEDBACK_ID") == feedback_id
				&& rd.getInt("USER_ID") == user_id
				&& rd.getInt("USER_ID") == role
				&& rd.getInt("ASSIGNED_BY") == assigned_id
				&& rd.getInt("ASSIGNED_ROLE") == assigned_role) {
			name_exists = true;
			conn.close();
		}
		rd.close();
		st.close();
		return name_exists;

	}

	private static Map<UserRole, List<Integer>> getFeedbackUsers() {
		Map<UserRole, List<Integer>> my_users = new HashMap<>();

		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM FEEDBACK_USER");
			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				UserRole r = new UserRole(rd.getInt("USER_ID"),
						rd.getInt("USER_ROLE"), rd.getInt("ASSIGNED_BY"));
				if (!my_users.containsKey(r)) {
					List<Integer> users = new ArrayList<>();
					users.add(rd.getInt("FEEDBACK_ID"));
					my_users.put(r, users);
				} else {
					if (!my_users.get(r).contains(rd.getInt("FEEDBACK_ID")))
						my_users.get(r).add(rd.getInt("FEEDBACK_ID"));
				}

			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return my_users;
	}

	private static Map<Integer, List<UserRole>> getUsersFeedback() {
		Map<Integer, List<UserRole>> my_users = new HashMap<>();

		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM FEEDBACK_USER");
			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				UserRole r = new UserRole(rd.getInt("USER_ID"),
						rd.getInt("USER_ROLE"), rd.getInt("ASSIGNED_BY"));
				if (!my_users.containsKey(rd.getInt("FEEDBACK_ID"))) {
					List<UserRole> users = new ArrayList<>();
					users.add(r);
					my_users.put(rd.getInt("FEEDBACK_ID"), users);
				} else {
					if (!my_users.get(rd.getInt("FEEDBACK_ID")).contains(r))
						my_users.get(rd.getInt("FEEDBACK_ID")).add(r);
				}
			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return my_users;
	}

	public static Status getStatus(int my_feedback_id, int user_id,
			int conference_id, int user_role) {
		Status status = null;
		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM FEEDBACK_USER WHERE FEEDBACK_ID = "
							+ my_feedback_id
							+ " AND USER_ID = "
							+ user_id
							+ " AND CONFERENCE_ID = "
							+ conference_id
							+ " AND USER_ROLE = " + user_role);
			ResultSet rd = st.executeQuery();
			if (rd.next()) {

				status = Status.translate(rd.getInt("STATUS"));

			}
			rd.close();
			st.close();
			conn.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static boolean setReview(int my_feedback_id, int user_id,
			String review, int[] values, int conference_id, int user_role) {
		try {
			Connection conn = getConnection();
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream(Integer.toString(my_feedback_id * 37
							+ user_id * 19)));
			oos.writeObject(values);
			oos.flush();
			PreparedStatement st = conn
					.prepareStatement("UPDATE FEEDBACK_USER SET REVIEW = '"
							+ review + "', SCORES = '" + oos
							+ "' WHERE FEEDBACK_ID = " + my_feedback_id
							+ " AND USER_ID = " + user_id
							+ " AND CONFERENCE_ID = " + conference_id
							+ " AND USER_ROLE = " + user_role);
			st.executeUpdate();
			oos.close();
			st.close();
			conn.close();
			return true;

		} catch (Exception e) {
		}

		return false;
	}

	public static boolean editReview(int my_feedback_id, int user_id,
			String review, int conference_id, int user_role) {
		try {

			Connection conn = getConnection();

			PreparedStatement st = conn
					.prepareStatement("UPDATE FEEDBACK_USER SET REVIEW = '"
							+ review + "' WHERE FEEDBACK_ID = "
							+ my_feedback_id + " AND USER_ID = " + user_id
							+ " AND CONFERENCE_ID = " + conference_id
							+ " AND USER_ROLE = " + user_role);
			st.executeUpdate();
			st.close();
			conn.close();
			return true;

		} catch (Exception e) {
		}

		return false;
	}

	public static String getReview(int my_feedback_id, int user_id,
			int conference_id, int user_role) {
		String the_review = null;
		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT REVIEW FROM FEEDBACK_USER WHERE FEEDBACK_ID = "
							+ my_feedback_id
							+ " AND USER_ID = "
							+ user_id
							+ " AND CONFERENCE_ID = "
							+ conference_id
							+ " AND USER_ROLE = " + user_role);
			ResultSet rd = st.executeQuery();
			if (rd.next()) {
				the_review = rd.getString(1);
			}
			st.close();
			rd.close();
			conn.close();

		} catch (Exception e) {
		}

		return the_review;
	}

	/**
	 * Reads objects from this locations into an array.
	 * 
	 * @param conference_id
	 * @param user_role
	 */
	public static int[] getScores(int my_feedback_id, int user_id,
			int conference_id, int user_role) {
		int[] readAllValues = null;

		try {
			Connection conn = getConnection();

			PreparedStatement pstmt = conn
					.prepareStatement("SELECT SCORES FROM FEEDBACK_USER WHERE FEEDBACK_ID ="
							+ my_feedback_id
							+ " AND USER_ID = "
							+ user_id
							+ " AND CONFERENCE_ID = "
							+ conference_id
							+ " AND USER_ROLE = " + user_role);

			ResultSet rd = pstmt.executeQuery();
			if (rd.next()) {

				ObjectInputStream oos = new ObjectInputStream(
						new FileInputStream(Integer.toString(my_feedback_id
								* 37 + user_id * 19)));
				readAllValues = (int[]) oos.readObject();
				oos.close();
			}
			rd.close();
			pstmt.close();
		} catch (Exception e) {

		}
		return readAllValues;
	}

	public static List<UserRole> getUsersFromUser(int user_id, int feedback_id,
			int conference_id) {
		List<UserRole> user_role = new ArrayList<>();

		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM FEEDBACK_USER WHERE ASSIGNED_BY = ? AND FEEDBACK_ID = ? AND CONFERENCE_ID = ?");
			st.setInt(1, user_id);
			st.setInt(2, feedback_id);
			st.setInt(3, conference_id);

			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				user_role.add(new UserRole(rd.getInt("USER_ID"), rd
						.getInt("USER_ROLE"), rd.getInt("ASSIGNED_BY")));

			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return user_role;
	}

	public static boolean setStatus(int my_feedback_id, int user_id,
			Status new_status, int conference_id, int user_role) {
		boolean isAlterable = false;
		try {

//			int status = getStatus(my_feedback_id, user_id, conference_id,
//					user_role).getValue();
			Connection conn = getConnection();

//			if (status == 0) {
				PreparedStatement st = conn
						.prepareStatement("UPDATE FEEDBACK_USER SET STATUS = '"
								+ new_status.getValue()
								+ "' WHERE FEEDBACK_ID = " + my_feedback_id
								+ " AND USER_ID = " + user_id
								+ " AND CONFERENCE_ID = " + conference_id
								+ " AND USER_ROLE = " + user_role);
				st.executeUpdate();
				st.close();
				isAlterable = true;
//			}

			conn.close();

		} catch (Exception e) {
		}

		return isAlterable;
	}

	public static List<Feedback> getUserFeedback(int user_id) {
		List<Feedback> user_feedbacks = new ArrayList<>();
		if (my_user_feedback.containsKey(user_id))
			for (int i : my_user_feedback.get(user_id)) {
				user_feedbacks.add(FeedbackDB.getFeedback(i));
			}

		return user_feedbacks;
	}

	/**
	 * DELETE THIS IF YOU ARE NOT DEBUGGING
	 */
	public static void clearTable() {
		try {
			Connection conn = getConnection();
			conn.createStatement().executeUpdate(
					"drop table if exists FEEDBACK_USER");
			conn.createStatement().executeUpdate(create);
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Feedback> getFeedbackFromUser(int conference_id,
			int user_id, int role, int assignee_role) {
		List<Feedback> feedback = new ArrayList<>();
		UserRole r = new UserRole(user_id, role, assignee_role);
		if (my_user_feedback.containsKey(r))
			for (int i : my_user_feedback.get(r)) {
				Feedback fb = FeedbackDB.getFeedback(i);
				if (fb != null) {
					if (fb.getConference().getID() == conference_id)
						feedback.add(fb);
				}
			}

		return feedback;
	}

	public static List<UserRole> getUsersFromFeedback(int conference_id,
			int feedback_id) {
		List<UserRole> feedback = new ArrayList<>();
		if (my_feedback_user.containsKey(feedback_id))
			for (UserRole i : my_feedback_user.get(feedback_id)) {
				Feedback fb = FeedbackDB.getFeedback(feedback_id);
				if (fb != null) {
					if (fb.getConference().getID() == conference_id)
						feedback.add(i);
				}
			}

		return feedback;
	}

	public static int getFeedbackSize(int conference_id, int user_id, int role,
			int assignee_id) {
		int z = 0;
		UserRole r = new UserRole(user_id, role, assignee_id);
		if (my_user_feedback.containsKey(r))
			for (int i : my_user_feedback.get(r)) {
				Feedback fb = FeedbackDB.getFeedback(i);

				if (fb != null)
					if (fb.getConference().getID() == conference_id)
						z++;
			}

		return z;

	}

	public static List<UserRoleFeedback> getFeedbackFromUser(int conference_id,
			int assignee_by, int assigned_role) {
		List<UserRoleFeedback> feedback = new ArrayList<>();

		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM FEEDBACK_USER WHERE ASSIGNED_BY = ? AND ASSIGNED_ROLE = ? AND CONFERENCE_ID = ?");
			st.setInt(1, assignee_by);
			st.setInt(2, assigned_role);

			st.setInt(3, conference_id);

			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				feedback.add(new UserRoleFeedback(rd.getInt("USER_ID"), rd
						.getInt("USER_ROLE"), rd.getInt("FEEDBACK_ID")));

			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return feedback;
	}
	
	

	public static List<UserRoleFeedback> getFeedbackFromUserWithoutAssigned(int conference_id,
			int user_id, int user_role) {
		List<UserRoleFeedback> feedback = new ArrayList<>();

		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM FEEDBACK_USER WHERE USER_ID = ? AND USER_ROLE = ? AND CONFERENCE_ID = ?");
			st.setInt(1, user_id);
			st.setInt(2, user_role);

			st.setInt(3, conference_id);

			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				feedback.add(new UserRoleFeedback(rd.getInt("USER_ID"), rd
						.getInt("USER_ROLE"), rd.getInt("FEEDBACK_ID")));

			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return feedback;
	}
	
	
	public static List<Feedback> getFeedbackByIDAndRole(int conference_id, int user_id, int current_role)
	{
		List<Integer> feedback = new ArrayList<>();
		List<Feedback> list = new ArrayList<>();

		try {
			Connection conn = getConnection();
			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM FEEDBACK_USER WHERE USER_ID = ? AND USER_ROLE = ? AND CONFERENCE_ID = ?");
			st.setInt(1, user_id);
			st.setInt(2, current_role);
			st.setInt(3, conference_id);

			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				feedback.add(rd.getInt("FEEDBACK_ID"));

			}
			rd.close();
			st.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		for (int i : feedback)
			list.add(FeedbackDB.getFeedback(i));

		return list;
	}

}
