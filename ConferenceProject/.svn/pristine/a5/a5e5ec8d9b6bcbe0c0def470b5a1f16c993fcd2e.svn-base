package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Model.Date;
import Model.TempConference;
import Model.TempUser;

public class ConferenceDB extends AbstractDB {

	static Map<Integer, TempConference> my_conference = getConferences();

	static final String SELECT = "SELECT * FROM CONFERENCE WHERE CONFERENCE_ID = ?";
	static final String CREATE_USER = "INSERT INTO CONFERENCE(CONFERENCE_NAME, PROGRAMCHAIR_ID, CONFERENCE_DATE, AUTHOR_DEADLINE, REVIEWER_DEADLINE, SUBPROGRAM_DEADLINE) VALUES (?, ?, ?, ?, ?, ?)";
	private static String create = "create table if not exists CONFERENCE (CONFERENCE_ID INTEGER, CONFERENCE_NAME STRING, PROGRAMCHAIR_ID INTEGER, CONFERENCE_DATE INTEGER, AUTHOR_DEADLINE INTERGER, REVIEWER_DEADLINE INTERGER, SUBPROGRAM_DEADLINE INTERGER, PRIMARY KEY (CONFERENCE_ID ASC))";

	static {
		try {
			Connection conn = getConnection();
			conn.createStatement().executeUpdate(create);

			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean createConference(String conference_name,
			int programchair_id, Date conference_date, Date author_deadline,
			Date review_deadline, Date subprogram_deadline) {
		try {
			Connection conn = getConnection();
			PreparedStatement pstmt = conn.prepareStatement(CREATE_USER);
			pstmt.setString(1, conference_name);
			pstmt.setInt(2, programchair_id);
			pstmt.setInt(3, conference_date.getDate());
			pstmt.setInt(4, author_deadline.getDate());
			pstmt.setInt(5, review_deadline.getDate());
			pstmt.setInt(6, subprogram_deadline.getDate());
			pstmt.executeUpdate();
			pstmt.close();

			PreparedStatement st = conn
					.prepareStatement("SELECT * FROM CONFERENCE WHERE CONFERENCE_ID = (SELECT MAX(CONFERENCE_ID) FROM CONFERENCE)");
			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				if(!my_conference.containsKey(rd.getInt(1)))
					my_conference.put(rd.getInt(1), new TempConference(rd.getInt(1)));
			}

			st.close();
			rd.close();
			conn.close();

			return true;
		} catch (Exception e) {
		}
		return false;
	}

	private static Object query(int conference_id, String column) {
		Object obj = null;
		try {

			Connection conn = getConnection();
			PreparedStatement st = conn.prepareStatement("SELECT " + column
					+ " FROM CONFERENCE WHERE CONFERENCE_ID = ?");
			st.setInt(1, conference_id);
			ResultSet rd = st.executeQuery();
			if (rd.next()) {
				obj = rd.getObject(column);
			}

			st.close();
			rd.close();
			conn.close();
			return obj;
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public static TempUser getProgramChair(int conference_id) {
		return UserDB.getUser((int) query(conference_id, "PROGRAMCHAIR_ID"));
	}
	
	public static String getConferenceName(int conference_id) {
		return (String) query(conference_id, "CONFERENCE_NAME");
	}

	public static Date getConferenceDate(int conference_id) {
		int date = (int) query(conference_id, "CONFERENCE_DATE");
		return new Date((date % 10000) / 100, date % 100, date / 10000);
	}

	public static Date getAuthorDeadline(int conference_id) {
		int date = (int) query(conference_id, "AUTHOR_DEADLINE");
		return new Date((date % 10000) / 100, date % 100, date / 10000);
	}

	public static Date getReviewerDeadline(int conference_id) {
		int date = (int) query(conference_id, "REVIEWER_DEADLINE");
		return new Date((date % 10000) / 100, date % 100, date / 10000);
	}

	public static Date getSubProgramDeadline(int conference_id) {
		int date = (int) query(conference_id, "SUBPROGRAM_DEADLINE");
		return new Date((date % 10000) / 100, date % 100, date / 10000);
	}

	public static TempConference getConference(int conference_id) {
		return my_conference.get(conference_id);
	}

	/**
	 * DELETE THIS IF YOU ARE NOT DEBUGGING
	 */
	public static void clearTable() {
		try {
			getConnection().createStatement().executeUpdate(
					"drop table if exists CONFERENCE");
			my_conference.clear();
			Connection conn = getConnection();
			conn.createStatement().executeUpdate(create);
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static Map<Integer, TempConference> getConferences() {
		Map<Integer, TempConference> conferences = new HashMap<>();
		try {
			Connection conn = getConnection();
			PreparedStatement st;
			st = conn.prepareStatement("SELECT CONFERENCE_ID FROM CONFERENCE");
			ResultSet rd = st.executeQuery();
			while (rd.next()) {
				if(!conferences.containsKey(rd.getInt(1)))
				conferences.put(rd.getInt(1), new TempConference(rd.getInt(1)));
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		return conferences;

	}

	public static List<TempConference> getConferenceList() {
		return new ArrayList<>(my_conference.values());
	}

}