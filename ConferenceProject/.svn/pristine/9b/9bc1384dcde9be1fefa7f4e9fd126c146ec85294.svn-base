package Tests.TestJoe;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import Model.Admin;
import Model.Conference;
import Model.ConferenceRole;
import Model.Feedback;
import Model.ProgramChair;
import Model.Reviewer;
import Model.SubProgramChair;
import Model.Feedback.FeedbackStatus;

public class ProgramChairTest {
	private Admin my_admin;
	private ProgramChair my_programchair;
	private SubProgramChair my_spc;
	private Reviewer my_reviewer;
	private Conference my_conference;

	@Before
	public void setUp() throws Exception {

		my_admin = new Admin("Guy52", "Guy Smith", "Password52");

		my_programchair = new ProgramChair("Johnny52", "John Smith",
				"Password52");
		my_admin.getListOfConferences().clear();
		my_admin.createConference("New conference", new Date(), my_programchair);
		my_conference = my_admin.getListOfConferences().get(0);
		my_reviewer = new Reviewer("ReviewerMan", "Reviewy", "Cool123");
		my_spc = new SubProgramChair("SubProgramGuy", "Subby", "Run112");

		my_conference.createFeedback("Author", "Text", "Other");
	}

	@Test
	public void testAssignPaper() {
		assertTrue(my_programchair.promoteUser(my_conference, my_spc));
		assertTrue(my_conference.getFeedbackList().size() == 1);
		assertTrue(my_programchair.assignPaper(my_conference.getFeedbackList()
				.get(0), my_spc));
		assertTrue(my_spc.getFeedback().size() == 1);
		assertTrue(my_programchair.assignPaper(my_conference.getFeedbackList()
				.get(0), my_spc));
		assertTrue(my_spc.getFeedback().size() == 2);
		assertFalse(my_programchair.assignPaper(null, my_spc));
		assertTrue(my_spc.getFeedback().size() == 2);
		assertFalse(my_programchair.assignPaper(my_conference.getFeedbackList()
				.get(0), null));
		assertFalse(my_programchair.assignPaper(my_conference.getFeedbackList()
				.get(0), my_reviewer));
		assertTrue(my_reviewer.getFeedback().size() == 0);
	}

	@Test
	public void testSubmitApproval() {
		Feedback fb = my_spc.getListOfConferences().get(0).getFeedbackList()
				.get(0);
		assertTrue(fb.getStatus(ConferenceRole.PROGRAM_CHAIR) == FeedbackStatus.PENDING);
		assertTrue(my_programchair.submitApproval(my_conference
				.getFeedbackList().get(0)));
		assertTrue(fb.getStatus(ConferenceRole.SUBPROGRAM_CHAIR) == FeedbackStatus.PENDING);
		assertTrue(fb.getStatus(ConferenceRole.PROGRAM_CHAIR) == FeedbackStatus.APPROVED);
		assertTrue(fb.getStatus(ConferenceRole.REVIEWER) == FeedbackStatus.PENDING);
		assertFalse(my_programchair.submitApproval(null));
	}

	@Test
	public void testGetFeedbackList() {
		assertTrue(my_programchair.getFeedbackList(my_conference).size() == 1);
		assertTrue(my_programchair.beAssigned(my_conference.getFeedbackList()
				.get(0)));
		assertTrue(my_programchair.getFeedbackList(my_conference).size() == 1);
		assertTrue(my_conference.createFeedback("Author1", "Text2", "Other3") == my_conference
				.getFeedbackList().size() - 1);
		assertTrue(my_programchair.beAssigned(my_conference.getFeedbackList()
				.get(1)));
		assertTrue(my_programchair.getFeedbackList(my_conference).size() == 2);
		assertNull(my_programchair.getFeedbackList(null));
	}

	@Test
	public void testGetUsers() {
		assertTrue(my_programchair.getUsers().size() == 0);
		assertTrue(my_programchair.promoteUser(my_conference, my_spc));
		assertTrue(my_programchair.getUsers().size() == 1);
		assertTrue(my_spc.promoteUser(my_conference, my_reviewer));
		assertTrue(my_programchair.getUsers().size() == 2);
	}

	@Test
	public void testPromoteUser() {
		assertTrue(my_programchair.promoteUser(my_conference, my_spc));
		assertFalse(my_programchair.promoteUser(my_conference, my_spc));
		assertTrue(my_spc.promoteUser(my_conference, my_reviewer));
		assertTrue(my_programchair.promoteUser(my_conference, my_reviewer));
		assertFalse(my_programchair.promoteUser(my_conference, my_reviewer));
		assertFalse(my_programchair.promoteUser(null, my_reviewer));
		assertFalse(my_programchair.promoteUser(null, null));
		assertFalse(my_programchair.promoteUser(my_conference, null));


	}

	@Test
	public void testGetStatus() {
		assertTrue(my_programchair.promoteUser(my_conference, my_spc));
		assertTrue(my_programchair.assignPaper(my_conference.getFeedbackList()
				.get(0), my_spc));

		assertTrue(my_programchair.getStatus(my_spc).size() == 1);
		assertTrue(my_programchair.getStatus(my_reviewer).size() == 0);
		assertNull(my_programchair.getStatus(null));
	}

}
