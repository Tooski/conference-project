package Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import Model.Feedback.Status;

import db.ConferenceDB;
import db.ConferenceUserDB;
import db.FeedbackDB;
import db.FeedbackUserDB;
import db.UserDB;

/**
 * @Author Josef Nosov
 */
public class User {

	protected int my_id;

	public User(int user_id) {
		my_id = user_id;
	}

	/**
	 * This method allows the user to submit a paper to a conference it also
	 * sets the user to Author status
	 * 
	 * @param the_title
	 *            the title of the paper
	 * @param the_text
	 *            the contents of the paper itself
	 * @return boolean on whether or not creation of the paper was successful
	 */
	public int submitPaper(Conference conf, String the_title,
			String the_paper) {
		if (the_title == null || the_paper == null) {
			return -1;
		}
		ConferenceUserDB.addUser(conf.getID(), 0, my_id, ConferenceRole.AUTHOR);
		return conf.createFeedback(getActualUserName(), the_title, the_paper);
	}

	/**
	 * This method is designed to delete a paper submitted by a user who is an
	 * author
	 * 
	 * @return boolean on whether or not paper was successfully deleted
	 */
	public boolean deletePaper(Feedback the_feedback) {
		if (ConferenceUserDB.getRole(the_feedback.getConference().getID(),
				my_id) != ConferenceRole.AUTHOR) {
			return false;
		}

		return FeedbackDB.deletePaper(the_feedback.getID());

	}

	/**
	 * This method allows a user who is an author to edit a paper they have
	 * written
	 * 
	 * @param the_feedback
	 *            the feedback object that contains their paper
	 * @param new_paper
	 *            the edited paper they wish to submit
	 * @return boolean on whether or not the paper was successfully edited
	 */
	public boolean editPaper(Feedback the_feedback, String new_paper) {

		return the_feedback.editPaper(new_paper);
	}

	/**
	 * this method returns the feedback associated with this user
	 * 
	 * @return the list of this user's associated feedback
	 */
	public List<Feedback> getFeedback() {

		return FeedbackUserDB.getUserFeedback(my_id);
	}

	/**
	 * Assigns a user
	 * 
	 * @param the_conference
	 * 
	 * @param the_feedback
	 * @return
	 */
	public boolean beAssigned(Feedback the_feedback) {
		return FeedbackUserDB.assignFeedback(the_feedback.getID(), my_id);
	}

	public List<Conference> getListOfConferences() {
		return ConferenceDB.getConferenceList();
	}

	public ConferenceRole getRoles(Conference the_conference) {
		return ConferenceUserDB.getRole(the_conference.getID(), my_id);

	}

	public int getID() {
		return my_id;
	}

	public String getUserName() {
		return UserDB.getUserName(my_id);
	}

	public String getActualUserName() {
		return UserDB.getActualName(my_id);
	}

	/**
	 * gets the list of feedback associated with a user
	 * 
	 * @param the_user
	 *            the user to get the feedback from
	 * @return the list of feedback associated with the user
	 */
	public List<Feedback> getFeedbacksByConference(
			Conference conference, User the_user) {

		if (the_user == null || conference == null)
			return null;

		return FeedbackUserDB.getFeedbackFromUser(conference.getID(),
				the_user.getID());
	}

	/**
	 * gets the list of feedback associated with a user
	 * 
	 * @param the_user
	 *            the user to get the feedback from
	 * @return the list of feedback associated with the user
	 */
	public List<Feedback> getFeedbackByUser(User the_user) {

		if (the_user == null)
			return null;

		return FeedbackUserDB.getUserFeedback(the_user.my_id);
	}

	/**
	 * gets the list of feedback associated with a user
	 * @param my_conference 
	 * 
	 * @param the_user
	 *            the user to get the feedback from
	 * @return the list of feedback associated with the user
	 */
	public List<Feedback> getFeedbackList(Conference my_conference) {


		return FeedbackUserDB.getFeedbackFromUser(my_conference.getID(),
				getID());
	}

	
	public List<Feedback> getAllFeedbackList(Conference the_conference) {
		if (the_conference == null
				|| ConferenceUserDB.getRole(the_conference.getID(), my_id) != ConferenceRole.PROGRAM_CHAIR)
			return null;

		return FeedbackDB.getFeedbackList(the_conference.getID());
	}

	/**
	 * This method allows a Reviewer to submit a review
	 * 
	 * @param feedback
	 *            the contents of the review
	 * @param rating
	 * @param feedback_index
	 *            the index of the feedback they are assigned to review
	 * @return boolean on whether or not the review was successful
	 */
	public boolean submitReview(Feedback feedback, String my_review,
			int[] rating) {
		List<Feedback> my_feedback = FeedbackUserDB.getUserFeedback(my_id);

		if (feedback != null
				&& ConferenceUserDB.getRole(feedback.getConference().getID(),
						my_id) == ConferenceRole.REVIEWER) {
			boolean contains_feedback = false;
			for (int i = 0; i < my_feedback.size(); i++) {
				if (my_feedback.get(i).equals(feedback)) {
					contains_feedback = true;
				}
			}

			if (feedback == null
					|| my_review == null
					|| !contains_feedback
					|| !FeedbackUserDB.setReview(feedback.getID(), my_id,
							my_review, rating)) {
				return false;
			}
			return FeedbackUserDB.setStatus(feedback.getID(), my_id,
					Status.APPROVED);
		}
		return false;
	}

	/**
	 * This method allows a Reviewer to edit an existing review
	 * 
	 * @param the_feedback
	 *            the feedback that contains this review
	 * @param the_new_review
	 *            the updated review
	 * @param review_index
	 *            the index of the old review
	 * @return boolean whether or not the review was updated
	 */
	public boolean editReview(Feedback the_feedback, String the_new_review) {
		if (ConferenceUserDB.getRole(the_feedback.getConference().getID(),
				my_id) == ConferenceRole.REVIEWER) {

			boolean contains_feedback = false;
			List<Feedback> my_feedback = FeedbackUserDB
					.getUserFeedback(my_id);
			for (int i = 0; i < my_feedback.size(); i++) {
				if (my_feedback.get(i).equals(the_feedback)) {
					contains_feedback = true;
				}
			}
			if (the_feedback == null || the_new_review == null
					|| !contains_feedback) {
				return false;
			}
			return FeedbackUserDB.editReview(the_feedback.getID(), my_id,
					the_new_review);
		}
		return false;

	}

	// /**
	// * allows a Reviewer to delete a review
	// *
	// * @param the_feedback
	// * the feedback this review is associated with
	// * @param review_index
	// * the index of the review to be deleted
	// * @return whether or not the review was deleted
	// */
	// public boolean deleteReview(Feedback the_feedback) {
	// if (the_feedback != null &&
	// my_role.get(the_feedback.getConference()).getRole() ==
	// ConferenceRole.REVIEWER) {
	// boolean contains_feedback = false;
	// for (int i = 0; i < my_feedback.size(); i++) {
	// if (my_feedback.get(i).equals(the_feedback)) {
	// contains_feedback = true;
	// }
	// }
	// if (the_feedback == null || !contains_feedback) {
	// return false;
	// }
	// return the_feedback.deleteReview(my_actualname);
	// }
	// return false;
	// }

	public boolean addUser(Conference my_conference, User the_user,
			ConferenceRole role) {
		if (my_conference == null || the_user == null) {
			return false;
		}
		ConferenceRole user_role = ConferenceUserDB.getRole(
				my_conference.getID(), my_id);
		if ((user_role == ConferenceRole.PROGRAM_CHAIR && role.getRole() > 2)
				|| (user_role == ConferenceRole.SUBPROGRAM_CHAIR && role
						.getRole() > 3))
			return ConferenceUserDB.addUser(my_conference.getID(), my_id,
					the_user.getID(), role);

		return false;
	}

	public boolean promoteUser(Conference my_conference, User the_user,
			ConferenceRole role) {
		ConferenceRole user_role = ConferenceUserDB.getRole(
				my_conference.getID(), my_id);

		if (user_role == ConferenceRole.PROGRAM_CHAIR
				|| user_role == ConferenceRole.SUBPROGRAM_CHAIR) {
			System.out.println("Test");

			if (my_conference == null || the_user == null) {
				return false;
			}
			if ((user_role == ConferenceRole.PROGRAM_CHAIR && role.getRole() >= 2)
					|| (user_role == ConferenceRole.SUBPROGRAM_CHAIR && role
							.getRole() >= 3))
			{
				return ConferenceUserDB.promoteUser(my_conference.getID(),
						my_id, the_user.getID(), role);
			}
		}
		return false;
	}

	public boolean assignPaper(Feedback the_feedback, User the_user) {
		if (the_feedback == null || the_user == null) {
			return false;
		}
		return FeedbackUserDB.assignFeedback(the_feedback.getID(),
				the_user.getID());

	}

	public boolean setStatus(Feedback the_feedback, Status status) {
		if (the_feedback == null || status == null) {
			return false;
		}
		return FeedbackUserDB.setStatus(the_feedback.getID(), my_id, status);
	}

	public List<User> getUsers(Conference my_conference) {
		if (ConferenceUserDB.getRole(my_conference.getID(), my_id) == ConferenceRole.PROGRAM_CHAIR) {

			return ConferenceUserDB.getUserList(my_conference.getID());
		} else
			return null;
	}

}
