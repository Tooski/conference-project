
package view.paper;

import java.util.Calendar;
import java.util.Observable;

import javax.swing.JPanel;

import model.Date;

import view.GUI;

/**
 * The abstract subpanel.
 * 
 * @author Josef Nosov
 * @version 06/06/2013
 * 
 */
public abstract class AbstractSubpanelGUI extends Observable implements GUI {
  /**
   * The panel.
   */
  protected JPanel my_panel;

  /**
   * Check date method checks to see if the date has passed or not.
   * 
   * @param the_date is the date you would like to check.
   * @return returns true or false if the date has passed.
   */
  protected boolean checkDate(final Date the_date) {
    boolean date_not_passed = false;
    if (Calendar.getInstance().get(Calendar.YEAR) > the_date.getYear()) {
      date_not_passed = true;
    } else if (Calendar.getInstance().get(Calendar.YEAR) == the_date.getYear()) {
      if (Calendar.getInstance().get(Calendar.MONTH) + 1 > the_date.getMonth()) {
        date_not_passed = true;
      } else if (Calendar.getInstance().get(Calendar.MONTH) + 1 == the_date.getMonth()) {
        date_not_passed =
            Calendar.getInstance().get(Calendar.DAY_OF_MONTH) <= the_date.getDay();
      }
    }
    return date_not_passed;
  }

  @Override
  public JPanel display() {
    return my_panel;
  }

}
