
package Tests.TestJoe;

import static org.junit.Assert.*;

import model.Date;

import org.junit.Before;
import org.junit.Test;

public class DateCalendarTest {

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void test() {
    Date calendar = new Date(05, 24, 2013);
    assertTrue(calendar.getDate() == 20130524);
    Date calendar1 = new Date(02, 28, 2013);

    assertTrue(calendar1.getDate() == 20130228);
    assertTrue(calendar1.getDate() != 20130219);

  }

}
