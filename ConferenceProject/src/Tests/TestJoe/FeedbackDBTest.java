
package Tests.TestJoe;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import db.FeedbackDB;

public class FeedbackDBTest {

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void testCreateFeedback() {
    assertTrue(FeedbackDB.createFeedback(1, "Harry Potter", "JK Rowling",
                                         "You're a wizard Harry", 0, null) == 1);
    assertTrue(FeedbackDB.createFeedback(1, "Harry Potter1", "JK Rowlingdf",
                                         "You're a wizard Harry", 2, null) == 2);
    assertTrue(FeedbackDB.createFeedback(1, "Harry Potter2", "JK Rowlingdf",
                                         "You're a wizard Harry", 1, null) == 3);
    assertTrue(FeedbackDB.createFeedback(1, "Harry Potter3", "JK Rowlingdf",
                                         "You're a wizard Harry", 2, null) == 4);
    assertTrue(FeedbackDB.createFeedback(1, "Harry Potter4", "JK Rowlingdf",
                                         "You're a wizard Harry", 3, null) == 5);
    assertTrue(FeedbackDB.createFeedback(1, "Harry Potter5", "JK Rowling",
                                         "You're a wizard Harry", 0, null) == 6);
    assertTrue(FeedbackDB.createFeedback(1, "Harry Potter3", "JK Rowling",
                                         "You're a wizard Harry", 0, null) == 7);
    assertTrue(FeedbackDB.createFeedback(1, "Harry Potter6", "JK Rowlingdf",
                                         "You're a wizard Harry", 4, null) == 8);

    assertTrue(FeedbackDB.createFeedback(2, "Harry Potter3", "JK Rowling",
                                         "You're a wizard Harry", 0, null) == 9);
    assertTrue(FeedbackDB.createFeedback(3, "Harry Potter3", "JK Rowling",
                                         "You're a wizard Harry", 0, null) == 10);

    assertTrue(FeedbackDB.getPaper(1).equals("You're a wizard Harry"));
    assertTrue(FeedbackDB.getTitle(1).equals("Harry Potter"));
    assertTrue(FeedbackDB.getAuthor(1).equals("JK Rowling"));
    // assertTrue(Integer.valueOf(FeedbackDB.getConference(2)).equals(Integer.valueOf(1)));

    FeedbackDB.editPaper(1, 1, "Snape kills Dumbledore");

    FeedbackDB.deletePaper(5);
    FeedbackDB.deletePaper(4);

    System.out.println(FeedbackDB.getFeedback(3).getID());

  }

}
