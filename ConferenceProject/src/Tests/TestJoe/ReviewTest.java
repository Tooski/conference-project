/**
 * OBSOLETE, REVIEWER CLASS HAS BEEN CHANGED TO SATISFY DATABASE
 * @author Josef Nosov
 * @version 05/31/13
 */

package Tests.TestJoe;

import static org.junit.Assert.*;

import java.util.GregorianCalendar;

import model.Admin;
import model.Conference;
import model.User;

import org.junit.Before;
import org.junit.Test;


public class ReviewTest {

	private Admin my_admin;
	private User my_programchair;
	private User my_spc;
	private User my_reviewer;
	private Conference my_conference;
	private int[] values = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

//	@Before
//	public void setUp() throws Exception {
//		my_admin = new Admin("Guy52", "Guy Smith", 1);
//		my_programchair = new User("Johnny52", "John Smith", 2);
//		my_admin.getListOfConferences().clear();
//		my_admin.createConference("New conference", new GregorianCalendar(), my_programchair);
//		my_conference = my_admin.getListOfConferences().get(0);
//		my_reviewer = new User("ReviewerMan", "Reviewy", 3);
//		my_spc = new User("SubProgramGuy", "Subby", 4);
//		my_programchair.promoteUser(my_conference, my_spc);
//		my_conference.createFeedback("Author", "Text", "Other");
//		my_spc.promoteUser(my_conference, my_reviewer);
//	}
//
//	@Test
//	public void testSubmitReview() {
//		assertTrue(my_reviewer.getFeedback().size() == 0);
//		assertTrue(my_spc.assignPaper(my_conference.getFeedbackList().get(0),
//				my_reviewer));
//		assertTrue(my_reviewer.getFeedback().size() == 1);
//		assertTrue(my_reviewer.submitReview(my_reviewer.getFeedback().get(0),
//				"It was all good!", values));
//		assertFalse(my_reviewer.submitReview(my_reviewer.getFeedback().get(0),
//				"It was all good!", values));
//		my_conference.createFeedback("Author2", "Text2", "Other2");
//		assertTrue(my_spc.assignPaper(my_conference.getFeedbackList().get(1),
//				my_reviewer));
//		assertTrue(my_reviewer.submitReview(my_reviewer.getFeedback().get(1),
//				"Not that great!", values));
//		assertTrue(my_reviewer.getFeedback().size() == 2);
//		assertFalse(my_reviewer.submitReview(null, "Not that great!", values));
//		assertTrue(my_reviewer.getFeedback().size() == 2);
//		assertFalse(my_reviewer.submitReview(null, null, values));
//		assertTrue(my_reviewer.getFeedback().size() == 2);
//		assertFalse(my_reviewer.submitReview(my_reviewer.getFeedback().get(0),
//				null, values));
//		assertTrue(my_reviewer.getFeedback().size() == 2);
//	}
//
//	@Test
//	public void testEditReview() {
//		assertTrue(my_reviewer.getFeedback().size() == 0);
//		assertTrue(my_spc.assignPaper(my_conference.getFeedbackList().get(0),
//				my_reviewer));
//		assertTrue(my_reviewer.getFeedback().size() == 1);
//		assertFalse(my_reviewer.editReview(my_reviewer.getFeedback().get(0),
//				"It was all good!"));
//		assertTrue(my_reviewer.submitReview(my_reviewer.getFeedback().get(0),
//				"It was all good!", values));
//		assertTrue(my_reviewer.getFeedback().get(0).getReview(0).getReview()
//				.equals("It was all good!"));
//		assertTrue(my_reviewer.editReview(my_reviewer.getFeedback().get(0),
//				"It was pretty good!"));
//		assertFalse(my_reviewer.getFeedback().get(0).getReview(0).getReview()
//				.equals("It was all good!"));
//		assertTrue(my_reviewer.getFeedback().get(0).getReview(0).getReview()
//				.equals("It was pretty good!"));
//
//		assertFalse(my_reviewer.submitReview(null, "Not that great!", values));
//		assertFalse(my_reviewer.submitReview(null, null, values));
//
//		assertFalse(my_reviewer.submitReview(my_reviewer.getFeedback().get(0),
//				null, values));
//
//	}
//	
//	@Test
//	public void testDeleteReview() {
//		assertTrue(my_reviewer.getFeedback().size() == 0);
//		assertTrue(my_spc.assignPaper(my_conference.getFeedbackList().get(0),
//				my_reviewer));
//		assertTrue(my_reviewer.getFeedback().size() == 1);
//		assertTrue(my_reviewer.submitReview(my_reviewer.getFeedback().get(0),
//				"It was all good!", values));
//		assertTrue(my_reviewer.deleteReview(my_reviewer.getFeedback().get(0)));
//		assertFalse(my_reviewer.deleteReview(my_reviewer.getFeedback().get(0)));
//		assertFalse(my_reviewer.deleteReview(null));
//
//	}

}
