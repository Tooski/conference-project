package Tests.TestJoe;

import static org.junit.Assert.*;

import java.util.Arrays;

import model.Date;
import model.Feedback;
import model.Feedback.Status;

import org.junit.Before;
import org.junit.Test;

import db.ConferenceDB;
import db.FeedbackDB;
import db.FeedbackUserDB;
import db.UserRoleFeedback;



public class FeedbackUserDBTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testAssignFeedback() {


		ConferenceDB.createConference("New conference", 1, new Date(05,5,2012), new Date(05,12,2012),new Date(05,19,2012),new Date(05,26,2012));
		assertTrue(FeedbackDB.createFeedback(1, "Harry Potter", "JK Rowling", "You're a blizzard Harry", 0, null)  == 1);
		assertTrue(FeedbackDB.createFeedback(1, "Harry Potter2", "JK Rowling", "You're a wizard Harry", 0, null) == 2);

    assertTrue(FeedbackUserDB.assignFeedback(1, 2, 2 , 0, 0, 0));
    assertTrue(FeedbackUserDB.assignFeedback(2, 2, 2, 0, 0, 0));

	
	}

	@Test
	public void testGetStatus() {
		assertTrue(FeedbackUserDB.getStatus(1, 2, 0, 0).getValue() == 0);
		FeedbackUserDB.setStatus(1, 2, Status.APPROVED_BY_PROGRAM_CHAIR, 0, 0);
		assertTrue(FeedbackUserDB.getStatus(1, 2, 0, 0).getValue()  == 1);

	}

	@Test
	public void testSetStatus() {
	    assertTrue(FeedbackUserDB.assignFeedback(2, 2, 3, 0, 0, 0));

		assertTrue(FeedbackUserDB.getStatus(2, 2, 0, 0).getValue()  == 0);
		assertTrue(FeedbackUserDB.setStatus(2, 2, Status.REJECTED_BY_PROGRAM_CHAIR, 0, 0));
		assertTrue(FeedbackUserDB.getStatus(2, 2, 0, 0).getValue()  == -1);
		
		assertFalse(FeedbackUserDB.setStatus(1, 2, Status.REJECTED_BY_PROGRAM_CHAIR, 0, 0));
		assertFalse(FeedbackUserDB.getStatus(1, 2, 0, 0).getValue()  == -1);
	}

	

	@Test
	public void testSetReview() {
	    assertTrue(FeedbackUserDB.setReview(1, 2, "Alright", new int []{1,2,3,4,5,6,7,8,9}, 0, 0));
	    System.out.println(Arrays.toString(FeedbackUserDB.getScores(1, 2, 0, 0)));
	    System.out.println(FeedbackUserDB.getReview(1, 2, 0, 0));
	    
//	    for(TempFeedback fb : FeedbackUserDB.getUserFeedback(2))
//	    System.out.println(fb.getID());
	    
	    FeedbackUserDB.editReview(1, 2, "NOPE", 0, 0);
	    System.out.println(FeedbackUserDB.getReview(1, 2, 0, 0));
	    
//	    System.out.println(FeedbackUserDB.getUserFeedback(user_id))
	    
	    for(UserRoleFeedback fb : FeedbackUserDB.getFeedbackFromUser(1, 2, 0))
	    	    	System.out.println(fb.getFeedback());
	    
	    assertTrue(FeedbackUserDB.getFeedbackFromUser(2, 2, 0).size() == 0);
	    assertTrue(FeedbackUserDB.getFeedbackFromUser(1, 1, 0).size() == 0);

	}
	
}
