/**
 * OBSOLETE, C CLASS HAS BEEN CHANGED TO SATISFY DATABASE
 * 
 * @author Josef Nosov
 * @version 05/31/13
 */

package Tests.TestJoe;

import static org.junit.Assert.*;

import model.ConferenceRole;
import model.Date;

import org.junit.Before;
import org.junit.Test;

import db.ConferenceDB;
import db.ConferenceUserDB;
import db.UserDB;

public class ConferenceDBTest {

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void testCreateConference() {

    assertFalse(ConferenceUserDB.promoteUser(1, UserDB.getUser(1), 1, ConferenceRole.ADMIN));
    assertTrue(ConferenceDB.createConference("New conference", 1, new Date(05, 5, 2012),
                                             new Date(05, 12, 2012), new Date(05, 19, 2012),
                                             new Date(05, 26, 2012)) == 1);
    assertTrue(ConferenceDB.createConference("New conference", 2, new Date(01, 4, 2012),
                                             new Date(02, 5, 2012), new Date(3, 6, 2012),
                                             new Date(4, 7, 2012)) == 2);
    // assertTrue(ConferenceDB.createConference("New conference", 8, 9, 42, 15,
    // 72));

  }

  @Test
  public void testGetDate() {
    assertTrue(ConferenceDB.getProgramChair(1).getID() == 1);
    assertTrue(ConferenceDB.getProgramChair(2).getID() == 2);

  }

  @Test
  public void testGetProgramChair() {
    System.out.println(ConferenceDB.getConferenceDate(1));

    assertEquals(ConferenceDB.getConferenceDate(1).toString(), ("5/5/2012"));
    // assertTrue(ConferenceDB.getProgramChair(2) == 5);
    // assertTrue(ConferenceDB.getProgramChair(3) == 8);

  }

  @Test
  public void testGetConference() {
    // Conference conf = db.getConference(1);
    // System.out.println(conf.getConferenceName());
    // System.out.println(conf.getConferenceName());

  }

}
