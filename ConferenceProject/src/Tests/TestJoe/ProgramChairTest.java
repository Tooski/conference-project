/**
 * OBSOLETE, PROGRAM CHAIR CLASS HAS BEEN CHANGED TO SATISFY DATABASE
 * @author Josef Nosov
 * @version 05/31/13
 */

//package Tests.TestJoe;
//
//import static org.junit.Assert.*;
//
//import java.util.GregorianCalendar;
//
//import org.junit.Before;
//import org.junit.Test;
//
//import db.ConferenceDB;
//import db.ConferenceUserDB;
//import db.FeedbackDB;
//import db.FeedbackUserDB;
//import db.UserDB;
//
//import Model.Admin;
//import Model.Conference;
//import Model.ConferenceRole;
//import Model.Date;
//import Model.Feedback;
//import Model.Feedback.Status;
//import Model.User;
//
//public class ProgramChairTest {
//	private Admin my_admin;
//	private User my_programchair;
//	private User my_spc;
//	private User my_reviewer;
//	private Conference my_conference;
//
//	@Before
//	public void setUp() throws Exception {
//
//		my_admin = new Admin(1);
//
//		my_programchair = new User(2);
//		my_spc = new User(4);
//		my_reviewer = new User(3);
//
//	}
//
//	@Test
//	public void testAssignPaper() {
//		my_conference = ConferenceDB.getConference(1);
//		my_admin = new Admin(1);
//
//		my_programchair = new User(2);
//		my_spc = new User(4);
//		my_reviewer = new User(3);
//
//		ConferenceUserDB.clearTable();
//		UserDB.clearTable();
//		ConferenceDB.clearTable();
//		FeedbackDB.clearTable();
//		FeedbackUserDB.clearTable();
//
//		UserDB.createUser("a", "b", "c");
//		UserDB.createUser("d", "e", "f");
//		UserDB.createUser("g", "h", "i");
//
//		my_admin.createConference("New conference", my_programchair, new Date(
//				05, 5, 2012), new Date(05, 5, 2012), new Date(05, 5, 2012),
//				new Date(05, 5, 2012));
//
//		my_conference.createFeedback("Author", "Text", "Other");
//
//		assertTrue(my_programchair.promoteUser(my_conference, my_spc,
//				ConferenceRole.SUBPROGRAM_CHAIR));
//
//		assertTrue(my_conference.getFeedbackList().size() == 1);
//		assertTrue(my_programchair.assignPaper(my_conference.getFeedbackList()
//				.get(0), my_spc));
//		assertTrue(my_spc.getFeedback().size() == 1);
//		assertFalse(my_programchair.assignPaper(my_conference.getFeedbackList()
//				.get(0), my_spc));
//		assertTrue(my_spc.getFeedback().size() == 1);
//		assertFalse(my_programchair.assignPaper(null, my_spc));
//		assertTrue(my_spc.getFeedback().size() == 1);
//		assertFalse(my_programchair.assignPaper(my_conference.getFeedbackList()
//				.get(0), null));
//		assertFalse(my_programchair.assignPaper(my_conference.getFeedbackList()
//				.get(0), my_reviewer));
//		assertTrue(my_reviewer.getFeedback().size() == 0);
//
//		assertTrue(my_programchair.getFeedbackList(my_conference).size() == 0);
//
//		System.out.println(my_programchair.getRoles(my_conference));
//
//		assertTrue(my_programchair.beAssigned(my_conference.getFeedbackList()
//				.get(0)));
//		assertTrue(my_programchair.getFeedbackList(my_conference).size() == 1);
//		// assertTrue(my_conference.createFeedback("Author1", "Text2", "Other3")
//		// == my_conference
//		// .getFeedbackList().size() - 1);
//		assertTrue(my_programchair.beAssigned(my_conference.getFeedbackList()
//				.get(1)));
//		assertTrue(my_programchair.getFeedbackList(my_conference).size() == 2);
//		assertNull(my_programchair.getFeedbackList(null));
//	}
//
//	@Test
//	public void testSubmitApproval() {
//
//		// Feedback fb = my_spc.getListOfConferences().get(0).getFeedbackList()
//		// .get(0);
//		// assertTrue(fb.getStatus(my_programchair.getID()) == Status.PENDING);
//		// assertTrue(my_programchair.setStatus(my_conference
//		// .getFeedbackList().get(0), Status.APPROVED));
//		// assertTrue(fb.getStatus(my_spc.getID()) == Status.PENDING);
//		// assertTrue(fb.getStatus(my_programchair.getID()) == Status.APPROVED);
//		// // assertTrue(fb.getStatus(ConferenceRole.REVIEWER) ==
//		// Status.PENDING);
//		// assertFalse(my_programchair.setStatus(my_conference
//		// .getFeedbackList().get(0), null));
//		// assertFalse(my_programchair.setStatus(null, Status.APPROVED));
//	}
//
//	@Test
//	public void testGetFeedbackList() {
//
//	}
//
//	// @Test
//	// public void testGetUsers() {
//	// assertTrue(my_programchair.getUsers(my_conference).size() == 1);
//	// assertTrue(my_programchair.promoteUser(my_conference, my_spc));
//	// assertTrue(my_programchair.getUsers(my_conference).size() == 2);
//	// assertTrue(my_spc.promoteUser(my_conference, my_reviewer));
//	// assertTrue(my_programchair.getUsers(my_conference).size() == 3);
//	// }
//	//
//	// @Test
//	// public void testPromoteUser() {
//	// assertTrue(my_programchair.promoteUser(my_conference, my_spc));
//	// assertFalse(my_programchair.promoteUser(my_conference, my_spc));
//	// assertTrue(my_spc.promoteUser(my_conference, my_reviewer));
//	// assertTrue(my_programchair.promoteUser(my_conference, my_reviewer));
//	// assertFalse(my_programchair.promoteUser(my_conference, my_reviewer));
//	// assertFalse(my_programchair.promoteUser(null, my_reviewer));
//	// assertFalse(my_programchair.promoteUser(null, null));
//	// assertFalse(my_programchair.promoteUser(my_conference, null));
//	//
//	//
//	// }
//
//	@Test
//	public void testGetStatus() {
//		// assertTrue(my_programchair.promoteUser(my_conference, my_spc));
//		// assertTrue(my_programchair.assignPaper(my_conference.getFeedbackList()
//		// .get(0), my_spc));
//		//
//		//
//		// assertTrue(my_spc.promoteUser(my_conference, my_reviewer));
//		// assertTrue(my_programchair.getStatus(my_conference, my_spc).size() ==
//		// 1);
//		// assertTrue(my_programchair.getStatus(my_conference,
//		// my_reviewer).size() == 0);
//		// assertNull(my_programchair.getStatus(my_conference, null));
//		// assertNull(my_programchair.getStatus(null , my_spc));
//
//	}
//
//}
