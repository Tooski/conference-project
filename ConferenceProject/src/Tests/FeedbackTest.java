package Tests;

import static org.junit.Assert.*;

import java.util.GregorianCalendar;

import model.*;
import model.Feedback.Status;

import org.junit.Before;
import org.junit.Test;


public class FeedbackTest {

	private String the_title = "I Am Chuck Norris";
	private String the_author = "Chuck Norris";
	private String the_text = "This is a Paper on the Life of Chuck Norris";
	private Admin the_admin;
	private User the_PC;
	private User the_programchair;
	private Feedback the_feedback;
	private Conference the_conference;
	private int[] values = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	
//	@Before
//	public void setUp() throws Exception {
//		the_admin = new Admin("Admin", "Steve", 1);
//		//for testing just Paper
//		the_PC = new User ("Admin", "Steve", 2);
//		the_paper = new Paper(the_title, the_author, the_text);
//		the_conference = new Conference("Roundhouse", new GregorianCalendar(), the_PC);
//		the_admin.createConference("New conference", the_programchair, new Date(05,5,2012), new Date(05,5,2012),new Date(05,5,2012),new Date(05,5,2012));
//		the_conference.createFeedback(the_title, the_author, the_text);
//		the_feedback = new Feedback(the_author, the_title, the_text, the_conference);
//	}
//	
//	@Test
//	public void testPaper() {
//		assertTrue("Name of author is Chuck Norris.", the_paper.getAuthor().equals("Chuck Norris"));
//		assertTrue("Name of paper is I Am Chuck Norris.",the_paper.getTitle().equals("I Am Chuck Norris") );
//		assertTrue("Contents of paper are.This is a Paper on the Life of Chuck Norris",the_paper.getPaper().equals("This is a Paper on the Life of Chuck Norris"));
//	}
//	
//	@Test
//	public void testEditPaper() {
//		the_paper.editpaper("The Paper has been edited");
//		assertTrue("Contents of paper are:The Paper has been edited",the_paper.getPaper().equals("The Paper has been edited"));
//		assertFalse(the_paper.editpaper(null));
//	}
//	
//	@Test
//	public void testFeedbackPaper() {
//		assertTrue("Name of author is Chuck Norris.", the_feedback.getPaper().getAuthor().equals("Chuck Norris"));
//		assertTrue("Name of paper is I Am Chuck Norris.",the_feedback.getPaper().getTitle().equals("I Am Chuck Norris") );
//		assertTrue("Contents of paper are:This is a Paper on the Life of Chuck Norris",the_feedback.getPaper().getPaper().equals("This is a Paper on the Life of Chuck Norris"));
//		//edit paper test
//		the_feedback.editPaper("The Paper has been edited");
//		assertTrue("Contents of paper are:The Paper has been edited",the_feedback.getPaper().getPaper().equals("The Paper has been edited"));
//		assertFalse(the_feedback.editPaper(null));
//	}
//	
//	
//	@Test
//	public void testFeedbackStatus() {
//		
//		assertTrue(the_feedback.setStatus(the_PC.getRoles(the_feedback.getConference()).getRole(), Status.APPROVED));
//		assertFalse(the_feedback.setStatus(the_PC.getRoles(the_feedback.getConference()).getRole(), Status.APPROVED));//cannot set status to a lower value
//	}
//	
//	@Test
//	public void testFeedbackReview() {
//		
//		assertTrue(the_feedback.submitReview(null, "something", values) == -1);//cannot submit a null string
//		assertTrue(the_feedback.submitReview("Steve", null, values) == -1);
//		assertTrue(the_feedback.submitReview("Steve", "the review", values) == 0);
//	}
//	
	
}
