///*
// * Conference Test.
// * Quan Le
// */
//package Tests;
//import static org.junit.Assert.*;
//
//import java.util.ArrayList;
//import java.util.GregorianCalendar;
//import java.util.List;
//
//import org.junit.Before;
//import org.junit.Test;
//
//import Model.*
//;
//public class ConferenceTest {
//	
//	private Conference my_conference;
//	private GregorianCalendar my_deadline;
//	private User my_user;//User class is not implemented yet
//	private String conference_name;
//	
//	/**
//	 * Initialize
//	 * @throws Exception
//	 */
//	@Before
//	public void setUp() throws Exception {
//		conference_name = "TestConference";
//		my_deadline = new GregorianCalendar();
//		my_user = new User(0);
//
//	//	my_conference = new Conference(conference_name, my_deadline, my_user);
//	}
//
//	@Test
//	public void testConference() {
//		assertTrue("Name of conference is TestConference.", my_conference.getConferenceName().equals("TestConference"));
//		assertTrue("Username is qle.", my_user.getUserName().equals("qle"));
//		assertTrue("Actual user name is Quan.", my_user.getActualName().equals("Quan"));
////		assertTrue("Password is 123.", my_user.getPassword().equals("123"));
//	}
//
//	@Test
//	public void testChangeDeadline() {
//		GregorianCalendar new_deadline = new GregorianCalendar();
//	//	my_conference.changeDeadline(new_deadline);
//	//	assertTrue(new_deadline.equals(my_conference.getDeadline()));
//		
//	}
//
//	@Test
//	public void testCreateFeedback() {
//		my_conference.createFeedback("Java", "Quan", "Java is fun");
//		assertTrue("Number of feedback is 1.", my_conference.getFeedbackList().size() == 1);
//	}
//
//	@Test
//	public void testGetConferenceName() {
//		assertTrue("Name of conference is TestConference.", my_conference.getConferenceName().equals("TestConference"));
//	}
//
//	@Test
//	public void testGetFeedbackList() {
//		List<Feedback> alist = new ArrayList<>();
//		my_conference.createFeedback("Java", "Quan", "Java is fun");
//		my_conference.createFeedback("Java2", "Quan2", "Java is fun 2");
//		alist = my_conference.getFeedbackList();
//		assertTrue("Size of alist is 2.", alist.size() == 2);
//		
//	}
//
//	@Test
//	public void testGetUniqueFeedback() {
//		my_conference.createFeedback("Java", "Quan", "Java is fun");
//		my_conference.createFeedback("Java2", "Quan2", "Java is fun 2");
//	//	assertTrue(my_conference.getUniqueFeedback(0).getPaper().getPaper().equals("Java is fun"));
//	//	assertTrue(my_conference.getUniqueFeedback(1).getPaper().getPaper().equals("Java is fun 2"));
//	}
//
//}
