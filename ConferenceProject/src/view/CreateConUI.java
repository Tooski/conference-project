
package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Observable;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.Admin;
import model.Date;
import model.User;

import swing.custom.JCustomButton;
import swing.custom.JCustomComboBox;
import swing.custom.JCustomLabel;
import swing.custom.JCustomList;
import swing.custom.JCustomTextField;
import swing.custom.JDialogBox;

import db.UserDB;

/**
 * Create conference GUI.
 * 
 * @author Matt Cookman and Josef Nosov
 */
public class CreateConUI extends Observable implements GUI {

  private JCustomTextField con_name;

  private JCustomComboBox<Integer> rev_year;
  private JCustomComboBox<Integer> rev_month;
  private JCustomComboBox<Integer> rev_day;

  private JCustomComboBox<Integer> au_year;
  private JCustomComboBox<Integer> au_month;
  private JCustomComboBox<Integer> au_day;

  private JCustomComboBox<Integer> con_year;
  private JCustomComboBox<Integer> con_month;
  private JCustomComboBox<Integer> con_day;

  private JCustomComboBox<Integer> sub_year;
  private JCustomComboBox<Integer> sub_month;
  private JCustomComboBox<Integer> sub_day;
  private final JCustomTextField combo;
  private JButton finish;
  private JCustomList<User> tb;
  private User my_user;
  

  /**
   * If we're removing items.
   */
  private final boolean[] my_removing = {false, false, false, false};

  /**
   * the user's id.
   */
  private int my_user_id;

  /**
   * 
   * Constructor of CreateConUI.
   * @author Matt Cookman
   * @param user the admin user of this conference
   */
  public CreateConUI(final Admin user) {
    super();
    my_user = user;
    final List<User> users = new ArrayList<>(UserDB.getAllUsers());
    users.remove(user);
    tb = new JCustomList<>();
    tb.addElements(users);
    con_name = new JCustomTextField(30);

    finish = new JCustomButton("Finish");
    finish.setEnabled(false);
    combo = new JCustomTextField(20);

    rev_year = new JCustomComboBox<Integer>();
    rev_month = new JCustomComboBox<Integer>();
    rev_day = new JCustomComboBox<Integer>();

    au_year = new JCustomComboBox<Integer>();
    au_month = new JCustomComboBox<Integer>();
    au_day = new JCustomComboBox<Integer>();

    con_year = new JCustomComboBox<Integer>();
    con_month = new JCustomComboBox<Integer>();
    con_day = new JCustomComboBox<Integer>();

    sub_year = new JCustomComboBox<Integer>();
    sub_month = new JCustomComboBox<Integer>();
    sub_day = new JCustomComboBox<Integer>();

    fillYear(rev_year);
    fillMonth(rev_month);
    fillDay(rev_day, rev_month.getSelectedIndex() + 1, rev_year.getSelectedIndex() + 1);

    fillYear(au_year);
    fillMonth(au_month);
    fillDay(au_day, au_month.getSelectedIndex() + 1, au_year.getSelectedIndex() + 1);

    fillYear(con_year);
    fillMonth(con_month);
    fillDay(con_day, con_month.getSelectedIndex() + 1, con_year.getSelectedIndex() + 1);

    fillYear(sub_year);
    fillMonth(sub_month);
    fillDay(sub_day, sub_month.getSelectedIndex() + 1, sub_year.getSelectedIndex() + 1);

    con_day.setPreferredSize(2);
    sub_day.setPreferredSize(2);
    rev_day.setPreferredSize(2);
    au_day.setPreferredSize(2);
    con_month.setPreferredSize(2);
    sub_month.setPreferredSize(2);
    rev_month.setPreferredSize(2);
    au_month.setPreferredSize(2);
    con_year.setPreferredSize(4);
    sub_year.setPreferredSize(4);
    rev_year.setPreferredSize(4);
    au_year.setPreferredSize(4);

    con_year.addActionListener(setYearListener(con_month, con_year, sub_month, sub_year, null,
                                               null));
    con_month.addActionListener(setMonthListener(con_day, con_month, con_year, sub_day,
                                                 sub_month, sub_year, null, null, null));
    con_day.addActionListener(setMonthListener(con_day, con_month, con_year, sub_day,
                                               sub_month, sub_year, null, null, null));
    sub_year.addActionListener(setYearListener(sub_month, sub_year, rev_month, rev_year,
                                               con_month, con_year));
    sub_month.addActionListener(setMonthListener(sub_day, sub_month, sub_year, rev_day,
                                                 rev_month, rev_year, con_day, con_month,
                                                 con_year));
    rev_day.addActionListener(setMonthListener(rev_day, rev_month, rev_year, au_day, au_month,
                                               au_year, sub_day, sub_month, sub_year));
    rev_year.addActionListener(setYearListener(rev_month, rev_year, au_month, au_year,
                                               sub_month, sub_year));
    rev_month.addActionListener(setMonthListener(rev_day, rev_month, rev_year, au_day,
                                                 au_month, au_year, sub_day, sub_month,
                                                 sub_year));
    rev_day.addActionListener(setMonthListener(rev_day, rev_month, rev_year, au_day, au_month,
                                               au_year, sub_day, sub_month, sub_year));
    au_year.addActionListener(setYearListener(au_month, au_year, null, null, rev_month,
                                              rev_year));
    au_month.addActionListener(setMonthListener(au_day, au_month, au_year, null, null, null,
                                                rev_day, rev_month, rev_year));
    au_day.addActionListener(setMonthListener(au_day, au_month, au_year, null, null, null,
                                              rev_day, rev_month, rev_year));
    con_name.addKeyListener(new KeyAdapter() {

      @Override
      public void keyReleased(KeyEvent arg0) {
        finish.setEnabled(values());
      }

    });
    setupComponents();

  }

  /**
   * An ActionListener to set month.
   * 
   * @author Josef Nosov
   * @param day the day
   * @param month the month
   * @param year the year
   * @param day1 the day #1
   * @param month1 the month #1
   * @param year1 the year #1
   * @param day2 the day #2
   * @param month2 the month #2
   * @param year2 the year #2
   * @return the ActionListener
   */
  private ActionListener setMonthListener(final JComboBox<Integer> day,
                                          final JComboBox<Integer> month,
                                          final JComboBox<Integer> year,
                                          final JComboBox<Integer> day1,
                                          final JComboBox<Integer> month1,
                                          final JComboBox<Integer> year1,
                                          final JComboBox<Integer> day2,
                                          final JComboBox<Integer> month2,
                                          final JComboBox<Integer> year2) {
    return new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (!my_removing[0]) {

          final int current_month = Calendar.getInstance().get(Calendar.MONTH) + 1;
          final int current_day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
          final int year_int = Integer.parseInt(year.getSelectedItem().toString());
          final int month_int = Integer.parseInt(month.getSelectedItem().toString());
          final int day_int = Integer.parseInt(day.getSelectedItem().toString());

          if (day2 != null && month2 != null && year2 != null) {
            final int year2_int = Integer.parseInt(year2.getSelectedItem().toString());
            final int month2_int = Integer.parseInt(month2.getSelectedItem().toString());
            final int day2_int = Integer.parseInt(day2.getSelectedItem().toString());

            my_removing[0] = true;
            day.removeAllItems();
            my_removing[0] = false;
            if (year_int == year2_int) {
              if (month_int == month2_int) {
                if (month_int == current_month) {
                  for (int x = current_day; x <= day2_int; x++) {
                    day.addItem(x);
                  }
                } else {
                  for (int x = 1; x <= day2_int; x++) {
                    day.addItem(x);
                  }
                }
              } else {
                if (month_int == current_month) {
                  for (int x = current_day; x <= Date.getDays(month_int, year_int); x++) {
                    day.addItem(x);
                  }
                } else {
                  for (int x = 1; x <= Date.getDays(month_int, year_int); x++) {
                    day.addItem(x);
                  }
                }
              }
            } else {
              if (month_int == current_month) {
                for (int x = current_day; x <= Date.getDays(month_int, year_int); x++) {
                  day.addItem(x);
                }
              } else {

                for (int x = 1; x <= Date.getDays(month_int, year_int); x++) {
                  day.addItem(x);
                }
              }
            }
          } else {
            if (!my_removing[0]) {
              if (year.equals(con_year)) {
                my_removing[0] = true;
                day.removeAllItems();
                my_removing[0] = false;
                if (month_int == current_month) {
                  for (int x = current_day; x <= Date.getDays(month_int, year_int); x++) {
                    day.addItem(x);
                  }
                } else {
                  for (int x = 1; x <= Date.getDays(month_int, year_int); x++) {
                    day.addItem(x);
                  }
                }
              }
            }

          }
          day.setSelectedItem(day_int);
          if (!my_removing[2])
            if (day1 != null && month1 != null && year1 != null) {
              int month_ph = Integer.parseInt(month1.getSelectedItem().toString());
              my_removing[0] = true;
              month1.removeAllItems();
              my_removing[0] = false;

              checkMonth(month1, year1, month, year);
              month1.setSelectedItem(month_ph);
            }
          finish.setEnabled(values());

        }

      }
    };
  }

  /**
   * ActionListener to set year.
   * 
   * @author Josef Nosov
   * @param month the month
   * @param year the year
   * @param month1 the month #1
   * @param year1 the year #1
   * @param month2 the month #2
   * @param year2 the year #2
   * @return ActionListener
   */
  private ActionListener setYearListener(final JComboBox<Integer> month,
                                         final JComboBox<Integer> year,
                                         final JComboBox<Integer> month1,
                                         final JComboBox<Integer> year1,
                                         final JComboBox<Integer> month2,
                                         final JComboBox<Integer> year2) {
    return new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        int current_year = Calendar.getInstance().get(Calendar.YEAR);
        int current_month = Calendar.getInstance().get(Calendar.MONTH) + 1;

        if (!my_removing[1]) {
          int year_int = Integer.parseInt(year.getSelectedItem().toString());
          int month_int = Integer.parseInt(month.getSelectedItem().toString());
          my_removing[2] = my_removing[0] = true;
          month.removeAllItems();
          my_removing[0] = false;
          if (month2 != null && year2 != null) {
            checkMonth(month, year, month2, year2);

          } else {
            if (!my_removing[0]) {
              if (year.equals(con_year)) {
                if (year_int == current_year) {
                  for (int x = current_month; x <= 12; x++) {
                    month.addItem(x);
                  }
                } else {
                  for (int x = 1; x <= 12; x++) {
                    month.addItem(x);
                  }
                }
              }
            }

          }
          month.setSelectedItem(month_int);

          if (month1 != null && year1 != null) {
            int year_ph = Integer.parseInt(year1.getSelectedItem().toString());
            my_removing[1] = true;
            year1.removeAllItems();
            my_removing[1] = false;

            for (int x = current_year; x <= year_int; x++) {
              year1.addItem(x);
            }
            year1.setSelectedItem(year_ph);
          }
          finish.setEnabled(values());

        }
        my_removing[2] = false;

      }
      // }
    };
  }

  /**
   * Private method to check month.
   * 
   * @author Josef Nosov
   * @param month month
   * @param year year
   * @param month2 month #2
   * @param year2 year #2
   */
  private void checkMonth(final JComboBox<Integer> month, final JComboBox<Integer> year,
                          final JComboBox<Integer> month2, final JComboBox<Integer> year2) {
    int year_int = Integer.parseInt(year.getSelectedItem().toString());
    int year2_int = Integer.parseInt(year2.getSelectedItem().toString());
    int month2_int = Integer.parseInt(month2.getSelectedItem().toString());
    int current_year = Calendar.getInstance().get(Calendar.YEAR);
    int current_month = Calendar.getInstance().get(Calendar.MONTH) + 1;
    if (year_int == year2_int) {
      if (year_int == current_year) {
        for (int x = current_month; x <= month2_int; x++) {
          month.addItem(x);
        }
      } else {
        for (int x = 1; x <= month2_int; x++) {
          month.addItem(x);
        }
      }
    } else {
      if (year_int == current_year) {

        for (int x = current_month; x <= 12; x++) {
          month.addItem(x);
        }
      } else {

        for (int x = 1; x <= 12; x++) {
          month.addItem(x);
        }
      }
    }
  }


  /**
   * Sets up components for this GUI.
   */
  private void setupComponents() {
    con_name.setEditable(true);
    finish.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent arg0) {
        int i =
            ((Admin) my_user).createConference(con_name.getText(), UserDB.getUser(my_user_id),
                                               new Date((Integer) con_month.getSelectedItem(),
                                                        (Integer) con_day.getSelectedItem(),
                                                        (Integer) con_year.getSelectedItem()),
                                               new Date((Integer) au_month.getSelectedItem(),
                                                        (Integer) au_day.getSelectedItem(),
                                                        (Integer) au_year.getSelectedItem()),
                                               new Date((Integer) rev_month.getSelectedItem(),
                                                        (Integer) rev_day.getSelectedItem(),
                                                        (Integer) rev_year.getSelectedItem()),
                                               new Date((Integer) sub_month.getSelectedItem(),
                                                        (Integer) sub_day.getSelectedItem(),
                                                        (Integer) sub_year.getSelectedItem()));
        setChanged();
        notifyObservers(i);
      }

    });
    JPanel master_panel = new JPanel(new BorderLayout());
    JPanel rev_panel = new JPanel(new BorderLayout());
    JPanel au_panel = new JPanel(new BorderLayout());
    JPanel con_panel = new JPanel(new BorderLayout());
    JPanel sub_panel = new JPanel(new BorderLayout());
    JPanel name_panel = new JPanel(new BorderLayout());
    JPanel dates_panel = new JPanel(new GridLayout(0, 1));
    JPanel button_panel = new JPanel(new BorderLayout());
    JPanel PC_panel = new JPanel(new BorderLayout());

    rev_panel.add(new JCustomLabel("Reviewer deadline"), BorderLayout.WEST);
    JPanel rev = new JPanel();
    rev.add(rev_month, BorderLayout.WEST);
    rev.add(rev_day, BorderLayout.CENTER);
    rev.add(rev_year, BorderLayout.EAST);
    rev_panel.add(rev, BorderLayout.EAST);

    au_panel.add(new JCustomLabel("Author deadline"), BorderLayout.WEST);
    JPanel au = new JPanel();
    au.add(au_month, BorderLayout.CENTER);
    au.add(au_day, BorderLayout.CENTER);
    au.add(au_year, BorderLayout.EAST);
    au_panel.add(au, BorderLayout.EAST);

    con_panel.add(new JCustomLabel("Conference date"), BorderLayout.WEST);
    JPanel con = new JPanel();
    con.add(con_month, BorderLayout.WEST);
    con.add(con_day, BorderLayout.CENTER);
    con.add(con_year, BorderLayout.EAST);
    con_panel.add(con, BorderLayout.EAST);

    sub_panel.add(new JCustomLabel("Subprogram Chair deadline"), BorderLayout.WEST);
    JPanel sub = new JPanel();
    sub.add(sub_month, BorderLayout.WEST);
    sub.add(sub_day, BorderLayout.CENTER);
    sub.add(sub_year, BorderLayout.EAST);
    sub_panel.add(sub, BorderLayout.EAST);

    combo.addKeyListener(new KeyListener() {
      @Override
      public void keyPressed(KeyEvent arg0) {
        finish.setEnabled(false);

      }

      @Override
      public void keyReleased(KeyEvent arg0) {

      }

      @Override
      public void keyTyped(KeyEvent arg0) {

      }
    });

    JScrollPane sp = new JScrollPane(tb);
    sp.setPreferredSize(new Dimension(100, 100));
    tb.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(ListSelectionEvent arg0) {
        finish.setEnabled(values());
        if (tb.getSelectedIndex() != -1)
          my_user_id = tb.getSelectedValue().getID();

      }

    });

    final JButton button = new JCustomButton("Search");
    button.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent arg0) {
        for (User u : UserDB.getAllUsers()) {
          if (u.getUserName().equals(combo.getText())) {
            finish.setEnabled(true);
            my_user_id = u.getID();
          }
        }
        if (!finish.isEnabled())
          JDialogBox.createDialogBox(new Object[] {"Account not found!"}, null);
      }

    });
    PC_panel.add(sp, BorderLayout.CENTER);
    JPanel finish_button = new JPanel(new BorderLayout());
    finish_button.add(finish);
    finish_button.setBorder(new EmptyBorder(6, 0, 0, 0));
    dates_panel.add(con_panel);
    dates_panel.add(sub_panel);
    dates_panel.add(rev_panel);

    dates_panel.add(au_panel);

    button_panel.add(new JCustomLabel("Select Program Chair"), BorderLayout.NORTH);
    button_panel.add(PC_panel, BorderLayout.CENTER);
    button_panel.add(finish_button, BorderLayout.SOUTH);
    name_panel.add(new JCustomLabel("Conference name "), BorderLayout.WEST);
    name_panel.add(con_name.getPanel(), BorderLayout.SOUTH);

    master_panel.add(name_panel, BorderLayout.NORTH);
    master_panel.add(dates_panel, BorderLayout.CENTER);
    master_panel.add(button_panel, BorderLayout.SOUTH);

    main.add(master_panel);

  }

  private boolean values() {
    boolean b = tb.getSelectedIndex() >= 0 && !con_name.getText().isEmpty();
    return b;
  }

  /**
   * Private method to check date.
   * @author Josef Nosov
   * 
   * @param month1 month #1
   * @param day1 day #1
   * @param year1 year #1
   * @param month2 month #2
   * @param day2 day #2
   * @param year2 year #2
   * @return true/false
   */
  @SuppressWarnings("unused")
  private boolean checkDates(int month1, int day1, int year1, int month2, int day2, int year2) {
    int current_year = Calendar.getInstance().get(Calendar.YEAR);
    int current_day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    int current_month = Calendar.getInstance().get(Calendar.MONTH) + 1;
    boolean greater_check = false;
    boolean lesser_check = false;
    if (year1 == current_year) {
      if (month1 == current_month) {
        if (day1 >= current_day) {
          greater_check = true;
        }
      } else if (month1 > current_month) {
        greater_check = true;
      }
    } else if (year1 > current_year)
      greater_check = true;

    if (year1 == year2) {
      if (month1 == month2) {
        if (day1 <= day2) {
          lesser_check = true;
        }
      } else if (month1 < month2) {
        lesser_check = true;
      }
    } else if (year1 < year2) {
      lesser_check = true;
    }

    return greater_check && lesser_check;

  }

  JPanel main = new JPanel();

  public JPanel display() {

    main.setVisible(true);
    return main;
  }

  /**
   * Fills day.
   * 
   * @author Josef Nosov
   * @param day2 day
   * @param month month
   * @param year year
   */
  private void fillDay(final JComboBox<Integer> day2, int month, int year) {
    for (int i = Calendar.getInstance().get(Calendar.DAY_OF_MONTH); i < Date.getDays(month,
                                                                                     year) + 1; i++) {
      day2.addItem(i);
    }

  }

  /**
   * Fills month.
   * 
   * @author Josef Nosov
   * @param month2 month
   */
  private void fillMonth(JComboBox<Integer> month2) {
    if (month2.equals(con_month)) {
      for (int i = Calendar.getInstance().get(Calendar.MONTH) + 1; i <= 12; i++) {
        month2.addItem(i);
      }
    } else {
      month2.addItem(Calendar.getInstance().get(Calendar.MONTH) + 1);

    }
  }

  /**
   * Fills year.
   * 
   * @author Josef Nosov
   * @param year2 year
   */
  private void fillYear(JComboBox<Integer> year2) {
    if (year2.equals(con_year)) {
      for (int i = 0; i < 10; i++) {
        year2.addItem(Calendar.getInstance().get(Calendar.YEAR) + i);
      }
    } else {
      year2.addItem(Calendar.getInstance().get(Calendar.YEAR));
    }
  }
}
