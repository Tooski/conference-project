
package view.paper;
import db.FeedbackDB;
import db.FeedbackUserDB;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import model.Conference;
import model.ConferenceRole;
import model.Feedback;
import model.Feedback.Status;
import model.User;
import swing.custom.JCustomButton;
import swing.custom.JCustomComboBox;
import swing.custom.JCustomLabel;
import swing.custom.JCustomList;
import swing.custom.JDefaultFont;
import swing.custom.JDialogBox;
import view.ReviewFormUI;

/**
 * The author's panel to view, edit, remove or check reviews.
 * 
 * @author Josef Nosov
 * @version 06/06/2013
 * 
 */
public class AuthorPanelGUI extends AbstractSubpanelGUI {

  /**
   * Height for JTextPane.
   */
  private static final int HEIGHT = 100;

  /**
   * Width for JTextPane.
   */
  private static final int WIDTH = 500;

  /**
   * The default font size.
   */
  private static final int FONT_SIZE = 16;

  /**
   * Remove button.
   */
  private static final String REMOVE = "Remove";

  /**
   * Edit button.
   */
  private static final String EDIT = "Edit";

  /**
   * The padding for the border.
   */
  private static final int PADDING = 10;

  /**
   * the current conference.
   */
  private Conference my_conference;

  /**
   * The paper's current status.
   */
  private Status my_paper_status;

  /**
   * The feedback selected.
   */
  private Feedback my_feedback;

  /**
   * Default author panel constructor.
   * 
   * @param the_conference the conference selected.
   * @param the_feedback the feedback selected.
   * @param the_user the user selected.
   * @param the_role the role selected.
   */
  public AuthorPanelGUI(final Conference the_conference, final Feedback the_feedback, 
                        final User the_user, final ConferenceRole the_role) {
    my_conference = the_conference;
    my_feedback = the_feedback;
    my_panel = new JPanel(new BorderLayout());

    my_panel.add(createObjects(the_user, the_role));
    my_panel.setBorder(BorderFactory.createCompoundBorder(
            BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY), 
            new EmptyBorder(PADDING, PADDING, PADDING, PADDING)));
    my_paper_status =
        FeedbackUserDB.getStatus(the_feedback.getID(), the_user.getID(), 
                                 the_conference.getID(), the_role.getRole());
    my_panel.add(createMainPanel());
    my_panel.add(createAbstract(), BorderLayout.SOUTH);
  }

  /**
   * Create's a list of objects defining the paper.
   * 
   * @param the_user the user of the paper.
   * @param the_role the user's role.
   * @return returns the paper information.
   */
  private JCustomList<Object> createObjects(final User the_user, 
                                            final ConferenceRole the_role) {
    final ReviewFormUI reviewform = new ReviewFormUI(my_panel);
    final JCustomList<Object> object = new JCustomList<>();
    object.setVisible(false);
    object.addElement(my_feedback);
    object.addElement(the_user);
    object.addElement(the_role);
    object.addElement(reviewform);
    return object;
  }

  /**
   * Creates the abstract.
   * @return returns the scroll pane.
   */
  private JScrollPane createAbstract() {
    final JTextPane jtp = new JTextPane();
    jtp.setPreferredSize(new Dimension(WIDTH, HEIGHT));
    jtp.setText("Abstract: " + my_feedback.getPaperAbstract());
    jtp.setEditable(false);
    jtp.setFont(new JDefaultFont().getFont(FONT_SIZE));
    return new JScrollPane(jtp);
  }

  /**
   * Creates the main panel.
   * 
   * @return returns the panel with all subpanels.
   */
  private JPanel createMainPanel() {
    final JPanel paper_info = new JPanel(new BorderLayout());
    final JPanel left_panel = new JPanel();
    final JLabel status = new JCustomLabel("Status: " + my_paper_status);
    final JPanel right_panel = new JPanel();
    final GridBagConstraints gbc = new GridBagConstraints();
    left_panel.setLayout(new BoxLayout(left_panel, BoxLayout.Y_AXIS));
    left_panel.add(new JCustomLabel("Title: " + my_feedback.getTitle()));
    left_panel.add(status);
    paper_info.add(left_panel, BorderLayout.WEST);
    right_panel.setLayout(new GridBagLayout());
    gbc.gridy = 0;
    right_panel.add(createButton("View", viewActionListener()), gbc);
    right_panel.add(createButton(EDIT, editActionListener()), gbc);
    right_panel.add(createButton(REMOVE, removeActionListener()), gbc);
    right_panel.add(createComboBox(), gbc);
    paper_info.add(right_panel, BorderLayout.EAST);
    return paper_info;
  }

  /**
   * Creates a combo box of Review Form.
   * 
   * @return returns combo box.
   */
  private JComboBox<ReviewFormUI> createComboBox() {
    final JCustomComboBox<ReviewFormUI> combo_box = new JCustomComboBox<>();
    combo_box.setVisible(my_paper_status == Status.APPROVED_BY_PROGRAM_CHAIR);
    for (ReviewFormUI rf : FeedbackDB.getAllReviews(my_feedback.getID(), my_conference)) {
      combo_box.addItem(rf);

    }
    combo_box.setFontSize(FONT_SIZE);
    combo_box.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        JDialogBox.createDialogBox((ReviewFormUI) combo_box.getSelectedItem(), "Review");
      }
    });
    return combo_box;
  }

  /**
   * Creates a JPanel filled with buttons.
   * 
   * @param the_string the name of the button.
   * @param the_al the action listener.
   * @return returns a panel with the JButton.
   */
  private JPanel createButton(final String the_string, final ActionListener the_al) {
    final JButton button_3 = new JCustomButton(the_string);
    button_3.setFont(new JDefaultFont().getFont(FONT_SIZE));
    button_3.addActionListener(the_al);
    if (the_string.equals(EDIT)) {
      button_3.setEnabled((checkDate(my_conference.getAuthorDeadline()) && 
          !FeedbackUserDB.isAssigned(my_feedback.getID())) ||
                          my_paper_status == Status.APPROVED_BY_PROGRAM_CHAIR);
    } else if (the_string.equals(REMOVE)) {
      button_3.setEnabled(checkDate(my_conference.getAuthorDeadline()));
      button_3.setVisible(checkDate(my_conference.getAuthorDeadline()) &&
                          !FeedbackUserDB.isAssigned(my_feedback.getID()));
    }
    final JPanel view = new JPanel(new BorderLayout());
    view.add(button_3);
    view.setBorder(new EmptyBorder(PADDING, PADDING, PADDING, PADDING));
    return view;

  }

  /**
   * View button action listener.
   * 
   * @return returns the action listener.
   */
  private ActionListener viewActionListener() {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        try {
          Desktop.getDesktop().open((File) my_feedback.getPaper());
        } catch (final IOException e) {
        }

      }
    };
  }

  /**
   * Edit button action listener.
   * 
   * @return returns the action listener.
   */
  private ActionListener editActionListener() {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        final JFileChooser jfc = new JFileChooser();
        if (jfc.showDialog(null, "Submit") == JFileChooser.APPROVE_OPTION) {
          JDialogBox.createDialogBox("Successfully edited paper", "");
          my_feedback.editPaper(jfc.getSelectedFile());
        }
      }
    };
  }

  /**
   * Remove button action listener.
   * 
   * @return returns the action listener.
   */
  private ActionListener removeActionListener() {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        JDialogBox.createDialogBox("Successfully removed paper", "");
        FeedbackDB.deletePaper(my_feedback.getID());
        setChanged();
        notifyObservers(my_panel);

      }
    };
  }

}
