
package view.paper;
import db.FeedbackDB;
import db.FeedbackUserDB;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import model.Conference;
import model.ConferenceRole;
import model.Feedback;
import model.Feedback.Status;
import model.User;
import swing.custom.JCustomButton;
import swing.custom.JCustomComboBox;
import swing.custom.JCustomLabel;
import swing.custom.JCustomList;
import swing.custom.JDefaultFont;
import swing.custom.JDialogBox;
import view.ReviewFormUI;



/**
 * ChairPanelGUI.
 * @author Josef Nosov
 * @version 06/06/2013
 *
 */
public class ChairPanelGUI extends AbstractSubpanelGUI {

  
  /**
   * The height.
   */
  private static final int HEIGHT = 100;
  
  /**
   * The width.
   */
  private static final int WIDTH = 500;
  
  /**
   * Padding.
   */
  private static final int PADDING = 10;
  
  /**
   * The status.
   */
  private static final String STATUS = "Status: ";
  
  /**
   * The font.
   */
  private static final int FONT_SIZE = 16;
  
  /**
   * View reviews.
   */
  private static final String VIEW_REVIEWS = "View Reviews";
  
  /**
   * View papers.
   */
  private static final String VIEW_PAPER = "View Paper";
  
  /**
   * Reject.
   */
  private static final String REJECT = "Reject";
  
  /**
   * Approve.
   */
  private static final String APPROVE = "Approve";
  
  /**
   * the current role.
   */
  private ConferenceRole my_current_role;
  
  /**
   * The author's conference.
   */
  private ConferenceRole my_author_conference_role;
  
  /**
   * The conferences.
   */
  private Conference my_conference;
  
  /**
   * The users.
   */
  private User my_user;
  
  /**the author users.
   * 
   */
  private User my_author_user;
  
  /**
   * The current_status.
   */
  private Status my_current_status;
  /**
   * The feedback.
   */
  private Feedback my_feedback;
  
  /**
   * The reject button.
   */
  private JButton my_reject_button;
  
  /**
   * The accept button.
   */
  private JButton my_accept_button;
  
  /**
   * The view button.
   */
  private JButton my_view_button;
  
  /**
   * The review button.
   */
  private JButton my_review_button;
  /**
   * The reviews.
   */
  private JCustomComboBox<ReviewFormUI> my_reviews;
  /**
   * The status.
   */
  private final JCustomLabel my_status;


  /**
   * 
   * @param the_user the user.
   * @param the_conference the conference.
   * @param the_feedback the feedback.
   * @param the_author_user the author.
   * @param the_author_role the role.
   * @param the_conference_role the conference role.
   */
  public ChairPanelGUI(final User the_user, final Conference the_conference, 
                       final Feedback the_feedback, final User the_author_user, 
                       final ConferenceRole the_author_role, final ConferenceRole 
                       the_conference_role) {
    my_author_conference_role = the_author_role;
    my_author_user = the_author_user;
    my_feedback = the_feedback;
    my_conference = the_conference;
    my_current_role = the_conference_role;
    my_user = the_user;
    my_panel = new JPanel(new BorderLayout());
    my_current_status =
        FeedbackUserDB.getStatus(the_feedback.getID(), the_author_user.getID(), 
        the_conference.getID(), the_author_role.getRole());
    my_status = new JCustomLabel(STATUS + my_current_status);
    init();
  }
  
  
  /**
   * init the panels.
   */
  private void init() {
    my_panel.add(createObject());
    my_panel.setBorder(BorderFactory.createCompoundBorder(
       BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY), 
       new EmptyBorder(PADDING, PADDING, PADDING, PADDING)));

    my_accept_button = createButton(APPROVE);
    my_reject_button = createButton(REJECT);
    my_view_button = createButton(VIEW_PAPER);
    my_review_button = createButton(VIEW_REVIEWS);
    final JPanel reviews = createPanel(my_view_button);
    my_accept_button.addActionListener(getAcceptListener(my_accept_button, my_reject_button));
    my_reject_button.addActionListener(getAcceptListener(my_reject_button, my_accept_button));
    my_view_button.addActionListener(viewActionListener());
    my_reviews = createReviews();
    my_review_button.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        JDialogBox.createDialogBox(new ReviewFormUI(FeedbackUserDB.getReview(
          my_feedback.getID(), my_author_user.getID(), my_conference.getID(), 
          my_author_conference_role.getRole()), FeedbackUserDB.getScores(
            my_feedback.getID(), my_author_user.getID(), my_conference.getID(), 
            my_author_conference_role.getRole())), "Review Form");
      }
    });
    final JPanel paper_info = new JPanel(new BorderLayout());
    paper_info.add(leftPanel(), BorderLayout.WEST);
    paper_info.add(rightPanel(reviews), BorderLayout.EAST);
    my_panel.add(paper_info);
    addAbstract();
  }
  
  /**
   * Adds the abstracts.
   */
  private void addAbstract() {
    final JTextPane jtp = new JTextPane();
    jtp.setPreferredSize(new Dimension(WIDTH, HEIGHT));
    jtp.setText("Abstract: " + my_feedback.getPaperAbstract());
    jtp.setEditable(false);
    jtp.setFont(new JDefaultFont().getFont(FONT_SIZE));
    my_panel.add(new JScrollPane(jtp), BorderLayout.SOUTH);

  }
  
  /**
   * Creates the abstract.
   * @return returns objects.
   */
  private JCustomList<Object> createObject() {
    final JCustomList<Object> object = new JCustomList<>();
    object.setVisible(false);
    object.addElement(my_feedback);
    object.addElement(my_author_user);
    object.addElement(my_author_conference_role);
    return object;
  }
  
  
  /**
   * The panel.
   * @return returns the left panel.
   */
  private JPanel leftPanel() {
    final JPanel left_panel = new JPanel();
    left_panel.setLayout(new BoxLayout(left_panel, BoxLayout.Y_AXIS));
    left_panel.add(new JCustomLabel("Author: " + my_feedback.getAuthor()));
    left_panel.add(new JCustomLabel("Title: " + my_feedback.getTitle()));
    if (my_current_role == ConferenceRole.SUBPROGRAM_CHAIR) {
      left_panel.add(new JCustomLabel("Review by: " + my_conference.getSubProgramDeadline()));
    }

    left_panel.add(new JCustomLabel(
       "Assigned to: " + my_author_user + " (" + my_author_conference_role + ")"));
    left_panel.add(my_status);
    return left_panel;
  }
  
  /**
   * The right panel.
   * @param the_reviews the reviews.
   * @return returns panels.
   */
  private JPanel rightPanel(final JPanel the_reviews) {
    final JPanel right_panel = new JPanel();
    right_panel.setLayout(new GridBagLayout());
    final GridBagConstraints gbc = new GridBagConstraints();
    gbc.gridy = 0;

    the_reviews.add((my_current_role == ConferenceRole.SUBPROGRAM_CHAIR) ? my_review_button
        : my_reviews);

    right_panel.add(createPanel(my_accept_button), gbc);
    right_panel.add(createPanel(my_reject_button), gbc);
    right_panel.add(createPanel(my_view_button), gbc);
    right_panel.add(the_reviews, gbc);
    return right_panel;
  }

  /**
   * The accept listener.
   * @param the_button1 the button1.
   * @param the_button2 the botton2.
   * @return returns listener.
   */
  private ActionListener getAcceptListener(final JButton the_button1, 
                                           final JButton the_button2) {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        getAction(the_button1, the_button2); 

      }

    };

  }
  
  /**
   * Same as Action.
   * @param the_button1 the button1.
   * @param the_button2 the button2.
   */
  private void getAction(final JButton the_button1, final JButton the_button2) {
    final String feedback =
        FeedbackUserDB.getReview(my_feedback.getID(), my_author_user.getID(),
                                 my_conference.getID(),
                                 my_author_conference_role.getRole());
    final int[] values =
        FeedbackUserDB.getScores(my_feedback.getID(), my_author_user.getID(),
                                 my_conference.getID(),
                                 my_author_conference_role.getRole());
    if (my_current_role == ConferenceRole.PROGRAM_CHAIR) {
      final Status s =
          (the_button1.getText().equals(APPROVE)) ? Status.APPROVED_BY_PROGRAM_CHAIR
              : Status.REJECTED_BY_PROGRAM_CHAIR;
      FeedbackUserDB.setStatusAll(my_feedback.getID(), my_conference.getID(), s.getValue());
      the_button1.setEnabled(false);
      the_button2.setEnabled(false);
      my_status.setText(STATUS + s);
    } else {
      final Status s =
          (the_button1.getText().equals(APPROVE)) ? Status.RECOMMENDED_BY_SUBPROGRAM_CHAIR
              : Status.REJECTED_BY_SUBPROGRAM_CHAIR;
      FeedbackUserDB.setStatusAll(my_feedback.getID(), my_conference.getID(), s.getValue());
      FeedbackUserDB.setReview(my_feedback.getID(), my_user.getID(), feedback, values,
                               my_conference.getID(), my_current_role.getRole());
      FeedbackUserDB.setStatusByAssigned(my_feedback.getID(), my_user.getID(), s,
                                         my_conference.getID(), my_current_role.getRole(),
                                         my_conference.getProgramChair().getID(),
                                         ConferenceRole.PROGRAM_CHAIR.getRole());
//      FeedbackUserDB.setStatus(my_feedback.getID(), user.getID(), s, my_conference.getID(),
//                               role.getRole());
      the_button1.setEnabled(false);
      the_button2.setEnabled(false);
      my_status.setText(STATUS + s);

    }
  }
  

  /**
   * Creates the buttons.
   * @param the_text the button's text.
   * @return returns the buttons.
   */
  private JButton createButton(final String the_text) {

    final JButton button_1 = new JCustomButton(the_text);
    if (!the_text.equals(VIEW_PAPER) && !the_text.equals(VIEW_REVIEWS)) {
      button_1.setEnabled((my_current_role == ConferenceRole.SUBPROGRAM_CHAIR) ? checkDate(
         my_conference.getSubProgramDeadline()) && my_current_status == 
         Status.REVIEWED_BY_REVIEWER : my_current_status == 
         Status.REVIEWED_BY_REVIEWER ||  my_current_status == 
         Status.RECOMMENDED_BY_SUBPROGRAM_CHAIR);
    }
    button_1.setFont(new JDefaultFont().getFont(FONT_SIZE));
    return button_1;
  }

  /**
   * Creates the panel for the button.
   * 
   * @param the_button the button being passed in.
   * @return returns a panel.
   */
  private JPanel createPanel(final JButton the_button) {
    final JPanel panel = new JPanel(new BorderLayout());
    panel.add(the_button);
    panel.setBorder(new EmptyBorder(PADDING, PADDING, PADDING, PADDING));
    return panel;

  }

  /**
   * View button action listener.
   * 
   * @return returns the action listener.
   */
  private ActionListener viewActionListener() {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        try {
          Desktop.getDesktop().open((File) my_feedback.getPaper());
        } catch (final IOException e) {
        }

      }
    };
  }

  /**
   * Creates reviews for the program chair.
   * 
   * @return returns a list of reviews.
   */
  private JCustomComboBox<ReviewFormUI> createReviews() {
    final JCustomComboBox<ReviewFormUI> combobox = new JCustomComboBox<>();
    combobox.setVisible(my_current_status == Status.APPROVED_BY_PROGRAM_CHAIR);
    combobox.setFontSize(FONT_SIZE);

    for (ReviewFormUI rf : FeedbackDB.getAllReviews(my_feedback.getID(), my_conference)) {
      combobox.addItem(rf);

    }
    combobox.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        JDialogBox.createDialogBox((ReviewFormUI) combobox.getSelectedItem(), "Review");

      }
    });

    if (my_current_role == ConferenceRole.PROGRAM_CHAIR) {
      combobox.setVisible(
         my_current_status.getValue() >= Status.REVIEWED_BY_REVIEWER.getValue());
      my_review_button.setVisible(false);
    } else {
      my_review_button.setEnabled(
          my_current_status.getValue() <= Status.REJECTED_BY_SUBPROGRAM_CHAIR.getValue() ||
          my_current_status.getValue() >= Status.REVIEWED_BY_REVIEWER.getValue());
      combobox.setVisible(false);
    }
    return combobox;

  }
}
