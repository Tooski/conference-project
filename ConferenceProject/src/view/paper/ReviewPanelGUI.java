
package view.paper;
import db.FeedbackUserDB;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import model.Conference;
import model.ConferenceRole;
import model.Feedback;
import model.Feedback.Status;
import model.User;
import swing.custom.JCustomButton;
import swing.custom.JCustomLabel;
import swing.custom.JCustomList;
import swing.custom.JDefaultFont;
import swing.custom.JDialogBox;
import view.ReviewFormUI;

/**
 * Review Panel.
 * @author Josef Nosov
 * @version 06/06/2013
 *
 */
public class ReviewPanelGUI extends AbstractSubpanelGUI {

  /**
   * The view review.
   */
  private static final String VIEW_REVIEW = "View Review";
  
  /**
   * The review form.
   */
  private static final String REVIEW_FORM = "Review Form";
  /**
   * The height.
   */
  private static final int HEIGHT = 100;
  
  /**
   * The width.
   */
  private static final int WIDTH = 500;
  /**
   * The font size.
   */
  private static final int FONT_SIZE = 16;
  /**
   * Padding.
   */
  private static final int PADDING = 10;
  /**
   * The view review.
   */
  private final JButton my_view_review;
  /**
   * creates review.
   */
  private final JButton my_create_review;
  /**
   * the paper.
   */
  private final JButton my_view_paper;
  /**
   * my paper.
   */
  private final PaperGUI my_paper; 
  /**
   * my conference.
   */
  private final Conference my_conference;
  /**
   * my feedback.
   */
  private final Feedback my_feedback;
  /**
   * my user.
   */
  private final User my_user;
  /**
   * my role.
   */
  private final ConferenceRole my_role;
  /**
   * My status.
   */
  private final Status my_status;
  /**
   * The review form.
   */
  private final ReviewFormUI my_review_form;

  /**
   * The paper review.
   * @param the_paper the paper.
   * @param the_conference the conference.
   * @param the_feedback the feedback.
   * @param the_user the user.
   * @param the_role the role.
   */
  public ReviewPanelGUI(final PaperGUI the_paper, final Conference the_conference, 
                        final Feedback the_feedback, final User the_user,
                        final ConferenceRole the_role) {
    my_panel = new JPanel(new BorderLayout());
    my_paper = the_paper;
    my_conference = the_conference;
    my_user = the_user;
    my_role = the_role;
    my_feedback = the_feedback;
    my_view_review = createButton(VIEW_REVIEW, viewListener());
    my_create_review = createButton("Create Review", createListener());
    my_view_paper = createButton("View", viewPaperListener());
    my_review_form = new ReviewFormUI(my_panel);
    my_status =  FeedbackUserDB.getStatus(the_feedback.getID(), the_user.getID(), 
       the_conference.getID(), the_role.getRole());
    getList();
    init();
  }
  
  /**
   * Init.
   */
  private void init() {
    my_panel.setBorder(BorderFactory.createCompoundBorder(
       BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY), 
       new EmptyBorder(PADDING, PADDING, PADDING, PADDING)));
    my_view_review.setEnabled(FeedbackUserDB.getScores(my_feedback.getID(), 
       my_user.getID(), my_conference.getID(), my_role.getRole()) != null);

    my_create_review.setEnabled(FeedbackUserDB.getStatus(my_feedback.getID(), 
       my_user.getID(), my_conference.getID(), my_role.getRole()) == 
       Status.ASSIGNED_TO_REVIEWER && checkDate(my_conference.getReviewerDeadline()));
    final JPanel right_panel = new JPanel(new GridBagLayout());
    final GridBagConstraints gbc = new GridBagConstraints();
    gbc.gridy = 0;
    right_panel.add(getPanel(my_create_review), gbc);
    right_panel.add(getPanel(my_view_paper), gbc);
    right_panel.add(getPanel(my_view_review), gbc);
    
    final JPanel left_panel = new JPanel();
    left_panel.setLayout(new BoxLayout(left_panel, BoxLayout.Y_AXIS));
    left_panel.add(new JCustomLabel("Author: " + my_feedback.getAuthor()));
    left_panel.add(new JCustomLabel("Title: " + my_feedback.getTitle()));
    left_panel.add(new JCustomLabel("Review by: " + my_conference.getReviewerDeadline()));
    left_panel.add(new JCustomLabel("Assigned to: " + my_user + " (" + my_role + ")"));
    final JLabel status = new JCustomLabel("Status: " + my_status);

    left_panel.add(status);
    
    final JPanel paper_info = new JPanel(new BorderLayout());
    final JTextPane jtp = new JTextPane();
    jtp.setPreferredSize(new Dimension(WIDTH, HEIGHT));
    jtp.setText("Abstract: " + my_feedback.getPaperAbstract());
    jtp.setEditable(false);
    jtp.setFont(new JDefaultFont().getFont(FONT_SIZE));
    paper_info.add(left_panel, BorderLayout.WEST);
    paper_info.add(right_panel, BorderLayout.EAST);
    my_panel.add(paper_info);
    my_panel.add(new JScrollPane(jtp), BorderLayout.SOUTH);
  }
  
  /**
   * Creates list.
   */
  private void getList() {
    final JCustomList<Object> object = new JCustomList<>();
    object.setVisible(false);
    object.addElement(my_feedback);
    object.addElement(my_user);
    object.addElement(my_role);
    object.addElement(my_review_form);
    my_panel.add(object);
  }
  
  

  /**
   * Creates button.
   * @param the_text the texdt.
   * @param the_al the action listener.
   * @return the button.
   */
  private JButton createButton(final String the_text, final ActionListener the_al) {

    final JButton button = new JCustomButton(the_text);
    button.setFont(new JDefaultFont().getFont(FONT_SIZE));
    button.addActionListener(the_al);
    return button;

  }

  /**
   * The view listener.
   * @return action listener.
   */
  private ActionListener viewListener() {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        JDialogBox.createDialogBox(new ReviewFormUI(
          FeedbackUserDB.getReview(my_feedback.getID(), my_user.getID(), my_conference.getID(),
            my_role.getRole()), FeedbackUserDB.getScores(my_feedback.getID(), my_user.getID(), 
              my_conference.getID(), my_role.getRole())), REVIEW_FORM);
      }
    };
  }
  
  /**
   * The view paper.
   * @return returns the action listener.
   */
  private ActionListener viewPaperListener() {
    return new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        try {
          Desktop.getDesktop().open((File) my_feedback.getPaper());
        } catch (final IOException e) {
          e.printStackTrace();
        }
      }
    };
  }
  
  /**
   * The view listener.
   * @return action listener.
   */
  private ActionListener createListener() {
    return new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        my_review_form.addObserver(my_paper);
        JDialogBox.createDialogBox(my_review_form, REVIEW_FORM);
        my_view_review.setEnabled(FeedbackUserDB.getScores(my_feedback.getID(), 
           my_user.getID(), my_conference.getID(), my_role.getRole()) != null);
      }
    };
  }

  /**
   * adds button to the panel.
   * 
   * @param the_button adds the button to the panel.
   * @return returns the panel.
   */
  private JPanel getPanel(final JButton the_button) {
    final JPanel view_r = new JPanel(new BorderLayout());
    view_r.add(the_button);
    view_r.setBorder(new EmptyBorder(PADDING, PADDING, PADDING, PADDING));
    return view_r;
  }

}
