
package view.paper;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import model.Conference;
import model.ConferenceRole;
import model.Feedback;
import model.User;
import model.Feedback.Status;

import db.ConferenceUserDB;
import db.FeedbackDB;
import db.FeedbackUserDB;
import db.UserDB;
import db.UserRoleFeedback;

import swing.custom.JCustomButton;

import swing.custom.JCustomList;
import swing.custom.JCustomTable;
import swing.custom.JDefaultFont;
import view.GUI;
import view.ReviewFormUI;

/**
 * 
 * @author Josef Nosov
 * 
 */
public class PaperGUI extends Observable implements GUI, Observer {

  private JPanel jp;
  private Conference my_conference;
  private User my_user;
  private ConferenceRole my_current_role;
  private JScrollPane scroll;
  private final JPanel center_panel;

  public PaperGUI(Conference conference, User user) {

    addObserver(this);

    jp = new JPanel(new BorderLayout());
    my_conference = conference;
    my_user = user;
    my_current_role = user.getCurrentRole();
    JPanel center_parent = new JPanel(new GridBagLayout());
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.anchor = GridBagConstraints.NORTH;
    gbc.weighty = 1;
    gbc.weightx = 1;
    gbc.fill = GridBagConstraints.HORIZONTAL;
    center_panel = new JPanel();
    center_parent.add(center_panel, gbc);

    scroll = new JScrollPane(center_parent);
    createCenter(center_panel);

    if (my_current_role == ConferenceRole.SUBPROGRAM_CHAIR ||
        my_current_role == ConferenceRole.PROGRAM_CHAIR) {
      chairAssignPanel();
    } else {
      // userPanel();
      jp.add(scroll);
    }

  }

  JCustomTable<Feedback> feedbacks;
  JScrollPane scroll1;

  private void chairAssignPanel() {

    JPanel assign_papers = new JPanel(new GridBagLayout());
    JLabel assign_paper = new JLabel("Assign Papers", JLabel.CENTER);
    assign_paper.setFont(new JDefaultFont().getFont(12));
    List<Feedback> list = null;
    if (my_current_role == ConferenceRole.PROGRAM_CHAIR)
      list = FeedbackDB.getFeedbackList(my_conference.getID());
    else if (my_current_role == ConferenceRole.SUBPROGRAM_CHAIR) {
      list =
          FeedbackUserDB.getFeedbackByIDAndRole(my_conference.getID(), my_user.getID(),
                                                my_current_role.getRole());
    }
    int value = 0;
    if (list != null)
      value = list.size();
    Object[][] title_author = new Object[value][2];

    for (int i = 0; i < title_author.length; i++) {
      title_author[i][0] = list.get(i);
      title_author[i][1] = list.get(i).getAuthor();
    }
    feedbacks = new JCustomTable<>(new String[] {"Paper Title", "Author Name"}, title_author);
    scroll1 = new JScrollPane(feedbacks);
    scroll1.setPreferredSize(new Dimension(200, 100));

    Map<User, List<ConferenceRole>> users =
        ConferenceUserDB.getParticipants(my_conference.getID(), my_user.getID(),
                                         my_current_role.getRole());
    int value1 = 0;

    if (users != null)
      for (List<ConferenceRole> cr : users.values()) {
        for (@SuppressWarnings("unused")
        ConferenceRole r : cr)
          value1++;
      }

    Object[][] user_list = new Object[value1][3];

    int i = 0;
    for (User u : users.keySet()) {
      for (ConferenceRole role : users.get(u)) {
        user_list[i][0] = u;
        user_list[i][1] = role;
        user_list[i][2] =
            FeedbackUserDB.getFeedbackSize(my_conference.getID(), u.getID(), role.getRole(),
                                           my_user.getID());
        i++;
      }

    }

    JCustomTable<User> roles =
        new JCustomTable<>(new String[] {"User Name", "Role", "Assigned Papers"}, user_list);
    JScrollPane scroll2 = new JScrollPane(roles);
    scroll2.setPreferredSize(new Dimension(300, 100));

    JCustomButton assign_button = new JCustomButton("Assign Paper");
    assign_button.setEnabled(false);
    assign_button.addActionListener(buttonClick(assign_button, feedbacks, roles));
    feedbacks.getSelectionModel().addListSelectionListener(listener(assign_button, feedbacks,
                                                                    roles));
    roles.getSelectionModel().addListSelectionListener(listener(assign_button, feedbacks,
                                                                roles));

    JPanel users_panel = new JPanel(new BorderLayout());
    users_panel.add(scroll2, BorderLayout.CENTER);
    users_panel.add(assign_button, BorderLayout.SOUTH);

    JPanel assignment_panel = new JPanel(new BorderLayout());
    assignment_panel.add(scroll1, BorderLayout.WEST);
    assignment_panel.add(users_panel, BorderLayout.EAST);
    JPanel centered_panel = new JPanel(new BorderLayout());
    centered_panel.add(assign_paper, BorderLayout.NORTH);
    centered_panel.add(assignment_panel, BorderLayout.CENTER);
    assign_papers.add(centered_panel, new GridBagConstraints());
    JScrollPane scroll3 = new JScrollPane(assign_papers);
    scroll3.setPreferredSize(new Dimension(200, 150));
    scroll.setPreferredSize(new Dimension(200, 200));
    JSplitPane jsp = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scroll3, scroll);

    jsp.setDividerSize(3);
    jsp.setContinuousLayout(true);

    jp.add(jsp);

  }

  @Override
  public JPanel display() {
    return jp;
  }

  private void createCenter(final JPanel center_panel) {
    center_panel.setLayout(new BoxLayout(center_panel, BoxLayout.Y_AXIS));

    if (my_current_role == ConferenceRole.PROGRAM_CHAIR ||
        my_current_role == ConferenceRole.SUBPROGRAM_CHAIR) {

      for (UserRoleFeedback f : FeedbackUserDB.getFeedbackFromUser(my_conference.getID(),
                                                                   my_user.getID(), my_user
                                                                       .getCurrentRole()
                                                                       .getRole())) {

        center_panel.add(new ChairPanelGUI(my_user, my_conference, FeedbackDB.getFeedback(f
            .getFeedback()), UserDB.getUser(f.getID()), ConferenceRole.translate(f.getRole()),
                                           my_current_role).display());
      }
    } else if (my_current_role == ConferenceRole.REVIEWER) {

      for (UserRoleFeedback f : FeedbackUserDB
          .getFeedbackFromUserWithoutAssigned(my_conference.getID(), my_user.getID(), my_user
              .getCurrentRole().getRole())) {
        ReviewPanelGUI r =
            new ReviewPanelGUI(this, my_conference, FeedbackDB.getFeedback(f.getFeedback()),
                               UserDB.getUser(f.getID()),
                               ConferenceRole.translate(f.getRole()));
        r.addObserver(this);
        center_panel.add(r.display());
      }
    } else if (my_current_role == ConferenceRole.USER ||
               my_current_role == ConferenceRole.AUTHOR) {
      UserPanelGUI user = new UserPanelGUI(my_conference, my_user);
      user.addObserver(this);
      center_panel.add(user.display());

      for (UserRoleFeedback f : FeedbackUserDB
          .getFeedbackFromUserWithoutAssigned(my_conference.getID(), my_user.getID(),
                                              ConferenceRole.AUTHOR.getRole())) {



        AuthorPanelGUI apg =
            new AuthorPanelGUI(my_conference, FeedbackDB.getFeedback(f.getFeedback()),
                               my_user, ConferenceRole.translate(f.getRole()));
        apg.addObserver(this);
        center_panel.add(apg.display());

      }
    }

  }

  private ListSelectionListener listener(final JCustomButton button,
                                         final JCustomTable<Feedback> feedback,
                                         final JCustomTable<User> my_users) {

    return new ListSelectionListener() {

      @Override
      public void valueChanged(ListSelectionEvent arg0) {
        if (my_users.getSelectedRow() != -1 && feedback.getSelectedRow() != -1) {

          User user = ((User) my_users.getValueAt(my_users.getSelectedRow(), 0));
          ConferenceRole role =
              ((ConferenceRole) my_users.getValueAt(my_users.getSelectedRow(), 1));
          Feedback fb = ((Feedback) feedback.getValueAt(feedback.getSelectedRow(), 0));
          button.setEnabled((int) my_users.getValueAt(my_users.getSelectedRow(), 2) < 4 &&
                            user.getID() != fb.getAuthorID() &&
                            !FeedbackUserDB.getFeedbackFromUser(my_conference.getID(),
                                                                user.getID(), role.getRole(),
                                                                my_user.getID()).contains(fb));

        } else {
          button.setEnabled(false);
        }
      }

    };

  }

  private ActionListener buttonClick(final JCustomButton button,
                                     final JCustomTable<Feedback> feedback,
                                     final JCustomTable<User> my_users) {
    return new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        if (my_users.getSelectedRow() != -1 && feedback.getSelectedRow() != -1) {
          Feedback fb =
              (Feedback) ((DefaultTableModel) feedback.getModel()).getValueAt(feedback
                  .getSelectedRow(), 0);
          User users =
              (User) ((DefaultTableModel) my_users.getModel()).getValueAt(my_users
                  .getSelectedRow(), 0);
          ConferenceRole role =
              (ConferenceRole) ((DefaultTableModel) my_users.getModel()).getValueAt(my_users
                  .getSelectedRow(), 1);
        FeedbackUserDB.assignFeedback(my_conference.getID(), fb.getID(), my_user.getID(), my_current_role.getRole(), users.getID(), role.getRole());

          ((DefaultTableModel) my_users.getModel()).setValueAt(FeedbackUserDB
              .getFeedbackSize(my_conference.getID(), users.getID(), role.getRole(),
                               my_user.getID()), my_users.getSelectedRow(), 2);

          button.setEnabled((int) my_users.getValueAt(my_users.getSelectedRow(), 2) < 4 &&
                            users.getID() != fb.getAuthorID() &&
                            !FeedbackUserDB.getFeedbackFromUser(my_conference.getID(),
                                                                users.getID(), role.getRole(),
                                                                my_user.getID()).contains(fb));
          center_panel.add(new ChairPanelGUI(my_user, my_conference, fb, users, role,
                                             my_current_role).display());

          center_panel.validate();
          center_panel.repaint();
          scroll.validate();
          scroll.repaint();

        } else {
          button.setEnabled(false);
        }
      }

    };
  }

  @SuppressWarnings("unchecked")
  @Override
  public void update(Observable o, Object arg) {

    if (o instanceof ReviewFormUI) {
      Feedback f =
          (Feedback) ((DefaultListModel<Object>) ((JCustomList<Object>) ((JPanel) arg)
              .getComponent(0)).getModel()).getElementAt(0);
      User user =
          (User) ((DefaultListModel<Object>) ((JCustomList<Object>) ((JPanel) arg)
              .getComponent(0)).getModel()).getElementAt(1);
      ConferenceRole role =
          (ConferenceRole) ((DefaultListModel<Object>) ((JCustomList<Object>) ((JPanel) arg)
              .getComponent(0)).getModel()).getElementAt(2);
      ReviewFormUI rfu =
          (ReviewFormUI) ((DefaultListModel<Object>) ((JCustomList<Object>) ((JPanel) arg)
              .getComponent(0)).getModel()).getElementAt(3);

      Status s = null;
      if (role == ConferenceRole.REVIEWER) {
        ((JButton) ((JPanel) ((JPanel) ((JPanel) ((JPanel) arg).getComponent(1))
            .getComponent(1)).getComponent(0)).getComponent(0)).setEnabled(false);

        s = Status.REVIEWED_BY_REVIEWER;
      } else if (role == ConferenceRole.SUBPROGRAM_CHAIR) {
        ((JButton) ((JPanel) ((JPanel) ((JPanel) ((JPanel) arg).getComponent(1))
            .getComponent(1)).getComponent(0)).getComponent(0)).setEnabled(false);
        ((JButton) ((JPanel) ((JPanel) ((JPanel) ((JPanel) arg).getComponent(1))
            .getComponent(1)).getComponent(0)).getComponent(1)).setEnabled(false);
        s = Status.RECOMMENDED_BY_SUBPROGRAM_CHAIR;
      }

      FeedbackUserDB.setReview(f.getID(), user.getID(), rfu.getText(), rfu.getScores(),
                               my_conference.getID(), role.getRole());
      FeedbackUserDB.setReview(f.getID(), my_conference.getProgramChair().getID(),
                               rfu.getText(), rfu.getScores(), my_conference.getID(),
                               ConferenceRole.PROGRAM_CHAIR.getRole());

      FeedbackUserDB.setStatus(f.getID(), user.getID(), s, my_conference.getID(),
                               role.getRole());
      FeedbackUserDB.setStatus(f.getID(), my_conference.getProgramChair().getID(), s,
                               my_conference.getID(), ConferenceRole.PROGRAM_CHAIR.getRole());
      ((JLabel) ((JPanel) ((JPanel) ((JPanel) arg).getComponent(1)).getComponent(0))
          .getComponent(2)).setText("Status: " + s);

    } else if (o instanceof UserPanelGUI) {
      AuthorPanelGUI apg =
          new AuthorPanelGUI(my_conference, FeedbackDB.getFeedback((int) arg), my_user,
                             ConferenceRole.AUTHOR);
      apg.addObserver(this);
      center_panel.add(apg.display());

      center_panel.validate();
      center_panel.repaint();
      scroll.validate();
      scroll.repaint();
      setChanged();
      notifyObservers();
    } else if (o instanceof AuthorPanelGUI) {

      center_panel.remove((JPanel) arg);
      center_panel.validate();
      center_panel.repaint();
      scroll.validate();
      scroll.repaint();
      setChanged();
      notifyObservers();
    } else if (o instanceof ReviewPanelGUI) {
      setChanged();
      notifyObservers();
    }
  }

}
