
package view.paper;

import db.FeedbackDB;
import db.FeedbackUserDB;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import model.Conference;
import model.ConferenceRole;
import model.Feedback.Status;
import model.User;
import swing.custom.JCustomButton;
import swing.custom.JCustomLabel;
import swing.custom.JDefaultFont;
import swing.custom.JDialogBox;
import view.GUI;

/**
 * The user panel class allows user to submit papers.
 * @author Josef Nosov
 * @version 06/06/2013
 * 
 */
public class UserPanelGUI extends AbstractSubpanelGUI implements GUI {

  /**
   * The abstract pane's height.
   */
  private static final int HEIGHT = 100;
  /**
   * The abstract pane's width.
   */
  private static final int WIDTH = 500;
  /**
   * For the submit paper button.
   */
  private static final String SUBMIT_PAPER = "Submit Paper";
  /**
   * Default font size.
   */
  private static final int FONT_SIZE = 12;
  /**
   * Padding for the borders.
   */
  private static final int PADDING = 10;
  /**
   * The current user for this conference.
   */
  private User my_user;
  /**
   * The current conference for this session.
   */
  private Conference my_conference;

  /**
   * The text field to input author's name.
   */
  private JTextField my_author_name;

  /**
   * The text field to input the paper title.
   */
  private JTextField my_paper_title;

  /**
   * The pane to allow user to input the abstract pane.
   */
  private JTextPane my_abstract_pane;

  /**
   * The submit button to submit a paper.
   */
  private JButton my_submit_button;

  /**
   * The default confernece.
   * 
   * @param the_conference the conference the user currently is on.
   * @param the_user the current selected use.
   */
  public UserPanelGUI(final Conference the_conference, final User the_user) {
    my_panel = new JPanel(new BorderLayout());
    my_user = the_user;
    my_conference = the_conference;
    my_panel.setBorder(BorderFactory.createCompoundBorder(
         BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY), 
         new EmptyBorder(PADDING, PADDING, PADDING, PADDING)));

    my_panel.add(createPaperFields(), BorderLayout.NORTH);
    my_panel.add(new JScrollPane(my_abstract_pane), BorderLayout.CENTER);
    my_panel.add(createSubmitButton(), BorderLayout.SOUTH);

  }

  /**
   * Creates the paper text field.
   * @param the_text the name in the text
   * @return returns a new JTextField.
   */
  private JTextField paperTextField(final String the_text) {
    final JTextField text_field = new JTextField(15);
    text_field.setFont(new JDefaultFont().getFont(FONT_SIZE));
    text_field.setText(the_text);
    return text_field;

  }

  /**
   * Creates a paper field.
   * @return returns the paper panel.
   */
  private JPanel createPaperFields() {
    final JPanel paper_panel = new JPanel(new BorderLayout());
    final JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    panel.add(createPaperPanel());
    paper_panel.add(panel, BorderLayout.WEST);
    return paper_panel;
  }

  /**
   * Creates a submit paper button.
   * @return returns the submit paper button.
   */
  private JButton createSubmitButton() {
    my_submit_button = new JCustomButton(SUBMIT_PAPER);
    my_submit_button.setEnabled(checkDate(my_conference.getAuthorDeadline()) &&
                                !my_abstract_pane.getText().isEmpty() &&
                                !my_author_name.getText().isEmpty() &&
                                !my_submit_button.getText().isEmpty());
    my_submit_button.addActionListener(submitPaperButton());
    return my_submit_button;

  }

  /**
   * Creates a paper panel.
   * @return returns a new panel.
   */
  private JPanel createPaperPanel() {
    my_abstract_pane = new JTextPane();
    my_abstract_pane.setPreferredSize(new Dimension(WIDTH, HEIGHT));
    my_abstract_pane.setFont(new JDefaultFont().getFont(FONT_SIZE));
    my_author_name = paperTextField(my_user.getActualName());
    my_paper_title = paperTextField("");
    my_abstract_pane.addKeyListener(textFiles());
    my_paper_title.addKeyListener(textFiles());
    my_author_name.addKeyListener(textFiles());
    final JPanel author_panel = new JPanel(new GridLayout(4, 2));
    author_panel.add(new JCustomLabel("Author: "));
    author_panel.add(my_author_name);
    author_panel.add(new JCustomLabel("Title: "));
    author_panel.add(my_paper_title);
    author_panel.add(new JCustomLabel("Submission by: " + my_conference.getAuthorDeadline()));
    author_panel.add(blankPanel());
    author_panel.add(new JCustomLabel("Abstract:"));
    return author_panel;
  }

  /**
   * returns a blank panel for padding the box.
   * @return creates a blank panel.
   */
  private JPanel blankPanel() {
    return new JPanel();
  }

  /**
   * The text file listener.
   * @return returns the key listener.
   */
  private KeyListener textFiles() {
    return new KeyAdapter() {

      @Override
      public void keyReleased(final KeyEvent the_arg) {
        my_submit_button.setEnabled(!my_abstract_pane.getText().isEmpty() &&
                                    !my_author_name.getText().isEmpty() &&
                                    !my_paper_title.getText().isEmpty());
      }
    };

  }

  /**
   * An action listener for the file chooser button.
   * @return returns a new action listener.
   */
  private ActionListener submitPaperButton() {
    return new ActionListener() {
      public void actionPerformed(final ActionEvent the_event) {
        final JFileChooser file_chooser = new JFileChooser(SUBMIT_PAPER);
        final int submission_value = file_chooser.showDialog(null, "Submit");
        if (submission_value == JFileChooser.APPROVE_OPTION) {
          final int v =
              FeedbackDB.createFeedback(my_conference.getID(), my_paper_title.getText(),
                                        my_author_name.getText(), my_abstract_pane.getText(),
                                        my_user.getID(), file_chooser.getSelectedFile());
          my_paper_title.setText("");
          my_author_name.setText(my_user.getActualName());
          my_abstract_pane.setText("");
          JDialogBox.createDialogBox("Successfully submitted paper", "");
          FeedbackUserDB.setStatus(v, my_user.getID(), Status.PENDING, my_conference.getID(),
                                   ConferenceRole.AUTHOR.getRole());
          setChanged();
          notifyObservers(v);
        }
      }
    };

  }

}
