
package view;
import db.UserDB;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JPanel;
import model.Admin;
import swing.custom.JCustomButton;


/**
 * Conference database UI class.
 * 
 * @author Josef Nosov
 * @version 5/31/13
 * 
 */
@SuppressWarnings("serial")
public class ConferenceDBUI extends JPanel implements Observer, GUI {

  
  /**
   * The new conference.
   */
  private JButton my_newcon;
  /**
   * The conference.
   */
  private ConferenceDBUI my_conference;
  
  /**
   * my id.
   */
  private int my_id;
  
  /**
   * The bottom panel.
   */
  private final JPanel[] my_bottom_panel = {new JPanel()};
  
  /**
   * The master panel.
   */
  private final JPanel my_master_panel = new JPanel(new BorderLayout());

  /**
   * ConferenceDBUI constructor.
   * 
   * @param the_class the overlay UI
   * @param the_id the ID
   */
  public ConferenceDBUI(final OverlayUI the_class, final int the_id) {
    super(new BorderLayout());
    my_newcon = new JCustomButton("Create new conference");
    my_id = the_id;
    setComponents(the_class);
    my_conference = this;

  }



  /**
   * Set up the components for the GUI.
   * 
   * @param the_class the overlay UI
   */
  private void setComponents(final OverlayUI the_class) {
    final JPanel button_panel = new JPanel();
    button_panel.add(my_newcon);
    my_master_panel.add(button_panel, BorderLayout.NORTH);
    my_bottom_panel[0].setPreferredSize(new Dimension(1000, 1000));
    my_newcon.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent the_event) {
        my_newcon.setEnabled(false);
        final CreateConUI conf = new CreateConUI((Admin) UserDB.getUser(my_id));
        conf.addObserver(the_class);
        conf.addObserver(my_conference);

        my_bottom_panel[0] = conf.display();
        my_master_panel.add(my_bottom_panel[0], BorderLayout.CENTER);
        my_master_panel.validate();
        my_master_panel.repaint();
      }

    });

    add(my_master_panel);

  }


  /**
   * Displays the GUI of this class.
   */
  public JPanel display() {
    return this;
  }

  @Override
  public void update(final Observable the_o, final Object the_arg) {
    my_master_panel.remove(my_bottom_panel[0]);
    my_master_panel.validate();
    my_master_panel.repaint();
    my_newcon.setEnabled(true);
  }

}
