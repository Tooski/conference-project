

package view;

import java.awt.BorderLayout;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;

import javax.swing.JPanel;
import javax.swing.JScrollPane; 
import swing.custom.JCustomButton;
import swing.custom.JCustomLabel;
import swing.custom.JCustomRadioButton;
import swing.custom.JDefaultFont;

/**
 * ReviewFormUI 
 * @author Josh Keaton and Josef Nosov
 */
@SuppressWarnings("serial")
public class ReviewFormUI extends Observable implements GUI, Serializable {

  private JPanel main;
  private ArrayList<JCustomRadioButton> my_question_1;
  private ArrayList<JCustomRadioButton> my_question_2;
  private ArrayList<JCustomRadioButton> my_question_3;
  private ArrayList<JCustomRadioButton> my_question_4;
  private ArrayList<JCustomRadioButton> my_question_5;
  private ArrayList<JCustomRadioButton> my_question_6;
  private ArrayList<JCustomRadioButton> my_question_7;
  private ArrayList<JCustomRadioButton> my_question_8;
  private ArrayList<JCustomRadioButton> my_question_9;
  private ArrayList<JCustomRadioButton> my_question_10;
  private JButton submit_review;
  private JPanel edit_review;
  private String my_review;
  private int[] my_values;
  private JPanel master_panel;


  /**
   * @author Josh Keaton
   * @param panel
   */
  public ReviewFormUI(JPanel panel) {
    main = new JPanel();
    edit_review = panel;
    main.setLayout(new BorderLayout());
    my_question_1 = new ArrayList<JCustomRadioButton>();
    my_question_2 = new ArrayList<JCustomRadioButton>();
    my_question_3 = new ArrayList<JCustomRadioButton>();
    my_question_4 = new ArrayList<JCustomRadioButton>();
    my_question_5 = new ArrayList<JCustomRadioButton>();
    my_question_6 = new ArrayList<JCustomRadioButton>();
    my_question_7 = new ArrayList<JCustomRadioButton>();
    my_question_8 = new ArrayList<JCustomRadioButton>();
    my_question_9 = new ArrayList<JCustomRadioButton>();
    my_question_10 = new ArrayList<JCustomRadioButton>();
    submit_review = new JCustomButton("Submit");
    submit_review.addActionListener(new SubmitListener());
    setupComponents();
  }


  /**
   * @author Josh Keaton
   * @param review
   * @param values
   */
  public ReviewFormUI(String review, int[] values) {
    main = new JPanel();

    my_review = review;
    my_values = values;
    main.setLayout(new BorderLayout());
    my_question_1 = new ArrayList<JCustomRadioButton>();
    my_question_2 = new ArrayList<JCustomRadioButton>();
    my_question_3 = new ArrayList<JCustomRadioButton>();
    my_question_4 = new ArrayList<JCustomRadioButton>();
    my_question_5 = new ArrayList<JCustomRadioButton>();
    my_question_6 = new ArrayList<JCustomRadioButton>();
    my_question_7 = new ArrayList<JCustomRadioButton>();
    my_question_8 = new ArrayList<JCustomRadioButton>();
    my_question_9 = new ArrayList<JCustomRadioButton>();
    my_question_10 = new ArrayList<JCustomRadioButton>();
    submit_review = new JCustomButton("Submit");
    submit_review.addActionListener(new SubmitListener());
    submit_review.setVisible(false);
    setupComponents();

  }


  /**
   * @author Josh Keaton
   */
  private void setupComponents() {
    master_panel = new JPanel();
    final JPanel review_panel = new JPanel();
    final JPanel buffer_panel = new JPanel();
    final JPanel button_panel = new JPanel();
    final JPanel text_panel = new JPanel();
    final JCustomLabel summary_label = new JCustomLabel("Summary Comments:");
    summary_label.setFont(new Font(summary_label.getFont().getFontName(), Font.PLAIN, 18));
    review_panel.setLayout(new BoxLayout(review_panel, BoxLayout.PAGE_AXIS));
    text_panel.setLayout(new BorderLayout());
    master_panel.setLayout(new BorderLayout());
    button_panel.setLayout(new FlowLayout());
    final JPanel question_panel = new JPanel(new GridLayout(11, 0));
    question_panel
        .add(QuestionGroup(1, "5", "4", "3", "2", "1",
                           "1. Can the content be directly applied by classroom instructors or curriculum designers?"));
    question_panel.add(buffer_panel);
    question_panel
        .add(QuestionGroup(2,
                           "5",
                           "4",
                           "3",
                           "2",
                           "1",
                           "<html>2. Does the work appeal to a broad readership interested in engineering education or is<br>it narrowly specialized?</html>"));
    question_panel.add(buffer_panel);
    question_panel.add(QuestionGroup(3, "5", "4", "3", "2", "1",
                                     "3. Does the work address a significant problem?"));
    question_panel.add(buffer_panel);
    question_panel
        .add(QuestionGroup(4, "5", "4", "3", "2", "1",
                           "4. Does the author build upon relevant references and bodies of knowledge"));
    question_panel.add(buffer_panel);
    question_panel
        .add(QuestionGroup(5,
                           "5",
                           "4",
                           "3",
                           "2",
                           "1",
                           "<html>5. If a teaching intervention is reported, is it adequately evaluated in terms of its impact<br>on learning in actual use?</html>"));
    question_panel.add(buffer_panel);
    question_panel
        .add(QuestionGroup(6,
                           "5",
                           "4",
                           "3",
                           "2",
                           "1",
                           "<html>6. Does the author use methods appropriate to the goals, both for the instructional<br>intervention and the evaluation of impact on learning?</html>"));
    question_panel.add(buffer_panel);
    question_panel
        .add(QuestionGroup(7, "5", "4", "3", "2", "1",
                           "7. Did the author provide sufficient detail to replicate and evaluate?"));
    question_panel.add(buffer_panel);
    question_panel.add(QuestionGroup(8, "5", "4", "3", "2", "1",
                                     "8. Is the paper clearly and carefully written?"));
    question_panel.add(buffer_panel);
    question_panel
        .add(QuestionGroup(9, "5", "4", "3", "2", "1",
                           "9. Does the paper adhere to accepted standards of style, usage, and composition?"));
    question_panel.add(QuestionGroup(10, "5", "4", "3", "2", "1", "Summary Rating:"));
    text_panel.add(summary_label, BorderLayout.NORTH);
    t = new TextArea("", 5, 25, TextArea.SCROLLBARS_VERTICAL_ONLY);
    if (my_review != null) {
      t.setText(my_review);
    }
    text_panel.add(t, BorderLayout.CENTER);
    text_panel.add(buffer_panel, BorderLayout.SOUTH);
    text_panel.add(buffer_panel, BorderLayout.EAST);
    text_panel.add(buffer_panel, BorderLayout.WEST);
    review_panel.add(Instructions());
    review_panel.add(buffer_panel);
    review_panel.add(question_panel);
    review_panel.add(text_panel);
    button_panel.add(submit_review);
    review_panel.add(button_panel);
    review_panel.add(buffer_panel);
    final JScrollPane scroll_pane = new JScrollPane(review_panel);
    master_panel.add(scroll_pane);
    // setPreferredSize(new Dimension(450, 110));
    t.setFont(new JDefaultFont().getFont(14));
    main.add(master_panel);
  }

  private TextArea t;

  /**
   * @author Josh Keaton and Josef Nosov
   * @param question_number
   * @param rate1
   * @param rate2
   * @param rate3
   * @param rate4
   * @param rate5
   * @param question
   * @return
   */
  public JPanel QuestionGroup(int question_number, String rate1, String rate2, String rate3,
                              String rate4, String rate5, String question) {
    JCustomRadioButton Q1_1 = new JCustomRadioButton(rate1);
    JCustomRadioButton Q1_2 = new JCustomRadioButton(rate2);
    JCustomRadioButton Q1_3 = new JCustomRadioButton(rate3);
    JCustomRadioButton Q1_4 = new JCustomRadioButton(rate4);
    JCustomRadioButton Q1_5 = new JCustomRadioButton(rate5);
    ButtonGroup bgroup = new ButtonGroup();
    if (my_values != null) {
      switch (my_values[question_number - 1]) {
        case 5:
          Q1_1.setSelected(true);
          break;
        case 4:
          Q1_2.setSelected(true);
          break;
        case 3:
          Q1_3.setSelected(true);
          break;
        case 2:
          Q1_4.setSelected(true);
          break;
        case 1:
          Q1_5.setSelected(true);
          break;
      }
    }

    switch (question_number) {
      case 1:
        my_question_1.add(Q1_1);
        my_question_1.add(Q1_2);
        my_question_1.add(Q1_3);
        my_question_1.add(Q1_4);
        my_question_1.add(Q1_5);
        break;
      case 2:
        my_question_2.add(Q1_1);
        my_question_2.add(Q1_2);
        my_question_2.add(Q1_3);
        my_question_2.add(Q1_4);
        my_question_2.add(Q1_5);
        break;
      case 3:
        my_question_3.add(Q1_1);
        my_question_3.add(Q1_2);
        my_question_3.add(Q1_3);
        my_question_3.add(Q1_4);
        my_question_3.add(Q1_5);
        break;
      case 4:
        my_question_4.add(Q1_1);
        my_question_4.add(Q1_2);
        my_question_4.add(Q1_3);
        my_question_4.add(Q1_4);
        my_question_4.add(Q1_5);
        break;
      case 5:
        my_question_5.add(Q1_1);
        my_question_5.add(Q1_2);
        my_question_5.add(Q1_3);
        my_question_5.add(Q1_4);
        my_question_5.add(Q1_5);
        break;
      case 6:
        my_question_6.add(Q1_1);
        my_question_6.add(Q1_2);
        my_question_6.add(Q1_3);
        my_question_6.add(Q1_4);
        my_question_6.add(Q1_5);
        break;
      case 7:
        my_question_7.add(Q1_1);
        my_question_7.add(Q1_2);
        my_question_7.add(Q1_3);
        my_question_7.add(Q1_4);
        my_question_7.add(Q1_5);
        break;
      case 8:
        my_question_8.add(Q1_1);
        my_question_8.add(Q1_2);
        my_question_8.add(Q1_3);
        my_question_8.add(Q1_4);
        my_question_8.add(Q1_5);
        break;
      case 9:
        my_question_9.add(Q1_1);
        my_question_9.add(Q1_2);
        my_question_9.add(Q1_3);
        my_question_9.add(Q1_4);
        my_question_9.add(Q1_5);
        break;
      case 10:
        my_question_10.add(Q1_1);
        my_question_10.add(Q1_2);
        my_question_10.add(Q1_3);
        my_question_10.add(Q1_4);
        my_question_10.add(Q1_5);
        break;
    }
    bgroup.add(Q1_1);
    bgroup.add(Q1_2);
    bgroup.add(Q1_3);
    bgroup.add(Q1_4);
    bgroup.add(Q1_5);
    JPanel radioPanel = new JPanel();
    radioPanel.setLayout(new GridLayout(1, 5));
    radioPanel.add(Q1_1);
    radioPanel.add(Q1_2);
    radioPanel.add(Q1_3);
    radioPanel.add(Q1_4);
    radioPanel.add(Q1_5);
    radioPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                                                          question));
    return radioPanel;
  }

  public JPanel Instructions() {
    JPanel InstructionPanel = new JPanel();
    InstructionPanel.setLayout(new BoxLayout(InstructionPanel, BoxLayout.PAGE_AXIS));
    final JCustomLabel label = new JCustomLabel("Instructions To Reviewers:");
    label.setFont(new Font(label.getFont().getFontName(), Font.PLAIN, 20));
    final JCustomLabel instructions1 =
        new JCustomLabel(
                         "Please provide a numeric rating on a 5-point scale for each question, along with a brief");
    final JCustomLabel instructions2 =
        new JCustomLabel(
                         "rationale for each numeric rating. In doing so, please discuss both the strengths and the");
    final JCustomLabel instructions3 =
        new JCustomLabel(
                         "weaknesses of each paper so that the editors and authors can understand your reasoning.");
    final JCustomLabel instructions4 =
        new JCustomLabel(
                         "Please phrase your reviews politely; even 'bad' papers represent a lot of work on the part of");
    final JCustomLabel instructions5 =
        new JCustomLabel(
                         "the authors. The review may be the basis for further revisions of the paper or the work that");
    final JCustomLabel instructions6 =
        new JCustomLabel(
                         "the paper reports. We all know how hurtful a needlessly negative review can be, and how");
    final JCustomLabel instructions7 =
        new JCustomLabel(
                         "helpful a positive one can be; please try to bear that in mind when you are writing yours.");
    InstructionPanel.add(label);
    InstructionPanel.add(instructions1);
    InstructionPanel.add(instructions2);
    InstructionPanel.add(instructions3);
    InstructionPanel.add(instructions4);
    InstructionPanel.add(instructions5);
    InstructionPanel.add(instructions6);
    InstructionPanel.add(instructions7);
    return InstructionPanel;

  }

  /**
   * Creates and displays the application frame.
   * 
   * @return
   */
  public JPanel display() {
    return main;
  }

  /**
   * @author Josh Keaton and Josef Nosov
   * An action listener for the login button.
   */
  private class SubmitListener implements ActionListener {
    public void actionPerformed(final ActionEvent the_event) {
      my_values = new int[10];
      for (JCustomRadioButton b : my_question_1) {
        if (b.isSelected()) {
          my_values[0] = Integer.parseInt(b.getText());
        }
      }
      for (JCustomRadioButton b : my_question_2) {
        if (b.isSelected()) {
          my_values[1] = Integer.parseInt(b.getText());
        }
      }
      for (JCustomRadioButton b : my_question_3) {
        if (b.isSelected()) {
          my_values[2] = Integer.parseInt(b.getText());
        }
      }
      for (JCustomRadioButton b : my_question_4) {
        if (b.isSelected()) {
          my_values[3] = Integer.parseInt(b.getText());
        }
      }
      for (JCustomRadioButton b : my_question_5) {
        if (b.isSelected()) {
          my_values[4] = Integer.parseInt(b.getText());
        }
      }
      for (JCustomRadioButton b : my_question_6) {
        if (b.isSelected()) {
          my_values[5] = Integer.parseInt(b.getText());
        }
      }
      for (JCustomRadioButton b : my_question_7) {
        if (b.isSelected()) {
          my_values[6] = Integer.parseInt(b.getText());
        }
      }
      for (JCustomRadioButton b : my_question_8) {
        if (b.isSelected()) {
          my_values[7] = Integer.parseInt(b.getText());
        }
      }
      for (JCustomRadioButton b : my_question_9) {
        if (b.isSelected()) {
          my_values[8] = Integer.parseInt(b.getText());
        }
      }
      for (JCustomRadioButton b : my_question_10) {
        if (b.isSelected()) {
          my_values[9] = Integer.parseInt(b.getText());
        }
      }
      setChanged();
      notifyObservers(edit_review);
    }
  }

  public JButton getSubmitButton() {
    return submit_review;
  }

  public String getText() {
    return t.getText();
  }

  public int[] getScores() {
    return my_values;
  }

  public String toString() {
    return "Review";

  }

}
