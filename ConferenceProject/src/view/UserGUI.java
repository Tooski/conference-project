
package view;

/**
 * @author Quan Le and Josef Nosov
 */
import db.ConferenceUserDB;
import db.UserDB;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.List;
import java.util.Observable;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import model.Conference;
import model.ConferenceRole;
import model.User;
import swing.custom.JCustomButton;
import swing.custom.JCustomComboBox;
import swing.custom.JCustomLabel;
import swing.custom.JCustomList;

public class UserGUI extends Observable implements GUI {
  private ConferenceRole[] program_chair = {ConferenceRole.SUBPROGRAM_CHAIR,
      ConferenceRole.REVIEWER};

  /**
   * The rolebox.
   */
  private JComboBox<ConferenceRole> my_rolebox;



  /**
   * A button used to push an element on the Stack.
   */
  private final JCustomButton my_promote_button;

  /**
   * The list of users.
   */
  private Collection<User> my_users;

  /**
   * The user list.
   */
  private JCustomList<User> my_user_list;


  /**
   * The role list.
   */
  private JCustomList<ConferenceRole> my_role_list;

  /**
   * THe users.
   */
  private User my_user;
  
  /**
   * The current role.
   */
  private final ConferenceRole my_current_role;


  /**
   * The conference.
   */
  private Conference my_conference;
  
  /**
   * THe main panel.
   */
  private JPanel my_panel = new JPanel();


  /**
   * UserGUI constructor.
   * @author Josef Nosov
   */
  public UserGUI(final Conference the_conference, final User the_user) {
    super();

    my_user = the_user;
    my_current_role = my_user.getCurrentRole();
    my_conference = the_conference;
    my_rolebox = new JCustomComboBox<ConferenceRole>(program_chair);
    if (my_current_role == ConferenceRole.SUBPROGRAM_CHAIR) {
      my_promote_button = new JCustomButton("Promote to Reviewer");
    } else {
      my_promote_button = new JCustomButton("Promote");
    }
    my_promote_button.setEnabled(false);

    my_users = UserDB.getAllUsers();
    my_user_list = new JCustomList<>();

    my_user_list.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(final ListSelectionEvent the_arg) {

        if (my_user_list.getSelectedIndex() != -1) {
          ((DefaultListModel<ConferenceRole>) my_role_list.getModel()).removeAllElements();

          final List<ConferenceRole> roles =
              ConferenceUserDB.getRole(the_conference.getID(), my_user_list.getSelectedValue()
                  .getID());
          for (ConferenceRole r : roles) {
            ((DefaultListModel<ConferenceRole>) my_role_list.getModel()).addElement(r);
          }

          programChairCheck();
          subprogramChairCheck();
        } else {
          my_promote_button.setEnabled(false);
        }
      }

    });
    my_role_list = new JCustomList<>();
    setupComponents();

    ConferenceUserDB.getParticipants(my_conference.getID(), the_user.getID(),
                                     my_current_role.getRole());
    for (User u : my_users) {
      if (!UserDB.isAdmin(u.getID())) {
        ((DefaultListModel<User>) my_user_list.getModel()).addElement(u);
      }
    }

    my_rolebox.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent the_arg) {
        programChairCheck();
        subprogramChairCheck();
      }
    });
  }

  /**
   * Checks to see if user is program chair and promotes based on role.
   * @author Josef Nosov
   */
  private void programChairCheck() {
    if (my_current_role == ConferenceRole.PROGRAM_CHAIR) {
      if ((ConferenceRole) my_rolebox.getSelectedItem() == ConferenceRole.SUBPROGRAM_CHAIR) {
        my_promote_button.setEnabled(false);
        boolean containsReviewer = false;
        boolean containsSPC = false;
        for (int i = 0; i < my_role_list.getModel().getSize(); i++) {
          if (my_role_list.getModel().getElementAt(i) == ConferenceRole.REVIEWER) {
            containsReviewer = true;
          } else if (my_role_list.getModel().getElementAt(i) == ConferenceRole.SUBPROGRAM_CHAIR) {
            containsSPC = true;
          }
        }
        my_promote_button.setEnabled(containsReviewer && !containsSPC);
      } else {
        my_promote_button.setEnabled(true);
        for (int i = 0; i < my_role_list.getModel().getSize(); i++) {
          if (my_role_list.getModel().getElementAt(i) == ConferenceRole.REVIEWER) {
            my_promote_button.setEnabled(false);
          }
        }
      }
    }
  }

  /**
   * Checks to see if user is subprogram chair and promotes based on role.
   * @author Josef Nosov

   */
  private void subprogramChairCheck() {
    if (my_current_role == ConferenceRole.SUBPROGRAM_CHAIR) {
      my_promote_button.setEnabled(true);
      for (int i = 0; i < my_role_list.getModel().getSize(); i++) {
        if (my_role_list.getModel().getElementAt(i) == ConferenceRole.REVIEWER) {
          my_promote_button.setEnabled(false);

        }
      }
    }
  }

  /**
   * @author Quan Le
   * Helper method to perform the work of setting up the GUI components.
   */
  private void setupComponents() {
    my_promote_button.setMnemonic(KeyEvent.VK_P);
    my_promote_button.addActionListener(new PromoteListener());


    my_user_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    final JScrollPane scroll_panel1 = new JScrollPane(my_user_list);
    scroll_panel1.setPreferredSize(new Dimension(150, 300));

    final JScrollPane scroll_panel2 = new JScrollPane(my_role_list);
    scroll_panel2.setPreferredSize(new Dimension(150, 300));
    my_rolebox.setSelectedIndex(0);  

    final JPanel master_panel = new JPanel(new BorderLayout());
    final JPanel empty_panel = new JPanel();
    final JPanel button_panel = new JPanel(new FlowLayout());
    final JPanel top = new JPanel(new BorderLayout());

    final JCustomLabel usernames = new JCustomLabel("Usernames");
    final JCustomLabel roles = new JCustomLabel("Roles");
    top.add(usernames, BorderLayout.WEST);
    top.add(roles, BorderLayout.EAST);

    if (my_current_role == ConferenceRole.PROGRAM_CHAIR) {
      button_panel.add(my_rolebox);
    }
    button_panel.add(my_promote_button);

    master_panel.add(top, BorderLayout.NORTH);
    master_panel.add(scroll_panel1, BorderLayout.WEST);
    master_panel.add(empty_panel, BorderLayout.CENTER);
    master_panel.add(scroll_panel2, BorderLayout.EAST);
    master_panel.add(button_panel, BorderLayout.SOUTH);
    my_panel.add(master_panel);
  }


  @Override
  public JPanel display() {
    my_panel.setVisible(true);

    return my_panel;

  }

  /**
   * Promotes the user.
   * @author Josef Nosov
   *
   */
  private class PromoteListener implements ActionListener {

    @Override
    public void actionPerformed(final ActionEvent the_arg) {

      ConferenceRole selected_role = ConferenceRole.REVIEWER;
      if (my_current_role == ConferenceRole.PROGRAM_CHAIR) {
        selected_role = (ConferenceRole) my_rolebox.getSelectedItem();

      }

      my_user.promoteUser(my_conference, my_user_list.getSelectedValue(), selected_role);
      ((DefaultListModel<ConferenceRole>) my_role_list.getModel()).addElement(selected_role);

      if (my_user_list.getSelectedValue().getID() == my_user.getID()) {
        setChanged();
        notifyObservers(selected_role);
      }

      programChairCheck();
      subprogramChairCheck();
    }

  }

}
