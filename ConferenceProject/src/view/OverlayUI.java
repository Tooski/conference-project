
package view;
import db.ConferenceDB;
import db.ConferenceUserDB;
import db.FeedbackDB;
import db.UserDB;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import model.Conference;
import model.ConferenceRole;
import model.User;
import swing.custom.JCustomButton;
import swing.custom.JCustomComboBox;
import swing.custom.JCustomLabel;
import swing.custom.JCustomList;
import swing.custom.JDialogBox;
import view.paper.PaperGUI;



/**
 * 
 * @author Josef Nosov
 * @version 06/06/13
 * 
 */
@SuppressWarnings("serial")
public class OverlayUI extends JFrame implements Observer {

  /**
   * The left panel.
   */
  private JPanel my_left_panel;
  /**
   * The top panel.
   */
  private JPanel my_top_panel;
  /**
   * The current user's id.
   */
  private int my_user_id;
  /**
   * The role for the selected conference.
   */
  private JCustomComboBox<ConferenceRole> my_role;
  /**
   * the currently selected conference.
   */
  private Conference my_selected_conference;
  /**
   * The center panel.
   */
  private JPanel my_center_panel;
  /**
   * Used to add listener.
   */
  private OverlayUI my_this_class;
  /**
   * the selected user.
   */
  private User my_current_user;
  /**
   * allows user to go back.
   */
  private Stack<JPanelHolder> my_back_history;
  /**
   * The left panel.
   */
  private final JButton my_back = new JCustomButton("Back");
  
  /**
   * The list of conferences.
   */
  private JCustomList<Conference> my_conferences;


  /**
   * The overlay.
   * @param user_id the user ID that is being passed.
   */
  public OverlayUI(int user_id) {
    super("MSEE Conference Organizer");

    my_this_class = this;
    my_left_panel = new JPanel(new BorderLayout());
    my_current_user = UserDB.getUser(user_id);
    my_center_panel = new JPanel(new BorderLayout());
    my_top_panel = new JPanel(new BorderLayout());
    my_user_id = user_id;
    my_role = new JCustomComboBox<>(0, false);
    my_back_history = new Stack<>();

    final JLabel conference_name = new JCustomLabel();
    final JLabel conference_date = new JCustomLabel();
    final JLabel conference_role;
    if (UserDB.isAdmin(my_user_id)) {
      conference_role = new JCustomLabel("User Role: Admin");
      my_center_panel.add(new ConferenceDBUI(my_this_class, my_user_id));

    } else {
      conference_role = new JCustomLabel("User Role: User");
    }
    final JLabel user_name = new JCustomLabel("Username: " + UserDB.getUserName(my_user_id));
    final JPanel panel = new JPanel();
    final JButton logout = new JCustomButton("Logout");
    logout.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent the_arg) {
        new LoginUI().display();
        dispose();
      }

    });

    panel.add(logout);
    panel.setOpaque(false);
    final JPanel label_row = new JPanel(new GridLayout(2, 2));
    label_row.add(conference_name);
    label_row.add(conference_role);
    label_row.add(conference_date);
    label_row.add(user_name);
    label_row.setOpaque(false);
    label_row.setBorder(new EmptyBorder(5, 10, 5, 10));
    my_top_panel.add(label_row, BorderLayout.CENTER);
    my_top_panel.add(panel, BorderLayout.EAST);
    my_top_panel.setBackground(Color.WHITE);
    my_left_panel.add(my_role, BorderLayout.NORTH);
    my_left_panel.add(initTable(), BorderLayout.CENTER);
    my_back.setEnabled(false);

    my_back.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        if (my_back_history.size() > 0) {

          my_center_panel.removeAll();
          final JPanelHolder jph = my_back_history.pop();
          jph.getUser().setCurrentRole(jph.getCurrentRole());
          if (my_role.getItemCount() > 0) {
            conference_name.setText("Conference Name: " +
                                    jph.getConference().getConferenceName());
            conference_date.setText("Conference Date: " +
                                    jph.getConference().getConferenceDate().toString());
            conference_role.setText("User Role: " + jph.getUser().getCurrentRole());

            my_current_user.setCurrentRole((ConferenceRole) my_role.getSelectedItem());
          }
          my_center_panel.add(jph.display());
          validate();
          repaint();
          my_back.setEnabled(!my_back_history.isEmpty());
        }

      }
    });

    my_left_panel.add(my_back, BorderLayout.SOUTH);
    my_left_panel.setBackground(Color.WHITE);
    my_role.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (my_role.getItemCount() > 0) {
          conference_name.setText("Conference Name: " +
                                  my_selected_conference.getConferenceName());
          conference_date.setText("Conference Date: " +
                                  my_selected_conference.getConferenceDate().toString());
          conference_role.setText("User Role: " + my_role.getSelectedItem().toString());

          if (my_center_panel.getComponentCount() >= 1) {
            my_back_history.addElement(new JPanelHolder((JPanel) my_center_panel.getComponent(0),
                                                     my_current_user, my_selected_conference));
            my_back.setEnabled(true);
            my_center_panel.removeAll();
          }

          my_current_user.setCurrentRole((ConferenceRole) my_role.getSelectedItem());

          GUI options = null;
          switch (my_current_user.getCurrentRole()) {
            case PROGRAM_CHAIR:
              my_current_user.setCurrentRole(ConferenceRole.PROGRAM_CHAIR);
              options = new OptionsUI(my_selected_conference, my_current_user);
              break;
            case SUBPROGRAM_CHAIR:
              my_current_user.setCurrentRole(ConferenceRole.SUBPROGRAM_CHAIR);

              options = new OptionsUI(my_selected_conference, my_current_user);

              break;

            case REVIEWER:
              my_current_user.setCurrentRole(ConferenceRole.REVIEWER);

              options = new PaperGUI(my_selected_conference, my_current_user);
              break;
            case USER:
            case AUTHOR:
            default:
              my_current_user.setCurrentRole(ConferenceRole.USER);
              options = new PaperGUI(my_selected_conference, my_current_user);
              break;
          }
          ((Observable) options).addObserver(my_this_class);
          my_center_panel.add(options.display());
          validate();
          repaint();

        }

      }

    });
    final JSplitPane jsp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, my_left_panel, my_center_panel);
    jsp.setDividerSize(3);
    jsp.setContinuousLayout(true);
    add(jsp, BorderLayout.CENTER);
    my_left_panel.setBorder(new EmptyBorder(6, 6, 6, 6));
    add(my_top_panel, BorderLayout.NORTH);
  }


  /**
   * Updates the roles.
   * 
   * @param conference_id the conference ID.
   */
  private void updateRoles(final int conference_id) {
    my_role.removeAllItems();
    final List<ConferenceRole> c = ConferenceUserDB.getRole(conference_id, my_user_id);

    for (ConferenceRole r : c) {
      my_role.addItem(r);
    }
    if (!ConferenceUserDB.getRole(conference_id, my_user_id)
        .contains(ConferenceRole.PROGRAM_CHAIR)) {
      if (FeedbackDB.isAuthor(my_user_id)) {
        my_role.addItem(ConferenceRole.AUTHOR);
      } else {
        my_role.addItem(ConferenceRole.USER);
      }
    }

  }


  /**
   * Creates the table panel;
   * 
   * @return the panel holding the table.
   */
  private JPanel initTable() {

    JPanel panel = new JPanel(new BorderLayout());

    my_conferences = new JCustomList<Conference>();
    my_conferences.addElements(ConferenceDB.getConferenceList());

    final JButton info = new JCustomButton("Info");
    info.setEnabled(false);
    info.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (my_conferences.getSelectedIndex() != -1) {
          Conference selected_conference = my_conferences.getSelectedValue();
          final JComponent[] inputs =
              new JComponent[] {

                  new JCustomLabel("Conference Name: " +
                                   selected_conference.getConferenceName()),
                  new JCustomLabel("Conference Date: " +
                                   selected_conference.getConferenceDate()),
                  new JCustomLabel("Program Chair: " +
                                   selected_conference.getProgramChair().getActualName()),

              };
          JDialogBox.createDialogBox(inputs, selected_conference.getConferenceName());

        }

      }

    });

    JPanel info_panel = new JPanel(new BorderLayout());

    JPanel select_panel = new JPanel(new BorderLayout());
    final JButton select_button = new JCustomButton("Select");
    final JPanel button_panel = new JPanel(new BorderLayout());
    final Conference[] previous_selected = {null};
    if (!UserDB.isAdmin(my_user_id)) {

      button_panel.setLayout(new GridLayout(1, 0));
      select_button.setEnabled(false);
      select_button.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent arg0) {
          if (my_conferences.getSelectedIndex() != -1) {
            Object o = my_conferences.getSelectedValue();
            if (o != null) {
              my_selected_conference = (Conference) o;

              previous_selected[0] = (Conference) o;
              if (my_center_panel.getComponentCount() > 0) {
                my_back_history.addElement(new JPanelHolder(
                                                         (JPanel) my_center_panel.getComponent(0),
                                                         my_current_user, my_selected_conference));
                my_back.setEnabled(true);
                my_center_panel.removeAll();
              }
              updateRoles(my_selected_conference.getID());
              select_button.setEnabled(false);
            }
          }
        }
      });
      select_panel.add(select_button, BorderLayout.CENTER);
      info_panel.setBorder(new EmptyBorder(6, 3, 6, 0));
    } else
      info_panel.setBorder(new EmptyBorder(6, 0, 6, 0));

    my_conferences.addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(ListSelectionEvent e) {
        boolean b = true;
        if (previous_selected[0] != null)
          b = !previous_selected[0].equals(my_conferences.getSelectedValue());
        select_button.setEnabled(my_conferences.getSelectedIndex() != -1 && b);
        info.setEnabled(my_conferences.getSelectedIndex() != -1);
      }

    });

    info_panel.add(info, BorderLayout.CENTER);
    select_panel.setBorder(new EmptyBorder(6, 0, 6, 3));
    info_panel.setOpaque(false);
    select_panel.setOpaque(false);
    button_panel.add(select_panel);
    button_panel.add(info_panel);
    button_panel.setBackground(Color.WHITE);
    JScrollPane jsp = new JScrollPane(my_conferences);
    jsp.setPreferredSize(new Dimension(125, 0));
    panel.add(jsp, BorderLayout.CENTER);
    panel.add(button_panel, BorderLayout.SOUTH);
    return panel;
  }
  
  /**
   * Creates a display.
   */
  public void display() {
    setSize(800, 600);
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setResizable(true);
    setVisible(true);
    setLocationRelativeTo(null);

  }


  @Override
  public void update(final Observable the_o, final Object the_object) {
    if (the_o instanceof CreateConUI) {
      my_conferences.addElement(ConferenceDB.getConference((int) the_object));
    } else if (the_object instanceof ConferenceRole) {
      my_role.addItem((ConferenceRole) the_object);
    } else {
      if (the_o instanceof PaperGUI) {
        final int user =
            ((DefaultComboBoxModel<ConferenceRole>) my_role.getModel())
                .getIndexOf(ConferenceRole.USER);

        final int author =
            ((DefaultComboBoxModel<ConferenceRole>) my_role.getModel())
                .getIndexOf(ConferenceRole.AUTHOR);
        if (FeedbackDB.isAuthor(my_user_id) && user != -1) {
          my_role.addItem(ConferenceRole.AUTHOR);

          ((DefaultComboBoxModel<ConferenceRole>) my_role.getModel()).removeElementAt(user);

          my_role.setSelectedItem(ConferenceRole.AUTHOR);
        } else if (!FeedbackDB.isAuthor(my_user_id) && author != -1) {

          my_role.addItem(ConferenceRole.USER);
          ((DefaultComboBoxModel<ConferenceRole>) my_role.getModel()).removeElementAt(author);

          my_role.setSelectedItem(ConferenceRole.USER);
        }
      } else {
        if (the_object instanceof Observable) {
          ((Observable) the_object).addObserver(this);
        }
        my_back_history.addElement(new JPanelHolder((JPanel) my_center_panel.getComponent(0),
                                                 my_current_user, my_selected_conference));
        my_back.setEnabled(true);
        my_center_panel.removeAll();
        my_center_panel.add(((GUI) the_object).display());
        validate();
        repaint();
      }
    }

  }

}
