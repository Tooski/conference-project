
package view;

import javax.swing.JPanel;

import model.Conference;
import model.ConferenceRole;
import model.User;

/**
 * This is used to for the back history.
 * 
 * @author Josef Nosov
 * @version 5/31/13
 * 
 */
public class JPanelHolder implements GUI {

  /**
   * The panel.
   */
  private JPanel my_panel;
  /**
   * The user.
   */
  private User my_user;

  /**
   * The conference role.
   */
  private ConferenceRole my_current_role;

  /**
   * The conference.
   */
  private Conference my_conference;

  /**
   * Constructor of JPanelHolder.
   * 
   * @param the_jp the panel
   * @param the_user the user
   * @param the_conference the conference
   */
  public JPanelHolder(final JPanel the_jp, final User the_user, 
                      final Conference the_conference) {
    my_panel = the_jp;
    my_user = the_user;
    my_conference = the_conference;
    my_current_role = the_user.getCurrentRole();
  }

  /**
   * Get a user.
   * 
   * @return a user
   */
  public User getUser() {
    return my_user;
  }

  @Override
  public JPanel display() {
    return my_panel;
  }

  /**
   * Gets the conference role of the user.
   * 
   * @return the conference role
   */
  public ConferenceRole getCurrentRole() {
    return my_current_role;
  }

  /**
   * Gets the conference.
   * 
   * @return the conference
   */
  public Conference getConference() {
    return my_conference;
  }

}
