
package view;

import javax.swing.JPanel;

/**
 * GUI interface.
 * 
 * @author Josef Nosov
 * @version 5/31/13
 */
public interface GUI {
  /**
   * Displays the panel.
   * 
   * @return the panel
   */
  JPanel display();

}
