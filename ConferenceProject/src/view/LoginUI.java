
package view;
import db.UserDB;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import model.User;
import swing.custom.JCustomButton;
import swing.custom.JCustomLabel;
import swing.custom.JCustomTextField;
import swing.custom.JDefaultFont;
import swing.custom.JDialogBox;


/**
 * LoginUI class.
 * 
 * @author Josef Nosov
 * @version 5/31/13
 */
public class LoginUI implements Observer {
  /**
   * Username text field.
   */
  private final JCustomTextField my_username_text;

  /**
   * Password text field.
   */
  private final JPasswordField my_password_text;

  /**
   * A button used to log in.
   */
  private final JButton my_login_button;
  
  /**
   * The frame.
   */
  private JFrame my_frame;
  
  /**
   * Swaps with previous_panel when creating user.
   */
  private JPanel panel;
  /**
   * Swaps with panel when login in.
   */
  private JPanel previous_panel;

  /**
   * Constructor of the LoginUI.
   */
  public LoginUI() {
    super();
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (final Exception ex) {
      ex.printStackTrace();
    }
    my_username_text = new JCustomTextField();
    my_password_text = new JPasswordField();
    my_username_text.setFont(new JDefaultFont().getFont(20));
    my_password_text.setFont(new JDefaultFont().getFont(20));
    my_password_text.setEchoChar('*');
    my_login_button = new JCustomButton("Login");
    setupComponents();
    final JFrame frame = new JFrame(panel.getName());
    frame.setSize(800, 600);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.add(previous_panel);
    frame.pack();

    my_frame = frame;
  }

  /**
   * Helper method to perform the work of setting up the GUI components.
   */
  private void setupComponents() {
    my_login_button.addActionListener(new LoginListener());
    my_login_button.setMnemonic(KeyEvent.VK_L);
    final JButton create_button = new JCustomButton("Create account?");
    create_button.setMnemonic(KeyEvent.VK_C);
    create_button.addActionListener(new CreateAccountListener());

    final JLabel username_label = new JCustomLabel("Username:   ");
    username_label.setFont(new JDefaultFont().getFont(15));
    final JLabel password_label = new JCustomLabel("Password:   ");
    password_label.setFont(new JDefaultFont().getFont(15));

    my_username_text.setEditable(true);
    my_password_text.setEditable(true);

    final JPanel master_panel = new JPanel();
    master_panel.setName("Login");
    master_panel.setLayout(new BoxLayout(master_panel, BoxLayout.Y_AXIS));
    final JPanel login_p = new JPanel(new BorderLayout());

    final JPanel login_panel = new JPanel(new BorderLayout());
    final JPanel username_fields = new JPanel(new BorderLayout());
    username_fields.add(username_label, BorderLayout.WEST);
    username_fields.add(my_username_text, BorderLayout.CENTER);

    final JPanel password_fields = new JPanel(new BorderLayout());
    password_fields.add(password_label, BorderLayout.WEST);
    password_fields.add(my_password_text, BorderLayout.CENTER);

    login_panel.add(username_fields, BorderLayout.NORTH);
    login_panel.add(password_fields, BorderLayout.SOUTH);
    login_p.add(login_panel, BorderLayout.NORTH);
    login_p.setBorder(new EmptyBorder(0, 10, 0, 10));

    final JPanel create = new JPanel(new GridLayout(1, 2));
    create.add(my_login_button);
    create.add(create_button);
    create.setBorder(new EmptyBorder(10, 10, 10, 10));
    final JPanel bottom_panel = new JPanel(new BorderLayout());

    bottom_panel.add(create, BorderLayout.NORTH);

    final JLabel label = new JLabel("MSEE", JLabel.CENTER);
    label.setFont(new JDefaultFont().getFont(72));
    final JLabel bottom_label = new JLabel("Conference Organizer", JLabel.CENTER);
    bottom_label.setFont(new JDefaultFont().getFont(18));
    final JPanel p = new JPanel(new BorderLayout());
    p.add(label, BorderLayout.CENTER);
    p.add(bottom_label, BorderLayout.SOUTH);
    p.setBorder(new EmptyBorder(0, 0, 10, 0));
    master_panel.add(p);
    master_panel.add(login_p);
    master_panel.add(create);
    panel = master_panel;
    CreateAccountGUI acc = new CreateAccountGUI();
    previous_panel = acc.display();
    acc.addObserver(this);

  }



  /**
   * Creates and displays the application frame.
   */
  public void display() {

    my_frame.remove(previous_panel);
    my_frame.add(panel);
    my_frame.validate();
    my_frame.repaint();
    my_frame.setLocationRelativeTo(null);
    my_frame.setVisible(true);
    my_frame.setResizable(false);
    my_username_text.grabFocus();
    panel.getRootPane().setDefaultButton(my_login_button);
  }

  /**
   * An action listener for the login button.
   */
  private class LoginListener implements ActionListener {
    public void actionPerformed(final ActionEvent the_event) {
      final String username = my_username_text.getText();
      final String password = String.valueOf(my_password_text.getPassword());
      final User user = UserDB.login(username, password);
      if (user != null) {
        my_frame.dispose();
        new OverlayUI(user.getID()).display();
      } else {
        JDialogBox.createDialogBox("Username/password is incorrect.", null);
      }

    }
  }

  /**
   * An action listener for the create account button.
   */
  private class CreateAccountListener implements ActionListener {

    public void actionPerformed(final ActionEvent the_event) {
      my_frame.setTitle(previous_panel.getName());
      my_frame.remove(panel);
      final JPanel temp = panel;
      panel = previous_panel;
      previous_panel = temp;
      my_frame.add(panel);
      my_frame.validate();
      my_frame.repaint();

    }
  }

  @Override
  public void update(Observable arg0, Object arg1) {
    my_frame.setTitle(previous_panel.getName());
    my_frame.remove(panel);
    JPanel temp = panel;
    panel = previous_panel;
    previous_panel = temp;
    my_frame.add(panel);
    my_frame.validate();
    my_frame.repaint();

  }
}
