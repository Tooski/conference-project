
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.ConferenceRole;
import model.User;

/**
 * Connects the conference with the user.
 * @author Josef Nosov
 * @version 05/31/13
 *
 */
public class ConferenceUserDB extends AbstractDB {

  static Map<Integer, List<Integer>> my_users = getUsers();

  static {
    try {
      Connection conn = getConnection();
      conn.createStatement()
          .executeUpdate("create table if not exists USER_CONFERENCE (CONFERENCE_ID INTEGER, USER_ID INTEGER, USER_ROLE INTEGER)");
      conn.createStatement()
          .executeUpdate("create table if not exists USER_USER (CONFERENCE_ID INTEGER, USER_ID INTEGER, USER_ROLE INTERGER, PARTICIPANT_ID INTEGER, PARTICIPANT_ROLE INTEGER)");
      conn.close();
    } catch (Exception e) {
    }
  }

  /**
   * Adds a new user to a conference
   * 
   * @param conference_id the id of the conference
   * @param user_id the id of the user
   * @param participant_id the participant ID
   * @param role the role of the user in this conference
   * @return if the user was added to the conference
   */
  public static boolean addUser(int conference_id, User user_id, int participant_id,
                                ConferenceRole role) {
    int current_role = user_id.getCurrentRole().getRole();
    try {
      Connection conn = getConnection();
      if (usernameExists(conn, conference_id, participant_id, role.getRole()))
        return false;
      PreparedStatement pstmt =
          conn.prepareStatement("INSERT INTO USER_CONFERENCE(CONFERENCE_ID, USER_ID, USER_ROLE) VALUES (?, ?, ?)");

      pstmt.setInt(1, conference_id);
      pstmt.setInt(2, participant_id);
      pstmt.setInt(3, role.getRole());
      pstmt.executeUpdate();
      pstmt.close();

      PreparedStatement st =
          conn.prepareStatement("SELECT * FROM USER_CONFERENCE WHERE CONFERENCE_ID = (SELECT MAX(CONFERENCE_ID) FROM USER_CONFERENCE)");
      ResultSet rd = st.executeQuery();
      if (rd.next()) {
        if (!my_users.containsKey(rd.getInt(1))) {
          List<Integer> users = new ArrayList<>();
          users.add(rd.getInt(2));
          my_users.put(rd.getInt(1), users);
        } else {
          if (!my_users.get(rd.getInt(1)).contains(rd.getInt(2)))
            my_users.get(rd.getInt(1)).add(rd.getInt(2));
        }
      }

      st.close();
      rd.close();

      if (!userNameExists(conn, conference_id, user_id.getID(), current_role, participant_id,
                          role.getRole())) {

        PreparedStatement pst =
            conn.prepareStatement("INSERT INTO USER_USER(CONFERENCE_ID, USER_ID, USER_ROLE, PARTICIPANT_ID, PARTICIPANT_ROLE) VALUES (?, ?, ?, ?, ?)");
        pst.setInt(1, conference_id);
        pst.setInt(2, user_id.getID());
        pst.setInt(3, current_role);
        pst.setInt(4, participant_id);
        pst.setInt(5, role.getRole());

        pst.executeUpdate();
        pst.close();

      }
      conn.close();
      return true;
    } catch (Exception e) {
    }
    return false;

  }

  /**
   * Checks to see if the username already exists
   * 
   * @param conn the connection
   * @param conference_id the id of the conference
   * @param user_id the id of the user
   * @param user_role the role of the user
   * @param participant_id the id of the participant
   * @param participant_role the role of the participant
   * @return if the username exists
   * @throws SQLException
   */
  private static boolean userNameExists(Connection conn, int conference_id, int user_id,
                                        int user_role, int participant_id, int participant_role)
      throws SQLException {
    boolean name_exists = false;
    PreparedStatement st =
        conn.prepareStatement("SELECT * FROM USER_USER WHERE CONFERENCE_ID = " +
                              conference_id + " AND USER_ID = " + user_id +
                              " AND PARTICIPANT_ID =" + participant_id +
                              " AND PARTICIPANT_ROLE = " + participant_role +
                              " AND USER_ROLE = " + user_role);

    ResultSet rd = st.executeQuery();
    if (rd.next()) {
      name_exists = true;
      conn.close();

    }
    rd.close();
    st.close();
    return name_exists;
  }

  /**
   * Checks to see if the username already exists
   * 
   * @param conn the connection
   * @param conference_id the id of the conference
   * @param user_id the id of the user
   * @param role the role of the user
   * @return if the username exists
   * @throws SQLException
   */
  private static boolean usernameExists(Connection conn, int conference_id, int user_id,
                                        int role) throws SQLException {
    boolean name_exists = false;
    PreparedStatement st =
        conn.prepareStatement("SELECT * FROM USER_CONFERENCE WHERE CONFERENCE_ID = ? AND USER_ID = ? AND USER_ROLE = ?");
    st.setInt(1, conference_id);
    st.setInt(2, user_id);
    st.setInt(3, role);

    ResultSet rd = st.executeQuery();
    if (rd.next() && rd.getInt("USER_ID") == user_id &&
        rd.getInt("CONFERENCE_ID") == conference_id && rd.getInt("USER_ROLE") == role) {
      name_exists = true;
      conn.close();
    }
    rd.close();
    st.close();
    return name_exists;
  }


  public static List<ConferenceRole> getRole(int conference_id, int user_id) {
    List<ConferenceRole> role = new ArrayList<>();
    try {

      Connection conn = getConnection();
      PreparedStatement st =
          conn.prepareStatement("SELECT USER_ROLE FROM USER_CONFERENCE WHERE USER_ID = " +
                                user_id + " AND CONFERENCE_ID = " + conference_id);
      ResultSet rd = st.executeQuery();
      while (rd.next()) {
        switch (rd.getInt(1)) {
          case 0:
            role.add(ConferenceRole.ADMIN);
            break;
          case 1:
            role.add(ConferenceRole.PROGRAM_CHAIR);
            break;
          case 2:
            role.add(ConferenceRole.SUBPROGRAM_CHAIR);
            break;
          case 3:
            role.add(ConferenceRole.REVIEWER);
            break;
          case 4:
            role.add(ConferenceRole.AUTHOR);
            break;
        }
      }
      st.close();
      rd.close();
      conn.close();
    } catch (Exception e) {
    }
    return role;
  }

  /**
   * get the map of users
   * 
   * @return the map of the users
   */
  private static Map<Integer, List<Integer>> getUsers() {
    Map<Integer, List<Integer>> my_users = new HashMap<>();

    try {
      Connection conn = getConnection();
      PreparedStatement st = conn.prepareStatement("SELECT * FROM USER_CONFERENCE");
      ResultSet rd = st.executeQuery();
      while (rd.next()) {
        if (!my_users.containsKey(rd.getInt(1))) {
          List<Integer> users = new ArrayList<>();
          users.add(rd.getInt(2));
          my_users.put(rd.getInt(1), users);
        } else {
          if (!my_users.get(rd.getInt(1)).contains(rd.getInt(2)))
            my_users.get(rd.getInt(1)).add(rd.getInt(2));
        }

      }
      rd.close();
      st.close();
      conn.close();
    } catch (Exception e) {
    }

    return my_users;
  }

  /**
   * promote a user to a new role in a conference
   * 
   * @param conference_id the id of the conference
   * @param user_id the id of the user
   * @param participant_id the id of the participant
   * @param role the new role the user will take
   * @return if the user was promoted
   */
  public static boolean promoteUser(int conference_id, User user_id, int participant_id,
                                    ConferenceRole role) {
    int current_role = ConferenceRole.USER.getRole();
    if (user_id.getCurrentRole() != null)
      current_role = user_id.getCurrentRole().getRole();
    try {
      Connection conn = getConnection();

      if (!usernameExists(conn, conference_id, participant_id, role.getRole())) {
        PreparedStatement pstmt =
            conn.prepareStatement("INSERT INTO USER_CONFERENCE(CONFERENCE_ID, USER_ID, USER_ROLE) VALUES (?, ?, ?)");

        pstmt.setInt(1, conference_id);
        pstmt.setInt(2, participant_id);
        pstmt.setInt(3, role.getRole());
        pstmt.executeUpdate();
        pstmt.close();
      }

      if (!userNameExists(conn, conference_id, user_id.getID(), current_role, participant_id,
                          role.getRole())) {
        PreparedStatement pst =
            conn.prepareStatement("INSERT INTO USER_USER(CONFERENCE_ID, USER_ID, USER_ROLE, PARTICIPANT_ID, PARTICIPANT_ROLE) VALUES (?, ?, ?, ?, ?)");
        pst.setInt(1, conference_id);
        pst.setInt(2, user_id.getID());
        pst.setInt(3, current_role);
        pst.setInt(4, participant_id);
        pst.setInt(5, role.getRole());

        pst.executeUpdate();
        pst.close();
      }
      conn.close();
      return true;
    } catch (Exception e) {
    }
    return false;
  }

  /**
   * get the map of participants
   * 
   * @param conference_id the id of the confernce
   * @param user_id the id of the user
   * @param role the role of the user
   * @return the map of the users in the conference
   */
  public static Map<User, List<ConferenceRole>> getParticipants(int conference_id,
                                                                int user_id, int role) {
    Map<User, List<ConferenceRole>> participants = new HashMap<>();
    try {
      Connection conn = getConnection();
      PreparedStatement st =
          conn.prepareStatement("SELECT * FROM USER_USER WHERE CONFERENCE_ID = " +
                                conference_id + " AND USER_ID = " + user_id +
                                " AND USER_ROLE = " + role);
      ResultSet rd = st.executeQuery();
      while (rd.next()) {
        ConferenceRole part_role = ConferenceRole.USER;
        switch (rd.getInt("PARTICIPANT_ROLE")) {

          case 2:
            part_role = ConferenceRole.SUBPROGRAM_CHAIR;
            break;
          case 3:
            part_role = ConferenceRole.REVIEWER;
            break;
        }
        User u = UserDB.getUser(rd.getInt("PARTICIPANT_ID"));
        if (participants.get(u) == null) {

          List<ConferenceRole> r = new ArrayList<>();
          r.add(part_role);
          participants.put(u, r);
        } else {

          participants.get(u).add(part_role);
        }

      }
      rd.close();
      st.close();
      conn.close();
    } catch (Exception e) {
    }

    return participants;

  }

  /**
   * get the list of users
   * 
   * @param conference_id the id of the conference
   * @return the list of users
   */
  public static List<User> getUserList(int conference_id) {
    List<User> users = new ArrayList<>();
    for (int i : my_users.get(conference_id)) {
      users.add(UserDB.getUser(i));
    }
    return users;
  }
}
