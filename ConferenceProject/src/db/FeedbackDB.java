
package db;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Conference;
import model.ConferenceRole;
import model.Feedback;

import view.ReviewFormUI;

/**
 * The feedbacks.
 * @author Josef Nosov
 * @version 05/31/13
 *
 */
public class FeedbackDB extends AbstractDB {

  static Map<Integer, List<Feedback>> my_feedbacks = getFeedback();
  static Map<Integer, Feedback> all_feedbacks;

  private static String CREATE =
      "create table if not exists FEEDBACK (FEEDBACK_ID INTEGER, CONFERENCE_ID INTEGER, PAPER_TITLE STRING, PAPER_AUTHOR STRING, PAPER_TEXT STRING, AUTHOR_ID INTEGER, PAPER BLOB, PRIMARY KEY (FEEDBACK_ID ASC))";

  static {
    try {
      Connection conn = getConnection();
      conn.createStatement().executeUpdate(CREATE);
      conn.close();
    } catch (Exception e) {
    }
  }

  public static int createFeedback(int conference_id, String paper_title, String paper_author,
                                   String paper_text, int author_id, Object blob) {
    int index = -1;
    try {
      Connection conn = getConnection();

      if (feedbackExists(conn, conference_id, paper_title, paper_author, paper_text, author_id))
        return -1;
      PreparedStatement pstmt =
          conn.prepareStatement("INSERT INTO FEEDBACK(CONFERENCE_ID, PAPER_TITLE, PAPER_AUTHOR, PAPER_TEXT, AUTHOR_ID) VALUES (?, ?, ?, ?, ?)");
      pstmt.setInt(1, conference_id);
      pstmt.setString(2, paper_title);
      pstmt.setString(3, paper_author);
      pstmt.setString(4, paper_text);
      pstmt.setInt(5, author_id);

      pstmt.executeUpdate();
      pstmt.close();

      PreparedStatement st =
          conn.prepareStatement("SELECT * FROM FEEDBACK WHERE FEEDBACK_ID = (SELECT MAX(FEEDBACK_ID) FROM FEEDBACK) AND CONFERENCE_ID = " +
                                conference_id);
      ResultSet rd = st.executeQuery();

      if (rd.next()) {

        index = rd.getInt(1);

        if (!my_feedbacks.containsKey(conference_id)) {
          List<Feedback> users = new ArrayList<>();
          Feedback temp =
              new Feedback(index, author_id, paper_author, paper_text, paper_title,
                           conference_id, blob);
          users.add(temp);
          my_feedbacks.put(conference_id, users);
          all_feedbacks.put(index, temp);
        } else {
          boolean contains = false;
          for (Feedback feed : my_feedbacks.get(conference_id)) {

            if (feed.getID() == rd.getInt(1)) {
              contains = true;
            }
          }
          if (!contains) {
            Feedback fb =
                new Feedback(index, author_id, paper_author, paper_text, paper_title,
                             conference_id, blob);
            my_feedbacks.get(conference_id).add(fb);
            all_feedbacks.put(rd.getInt(1), fb);
          }
        }
      }

      ObjectOutputStream oos =
          new ObjectOutputStream(
                                 new FileOutputStream(Integer
                                     .toString((conference_id * 37 + index * 17))));
      oos.writeObject(blob);
      oos.flush();
      PreparedStatement st1 =
          conn.prepareStatement("UPDATE FEEDBACK SET PAPER = '" + oos +
                                "' WHERE CONFERENCE_ID = " + conference_id +
                                " AND FEEDBACK_ID = " + index);
      st1.executeUpdate();
      oos.close();
      conn.close();

      FeedbackUserDB.assignFeedback(conference_id, index, 0, 0, author_id,
                                    ConferenceRole.AUTHOR.getRole());
      return index;
    } catch (Exception e) {
    }
    return index;
  }

  private static Object query(int conference_id, String column) {
    Object obj = null;
    try {

      Connection conn = getConnection();
      PreparedStatement st =
          conn.prepareStatement("SELECT " + column + " FROM FEEDBACK WHERE FEEDBACK_ID = ?");
      st.setInt(1, conference_id);
      ResultSet rd = st.executeQuery();
      if (rd.next()) {
        obj = rd.getObject(column);
      }

      st.close();
      rd.close();
      conn.close();
      return obj;
    } catch (Exception e) {
      return e.getMessage();
    }
  }

  public static String getPaper(int feedback_id) {
    return String.valueOf(query(feedback_id, "PAPER_TEXT"));
  }

  public static String getAuthor(int feedback_id) {
    return String.valueOf(query(feedback_id, "PAPER_AUTHOR"));
  }

  public static String getTitle(int feedback_id) {
    return String.valueOf(query(feedback_id, "PAPER_TITLE"));
  }

  public static Conference getConference(int feedback_id) {
    return ConferenceDB.getConference((int) query(feedback_id, "CONFERENCE_ID"));
  }

  public static int getAuthorID(int feedback_id) {
    return (int) query(feedback_id, "AUTHOR_ID");
  }

  private static boolean feedbackExists(Connection conn, int conference_id,
                                        String paper_title, String paper_author,
                                        String paper_text, int author_id) throws SQLException {
    boolean name_exists = false;
    PreparedStatement st =
        conn.prepareStatement("SELECT * FROM FEEDBACK WHERE CONFERENCE_ID = ? AND PAPER_TITLE = ? AND PAPER_AUTHOR = ? AND PAPER_TEXT = ? AND AUTHOR_ID = ?");
    st.setInt(1, conference_id);
    st.setString(2, paper_title);
    st.setString(3, paper_author);
    st.setString(4, paper_text);
    st.setInt(5, author_id);

    ResultSet rd = st.executeQuery();
    if (rd.next()) {

      if (rd.getInt("CONFERENCE_ID") == conference_id &&
          rd.getString("PAPER_TITLE").equals(paper_title) &&
          rd.getString("PAPER_AUTHOR").equals(paper_author) &&
          rd.getString("PAPER_TEXT").equals(paper_text) && rd.getInt("AUTHOR_ID") == author_id) {
        name_exists = true;
        conn.close();
      }
    }
    st.close();

    rd.close();
    return name_exists;
  }

  public static boolean editPaper(int feedback_id, int conference_id, Object obj) {
    try {

      Connection conn = getConnection();

      ObjectOutputStream oos =
          new ObjectOutputStream(
                                 new FileOutputStream(Integer
                                     .toString((conference_id * 37 + feedback_id * 17))));
      oos.writeObject(obj);
      oos.flush();
      PreparedStatement st1 =
          conn.prepareStatement("UPDATE FEEDBACK SET PAPER = '" + oos +
                                "' WHERE CONFERENCE_ID = " + conference_id +
                                " AND FEEDBACK_ID = " + feedback_id);
      st1.executeUpdate();
      oos.close();
      conn.close();

      return true;

    } catch (Exception e) {
    }
    return false;
  }

  public static boolean deletePaper(int feedback_id) {
    try {
      int conference_id = getConference(feedback_id).getID();
      List<Feedback> list = my_feedbacks.get(conference_id);
      for (int i = 0; i < list.size(); i++) {
        if (list.get(i).getID() == feedback_id) {
          my_feedbacks.get(conference_id).remove(i);
        }
      }
      Connection conn = getConnection();

      PreparedStatement st =
          conn.prepareStatement("DELETE FROM FEEDBACK WHERE FEEDBACK_ID = " + feedback_id);
      st.executeUpdate();
      st.close();
      conn.close();

      FeedbackUserDB.deleteUserPaper(feedback_id);

      return true;

    } catch (Exception e) {
    }
    return false;

  }


  private static Map<Integer, List<Feedback>> getFeedback() {
    Map<Integer, List<Feedback>> feedbacks = new HashMap<>();
    all_feedbacks = new HashMap<>();
    try {
      Connection conn = getConnection();
      PreparedStatement st = conn.prepareStatement("SELECT * FROM FEEDBACK");
      ResultSet rd = st.executeQuery();
      while (rd.next()) {
        int author_id = rd.getInt("AUTHOR_ID");
        String paper_author = rd.getString("PAPER_AUTHOR");
        String paper_text = rd.getString("PAPER_TEXT");
        String paper_title = rd.getString("PAPER_TITLE");

        ObjectInputStream oos =
            new ObjectInputStream(new FileInputStream(Integer.toString((rd
                .getInt("CONFERENCE_ID") * 37 + rd.getInt("FEEDBACK_ID") * 17))));

        Object blob = oos.readObject();
        oos.close();

        if (!feedbacks.containsKey(rd.getInt(2))) {
          List<Feedback> feedback = new ArrayList<>();
          Feedback temp =
              new Feedback(rd.getInt(1), author_id, paper_author, paper_text, paper_title,
                           rd.getInt("CONFERENCE_ID"), blob);
          feedback.add(temp);
          feedbacks.put(rd.getInt(2), feedback);
          all_feedbacks.put(rd.getInt(1), temp);
        } else {
          boolean contains = false;
          for (Feedback feed : feedbacks.get(rd.getInt(2))) {
            if (feed.getID() == rd.getInt(1)) {
              contains = true;
            }
          }
          if (!contains) {
            Feedback fb =
                new Feedback(rd.getInt(1), author_id, paper_author, paper_text, paper_title,
                             rd.getInt("CONFERENCE_ID"), blob);
            feedbacks.get(rd.getInt(2)).add(fb);
            all_feedbacks.put(rd.getInt(1), fb);
          }

        }

      }
      rd.close();
      st.close();
      conn.close();
    } catch (Exception e) {
    }

    return feedbacks;
  }

  public static List<Feedback> getFeedbackList(int conference_id) {

    return my_feedbacks.get(conference_id);
  }

  public static Feedback getFeedback(int feedback_id) {
    return all_feedbacks.get(feedback_id);

  }

  public static boolean isAuthor(int my_user_id) {
    try {

      Connection conn = getConnection();
      PreparedStatement st =
          conn.prepareStatement("SELECT * FROM FEEDBACK WHERE AUTHOR_ID = ?");
      st.setInt(1, my_user_id);
      ResultSet rd = st.executeQuery();
      if (rd.next()) {
        st.close();
        rd.close();
        conn.close();
        return true;
      }

      st.close();
      rd.close();
      conn.close();
      return false;
    } catch (Exception e) {
    }
    return false;
  }

  public static List<ReviewFormUI> getAllReviews(int feedback_id, Conference conf) {
    List<ReviewFormUI> getReviews = new ArrayList<>();
    try {

      Connection conn = getConnection();
      PreparedStatement st =
          conn.prepareStatement("SELECT * FROM FEEDBACK_USER WHERE FEEDBACK_ID = ?");
      st.setInt(1, feedback_id);
      ResultSet rd = st.executeQuery();
      while (rd.next()) {

        if (rd.getObject("SCORES") != null &&
            rd.getInt("ASSIGNED_BY") == conf.getProgramChair().getID()) {
          ObjectInputStream oos =
              new ObjectInputStream(new FileInputStream(Integer.toString(feedback_id * 37 +
                                                                         rd.getInt("USER_ID") *
                                                                         19)));
          getReviews.add(new ReviewFormUI(rd.getString("REVIEW"), (int[]) oos.readObject()));
          oos.close();

        }
      }
      st.close();
      rd.close();
      conn.close();
    } catch (Exception e) {
    }
    return getReviews;

  }

}
