
package db;

import java.sql.Connection;
import java.sql.DriverManager;


/**
 * Creates a connection.
 * @author Josef Nosov
 * @version 05/31/13
 *
 */
public abstract class AbstractDB {

  /**
   * check for a connection.
   * 
   * @return the connection to the database
   */
  protected static Connection getConnection() {
    Connection conn = null;
    try {
      Class.forName("org.sqlite.JDBC");
      conn = DriverManager.getConnection("jdbc:sqlite:conference.db");
    } catch (Exception e) {
    }
    return conn;
  }

}
