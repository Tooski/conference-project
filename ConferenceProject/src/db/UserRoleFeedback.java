
package db;

/**
 * UserRoleFeedback class.
 * 
 * @author Josef Nosov
 * @version 05/31/13
 * 
 */
public class UserRoleFeedback {
  private int my_id;
  private int my_role;
  private int my_feedback;

  /**
   * Constructor of the UserRoleFeedback.
   * 
   * @param user_id the user ID
   * @param role the role of user
   * @param feedback the feedback
   */
  public UserRoleFeedback(int user_id, int role, int feedback) {
    my_id = user_id;
    my_role = role;
    my_feedback = feedback;
  }

  /**
   * Gets the feedback.
   * 
   * @return the feedback
   */
  public int getFeedback() {
    return my_feedback;
  }

  /**
   * Gets user ID.
   * 
   * @return user ID
   */
  public int getID() {
    return my_id;
  }

  /**
   * Gets user role.
   * 
   * @return user role
   */
  public int getRole() {
    return my_role;
  }

  /**
   * {@inheritDoc}
   */
  public boolean equals(Object o) {
    return my_id == ((UserRoleFeedback) o).my_id &&
           my_role == ((UserRoleFeedback) o).my_role &&
           my_feedback == ((UserRoleFeedback) o).my_feedback;
  }

  /**
   * {@inheritDoc}
   */
  public int hashCode() {
    return my_id * 37 + my_role * 11 + my_feedback * 7;
  }
}
