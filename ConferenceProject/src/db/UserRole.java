
package db;

/**
 * UserRole class.
 * 
 * @author Josef Nosov
 * @version 05/31/13
 * 
 */
public class UserRole {
  private int my_id;
  private int my_role;
  private int my_assigned_id;

  /**
   * Constructor of UserRole
   * 
   * @param user_id the user ID
   * @param role the user's role
   * @param assigned_id the assigned ID
   */
  public UserRole(int user_id, int role, int assigned_id) {
    my_id = user_id;
    my_role = role;
    my_assigned_id = assigned_id;
  }

  /**
   * Gets assigned user ID.
   * 
   * @return the assigned user ID
   */
  public int getAsignee() {
    return my_assigned_id;
  }

  /**
   * Gets user ID.
   * 
   * @return the user ID
   */
  public int getID() {
    return my_id;
  }

  /**
   * Gets role of the user.
   * 
   * @return the user's role
   */
  public int getRole() {
    return my_role;
  }

  /**
   * {@inheritDoc}
   */
  public boolean equals(Object o) {
    if (((UserRole) o).my_assigned_id != 0)
      return my_id == ((UserRole) o).my_id && my_role == ((UserRole) o).my_role &&
             my_assigned_id == ((UserRole) o).my_assigned_id;

    return my_id == ((UserRole) o).my_id && my_role == ((UserRole) o).my_role;
  }

  /**
   * {@inheritDoc}
   */
  public int hashCode() {
    if (my_assigned_id != 0)
      return my_id * 37 + my_role * 11 + my_assigned_id * 7;
    return my_id * 37 + my_role * 11;
  }
}
