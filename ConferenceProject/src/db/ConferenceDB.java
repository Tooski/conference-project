
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import model.Conference;
import model.ConferenceRole;
import model.Date;
import model.User;

/**
 * Holds information for the conference.
 * @author Josef Nosov
 * @version 05/31/13
 *
 */
public class ConferenceDB extends AbstractDB {

  static Map<Integer, Conference> my_conference = getConferences();

  static final String SELECT = "SELECT * FROM CONFERENCE WHERE CONFERENCE_ID = ?";
  static final String CREATE_USER =
      "INSERT INTO CONFERENCE(CONFERENCE_NAME, PROGRAMCHAIR_ID, CONFERENCE_DATE, AUTHOR_DEADLINE, REVIEWER_DEADLINE, SUBPROGRAM_DEADLINE) VALUES (?, ?, ?, ?, ?, ?)";
  private static String create =
      "create table if not exists CONFERENCE (CONFERENCE_ID INTEGER, CONFERENCE_NAME STRING, PROGRAMCHAIR_ID INTEGER, CONFERENCE_DATE INTEGER, AUTHOR_DEADLINE INTERGER, REVIEWER_DEADLINE INTERGER, SUBPROGRAM_DEADLINE INTERGER, PRIMARY KEY (CONFERENCE_ID ASC))";

  static {
    try {
      Connection conn = getConnection();
      conn.createStatement().executeUpdate(create);

      conn.close();
    } catch (Exception e) {
    }
  }

  /**
   * Constructs a conference database object
   * 
   * @param conference_id the ID of the conference
   * @param conference_name the name of the conference
   * @param programchair_id the ID of the program chair
   * @param conference_date the final date of the conference
   * @param author_deadline the deadline for authors to submit papers
   * @param review_deadline the deadline for reviewers to submit their reviews
   *          of papers
   * @param subprogram_deadline the deadline for subprogram chairs to recommend
   *          a paper to the PC
   */
  public static int createConference(String conference_name, int programchair_id,
                                     Date conference_date, Date author_deadline,
                                     Date review_deadline, Date subprogram_deadline) {

    int conf_id = -1;
    try {
      Connection conn = getConnection();
      PreparedStatement pstmt = conn.prepareStatement(CREATE_USER);
      pstmt.setString(1, conference_name);
      pstmt.setInt(2, programchair_id);
      pstmt.setInt(3, conference_date.getDate());
      pstmt.setInt(4, author_deadline.getDate());
      pstmt.setInt(5, review_deadline.getDate());
      pstmt.setInt(6, subprogram_deadline.getDate());
      pstmt.executeUpdate();
      pstmt.close();

      PreparedStatement st =
          conn.prepareStatement("SELECT * FROM CONFERENCE WHERE CONFERENCE_ID = (SELECT MAX(CONFERENCE_ID) FROM CONFERENCE)");
      ResultSet rd = st.executeQuery();

      if (rd.next()) {
        conf_id = rd.getInt(1);
        if (!my_conference.containsKey(conf_id)) {
          my_conference.put(rd.getInt("CONFERENCE_ID"),
                            new Conference(rd.getInt("CONFERENCE_ID"), rd
                                .getString("CONFERENCE_NAME"), rd.getInt("PROGRAMCHAIR_ID"),
                                           rd.getInt("CONFERENCE_DATE"), rd
                                               .getInt("AUTHOR_DEADLINE"), rd
                                               .getInt("REVIEWER_DEADLINE"), rd
                                               .getInt("SUBPROGRAM_DEADLINE")));
        }
      }

      st.close();
      rd.close();
      conn.close();

      ConferenceUserDB.addUser(conf_id, new User(0, "", ""), programchair_id,
                               ConferenceRole.PROGRAM_CHAIR);

      return conf_id;
    } catch (Exception e) {
    }
    return conf_id;
  }

  /**
   * query a conference
   * 
   * @param conference_id the id of a conference
   * @param column the column in the conference
   * @return the object found in the column of the conference
   */
  private static Object query(int conference_id, String column) {
    Object obj = null;
    try {

      Connection conn = getConnection();

      PreparedStatement st =
          conn.prepareStatement("SELECT " + column +
                                " FROM CONFERENCE WHERE CONFERENCE_ID = ?");
      st.setInt(1, conference_id);
      ResultSet rd = st.executeQuery();
      if (rd.next()) {
        obj = rd.getObject(column);
      }

      st.close();
      rd.close();
      conn.close();
      return obj;
    } catch (Exception e) {
      return e.getMessage();
    }
  }

  /**
   * Gets the program chair of a particular conference
   * 
   * @param conference_id the id of the conference
   * @return the User that is PC in the conference
   */
  public static User getProgramChair(int conference_id) {
    return UserDB.getUser((int) query(conference_id, "PROGRAMCHAIR_ID"));
  }

  /**
   * gets the conference name
   * 
   * @param conference_id the conference id
   * @return the name of the conference
   */
  public static String getConferenceName(int conference_id) {
    return String.valueOf(query(conference_id, "CONFERENCE_NAME"));
  }

  /**
   * gets the date of the conference
   * 
   * @param conference_id the id of the conference
   * @return the date of the conference
   */
  public static Date getConferenceDate(int conference_id) {
    int date = (int) query(conference_id, "CONFERENCE_DATE");
    return new Date((date % 10000) / 100, date % 100, date / 10000);
  }

  /**
   * get the author deadline of the conference
   * 
   * @param conference_id the id of the conference
   * @return the author's deadline
   */
  public static Date getAuthorDeadline(int conference_id) {
    int date = (int) query(conference_id, "AUTHOR_DEADLINE");
    return new Date((date % 10000) / 100, date % 100, date / 10000);
  }

  /**
   * get the reviewer deadline of the conference
   * 
   * @param conference_id the id of the conference
   * @return the reviewer's deadline
   */
  public static Date getReviewerDeadline(int conference_id) {
    int date = (int) query(conference_id, "REVIEWER_DEADLINE");
    return new Date((date % 10000) / 100, date % 100, date / 10000);
  }

  /**
   * get the subPC deadline of the conference
   * 
   * @param conference_id the id of the conference
   * @return the subPC's deadline
   */
  public static Date getSubProgramDeadline(int conference_id) {
    int date = (int) query(conference_id, "SUBPROGRAM_DEADLINE");
    return new Date((date % 10000) / 100, date % 100, date / 10000);
  }

  /**
   * gets the conference
   * 
   * @param conference_id the id of the conference
   * @return the conference
   */
  public static Conference getConference(int conference_id) {
    return my_conference.get(conference_id);
  }



  /**
   * get the map of conferences
   * 
   * @return the map of conferences
   */
  private static Map<Integer, Conference> getConferences() {
    Map<Integer, Conference> conferences = new HashMap<>();
    try {
      Connection conn = getConnection();
      // conn.createStatement().executeUpdate(create);

      PreparedStatement st;
      st = conn.prepareStatement("SELECT * FROM CONFERENCE");
      ResultSet rd = st.executeQuery();
      while (rd.next()) {
        if (!conferences.containsKey(rd.getInt("CONFERENCE_ID")))
          conferences.put(rd.getInt("CONFERENCE_ID"),
                          new Conference(rd.getInt("CONFERENCE_ID"), rd
                              .getString("CONFERENCE_NAME"), rd.getInt("PROGRAMCHAIR_ID"), rd
                              .getInt("CONFERENCE_DATE"), rd.getInt("AUTHOR_DEADLINE"), rd
                              .getInt("REVIEWER_DEADLINE"), rd.getInt("SUBPROGRAM_DEADLINE")));
      }
      rd.close();
      st.close();
      conn.close();
      
    } catch (SQLException e) {
    }
    return conferences;
  }

  /**
   * get the list of conferences
   * 
   * @return the list of conferences
   */
  public static Collection<Conference> getConferenceList() {
    return my_conference.values();
  }

}
