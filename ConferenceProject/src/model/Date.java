
package model;

/**
 * A personalized class to express calendar dates.
 * 
 * @author Josef Nosov
 * 
 */
public class Date {

  /**
   * The month portion of the date.
   */
  private int month;
  /**
   * the day portion of the date.
   */
  private int day;
  /**
   * the year portion of the date.
   */
  private int year;
  /**
   * Whether the year is a leap year or not.
   */
  private boolean isLeapyear = false;

  /**
   * represents the varying number of days in a month.
   */
  private static int[] days_in_month = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

  /**
   * contructs a dates from three integer values, each representing a fraction
   * of the date.
   * 
   * @param the_month
   * @param the_day
   * @param the_year
   */
  public Date(int the_month, int the_day, int the_year) {
    if (the_year % 400 == 0 || the_year % 4 == 0) {
      isLeapyear = true;
    } else if (the_year % 100 == 0) {
      isLeapyear = false;
    }

    if (the_month > 12 || the_month < 1) {
      try {
        throw new Exception("Month is out of range.");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    if (the_month == 2 && isLeapyear && (the_day > 29 || the_day < 1)) {
      try {
        throw new Exception("This is not a leap year");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    if (!isLeapyear && the_day > days_in_month[the_month] || the_day < 1) {
      try {
        throw new Exception("Day is out of range.");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    month = the_month;
    day = the_day;
    year = the_year;
  }

  /**
   * gets the integer date
   * 
   * @return the integer date
   */
  public int getDate() {
    return year * 10000 + month * 100 + day; // Dont mess with this... used for
                                             // the DB.
  }

  /**
   * gets the year
   * 
   * @return the year
   */
  public int getYear() {
    return year;

  }

  /**
   * gets the day
   * 
   * @return the day
   */
  public int getDay() {
    return day;

  }

  /**
   * gets the month
   * 
   * @return the month
   */
  public int getMonth() {
    return month;

  }

  /**
   * puts the date into M/D/Year format as a String
   */
  public String toString() {
    return month + "/" + day + "/" + year;
  }

  /**
   * Gets the number of days in a month
   * 
   * @param the_month the month we are looking at
   * @param the_year the year we are looking at
   * @return The number of days in the month
   */
  public static int getDays(int the_month, int the_year) {
    boolean isLeapYear = false;
    if (the_year % 400 == 0 || the_year % 4 == 0) {
      isLeapYear = true;
    } else if (the_year % 100 == 0) {
      isLeapYear = false;
    }
    if (the_month == 2 && isLeapYear) {
      return 29;
    }

    return days_in_month[the_month];

  }

}
