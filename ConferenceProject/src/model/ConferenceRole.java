
package model;

/**
 * ConferenceRole allows users to identify their specific roles in the
 * conference.
 * 
 * @author Josef Nosov
 * @version 05/31/13
 * 
 */
public enum ConferenceRole {
  ADMIN(0), PROGRAM_CHAIR(1), SUBPROGRAM_CHAIR(2), REVIEWER(3), AUTHOR(4), USER(5);

  /**
   * The conference role.
   */
  private int my_role;
  /**
   * the String version of the conference role.
   */
  private StringBuilder toString;

  private ConferenceRole(final int the_status) {
    my_role = the_status;

    String sb = super.toString();
    toString = new StringBuilder();
    for (int i = 0; i < sb.length(); i++)
      if (i == 0 || sb.substring(i - 1, i).equals("_"))
        toString.append(sb.substring(i, i + 1).toUpperCase());
      else if (sb.substring(i, i + 1).equals("_"))
        toString.append(" ");
      else
        toString.append(sb.substring(i, i + 1).toLowerCase());
  }

  /**
   * Checks to see what Role a User has
   * 
   * @param i the role index from the database
   * @return ConferenceRole, the role the user has
   */
  public static ConferenceRole translate(int i) {
    if (i >= 0 && i <= 5) {
      switch (i) {
        case 0:
          return ADMIN;
        case 1:
          return PROGRAM_CHAIR;
        case 2:
          return SUBPROGRAM_CHAIR;
        case 3:
          return REVIEWER;
        case 4:
          return AUTHOR;
        case 5:
          return USER;
      }
    }
    return null;

  }

  /**
   * Sets the role of a User
   * 
   * @param role the role to set the user to
   */
  public void setRole(int role) {
    my_role = role;
  }

  /**
   * gets the role of the user
   * 
   * @return the user's role
   */
  public int getRole() {
    return my_role;
  }

  /**
   * Puts the role into a String and returns it
   */
  public String toString() {
    return toString.toString();

  }

}
