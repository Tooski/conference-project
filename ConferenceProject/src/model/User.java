
package model;

import db.ConferenceDB;
import db.ConferenceUserDB;
import db.FeedbackDB;
import db.FeedbackUserDB;
import db.UserDB;
import java.util.ArrayList;
import java.util.List;
import model.Feedback.Status;

/**
 * The user class handles all functionality associated with every conference
 * role.
 * 
 * @author Josef Nosov and Josh Keaton
 * @version 05/31/13
 */
public class User {

  /**
   * The id of the user.
   */
  protected int my_id;
  
  /**
   * The current role.
   */
  private ConferenceRole my_current_role;
  
  /**
   * The user name.
   */
  private String my_username;
  
  /**
   * Actual name.
   */
  private String my_name;

  /**
   * Constructs a new user object.
   * 
   * @param user_id the ID of the new user
   * @param username the username of the new user
   * @param name the actual name of the new user
   */
  public User(final int user_id, final String username, final String name) {
    my_id = user_id;
    my_username = username;
    my_name = name;
  }

  /**
   * This method allows the user to submit a paper to a conference it also sets
   * the user to Author status.
   * 
   * @param the_text the title of the paper
   * @param the_text the contents of the paper itself
   * @return boolean on whether or not creation of the paper was successful
   */
  public int submitPaper(final Conference conf, final String the_text, final String the_paper) {
    if (the_text == null || the_paper == null) {
      return -1;
    }
    ConferenceUserDB.addUser(conf.getID(), new User(0, "", ""), my_id, ConferenceRole.AUTHOR);
    return conf.createFeedback(getActualName(), the_text, the_paper, my_id);
  }

  /**
   * This method is designed to delete a paper submitted by a user who is an
   * author.
   * 
   * @return boolean on whether or not paper was successfully deleted
   */
  public boolean deletePaper(final Feedback the_feedback) {

    if (my_current_role != ConferenceRole.AUTHOR) {
      return false;
    }

    return FeedbackDB.deletePaper(the_feedback.getID());

  }

  /**
   * Sets the current role of the user.
   * 
   * @param conf the conferenceRole to set the user's role to
   */
  public void setCurrentRole(final ConferenceRole conf) {
    my_current_role = conf;
  }

  /**
   * This method allows a user who is an author to edit a paper they have
   * written.
   * 
   * @param the_feedback the feedback object that contains their paper
   * @param new_paper the edited paper they wish to submit
   * @return boolean on whether or not the paper was successfully edited
   */
  public boolean editPaper(final Feedback the_feedback, String new_paper) {

    return the_feedback.editPaper(new_paper);
  }

  /**
   * this method returns the feedback associated with this user.
   * 
   * @return the list of this user's associated feedback
   */
  public List<Feedback> getFeedback() {

    return FeedbackUserDB.getUserFeedback(my_id);
  }


  /**
   * Gets the list of conferences.
   * 
   * @return the list of conferences
   */
  public List<Conference> getListOfConferences() {
    return new ArrayList<Conference>(ConferenceDB.getConferenceList());
  }

  /**
   * get the list of roles for a particular conference.
   * 
   * @param the_conference the conference we want the roles for
   * @return the list of roles for the conference
   */
  public List<ConferenceRole> getRoles(final Conference the_conference) {
    return ConferenceUserDB.getRole(the_conference.getID(), my_id);

  }

  /**
   * gets the ID of the user.
   * 
   * @return the user's ID
   */
  public int getID() {
    return my_id;
  }

  /**
   * gets the username.
   * 
   * @return the username
   */
  public String getUserName() {
    return my_username;
  }

  /**
   * gets the actual name of the user.
   * 
   * @return the user's actual name
   */
  public String getActualName() {
    return my_name;
  }

  /**
   * gets the list of feedback associated with a user.
   * 
   * @param the_user the user to get the feedback from
   * @return the list of feedback associated with the user
   */
  public List<Feedback> getFeedbacksByConference(final Conference conference, 
                                                 final User the_user,
                                                 final ConferenceRole role) {

    if (the_user == null || conference == null) {
      return null;
    }

    return FeedbackUserDB.getFeedbackFromUser(conference.getID(), the_user.getID(),
                                              role.getRole(), my_id);
  }

  /**
   * gets the list of feedback associated with a user/
   * 
   * @param the_user the user to get the feedback from
   * @return the list of feedback associated with the user
   */
  public List<Feedback> getFeedbackByUser(User the_user) {

    if (the_user == null)
      return null;

    return FeedbackUserDB.getUserFeedback(the_user.my_id);
  }

  /**
   * gets the list of feedback associated with a user
   * 
   * @param my_conference
   * 
   * @param the_user the user to get the feedback from
   * @return the list of feedback associated with the user
   */
  public List<Feedback> getFeedbackList(Conference my_conference) {

    return FeedbackUserDB.getFeedbackFromUser(my_conference.getID(), getID(), getCurrentRole()
        .getRole(), my_id);
  }

  /**
   * get the list of all feedbacks for the conference
   * 
   * @param the_conference the list of feedbacks is from
   * @return the entire list of feedbacks for the conference
   */
  public List<Feedback> getAllFeedbackList(Conference the_conference) {
    if (the_conference == null || my_current_role != ConferenceRole.PROGRAM_CHAIR)
      return null;

    return FeedbackDB.getFeedbackList(the_conference.getID());
  }

  /**
   * This method allows a Reviewer to submit a review
   * 
   * @param feedback the contents of the review
   * @param rating
   * @param feedback_index the index of the feedback they are assigned to review
   * @return boolean on whether or not the review was successful
   */
  public boolean submitReview(Feedback feedback, String my_review, int[] rating,
                              Conference conf) {
    List<Feedback> my_feedback = FeedbackUserDB.getUserFeedback(my_id);

    if (feedback != null && my_current_role == ConferenceRole.REVIEWER) {
      boolean contains_feedback = false;
      for (int i = 0; i < my_feedback.size(); i++) {
        if (my_feedback.get(i).equals(feedback)) {
          contains_feedback = true;
        }
      }

      if (feedback == null ||
          my_review == null ||
          !contains_feedback ||
          !FeedbackUserDB.setReview(feedback.getID(), my_id, my_review, rating, conf.getID(),
                                    getCurrentRole().getRole())) {
        return false;
      }
      return FeedbackUserDB.setStatus(feedback.getID(), my_id, Status.REVIEWED_BY_REVIEWER,
                                      conf.getID(), getCurrentRole().getRole());
    }
    return false;
  }

  /**
   * This method allows a Reviewer to edit an existing review
   * 
   * @param the_feedback the feedback that contains this review
   * @param the_new_review the updated review
   * @param review_index the index of the old review
   * @return boolean whether or not the review was updated
   */
  public boolean editReview(Feedback the_feedback, String the_new_review, Conference conf) {
    if (my_current_role == ConferenceRole.REVIEWER) {

      boolean contains_feedback = false;
      List<Feedback> my_feedback = FeedbackUserDB.getUserFeedback(my_id);
      for (int i = 0; i < my_feedback.size(); i++) {
        if (my_feedback.get(i).equals(the_feedback)) {
          contains_feedback = true;
        }
      }
      if (the_feedback == null || the_new_review == null || !contains_feedback) {
        return false;
      }
      return FeedbackUserDB.editReview(the_feedback.getID(), my_id, the_new_review,
                                       conf.getID(), getCurrentRole().getRole());
    }
    return false;

  }


  /**
   * Adds a new user to the conference
   * 
   * @param my_conference the conference the user is joining
   * @param the_user the user
   * @param role the role of the user in this conference
   * @return if the user was added to the conference
   */
  public boolean addUser(Conference my_conference, User the_user, ConferenceRole role) {
    if (my_conference == null || the_user == null) {
      return false;
    }

    if ((my_current_role == ConferenceRole.PROGRAM_CHAIR && role.getRole() > 2) ||
        (my_current_role == ConferenceRole.SUBPROGRAM_CHAIR && role.getRole() > 3))
      return ConferenceUserDB.addUser(my_conference.getID(), UserDB.getUser(my_id),
                                      the_user.getID(), role);

    return false;
  }

  /**
   * Promotes a User
   * 
   * @param my_conference the conference a user will be promoted in
   * @param the_user the user
   * @param role the new role of the user
   * @return if the user was promoted or not
   */
  public boolean promoteUser(Conference my_conference, User the_user, ConferenceRole role) {

    if (my_current_role == ConferenceRole.PROGRAM_CHAIR ||
        my_current_role == ConferenceRole.SUBPROGRAM_CHAIR) {

      if (my_conference == null || the_user == null) {
        return false;
      }
      if ((my_current_role == ConferenceRole.PROGRAM_CHAIR && role.getRole() >= 2) ||
          (my_current_role == ConferenceRole.SUBPROGRAM_CHAIR && role.getRole() >= 3)) {
        return ConferenceUserDB.promoteUser(my_conference.getID(), UserDB.getUser(my_id),
                                            the_user.getID(), role);
      }
    }
    return false;
  }

  /**
   * Assigns a paper to a user
   * 
   * @param conf the conference the paper will be assigned in
   * @param the_feedback the feedback the user will be assigned
   * @param the_user the user
   * @param role the role of the user
   * @return if the user was assigned or not
   */
  public boolean assignPaper(Conference conf, Feedback the_feedback, User the_user,
                             ConferenceRole role) {
    if (the_feedback == null || the_user == null || role == null) {
      return false;
    }

    return FeedbackUserDB.assignFeedback(conf.getID(), the_feedback.getID(), my_id,
                                         getCurrentRole().getRole(), the_user.getID(),
                                         role.getRole());

  }

  /**
   * Sets the status of a feedback
   * 
   * @param the_feedback the feedback to set
   * @param status the new status of the feedback
   * @param conf the conference the feedback is in
   * @return if the status was set
   */
  public boolean setStatus(Feedback the_feedback, Status status, Conference conf) {
    if (the_feedback == null || status == null) {
      return false;
    }
    return FeedbackUserDB.setStatus(the_feedback.getID(), my_id, status, conf.getID(),
                                    getCurrentRole().getRole());
  }

  /**
   * get the list of users associated with a conference
   * 
   * @param my_conference the conference to get users from
   * @return the list of users
   */
  public List<User> getUsers(Conference my_conference) {

    if (my_current_role == ConferenceRole.PROGRAM_CHAIR)

      return ConferenceUserDB.getUserList(my_conference.getID());

    return null;
  }

  /**
   * The username of the user in String format
   */
  public String toString() {
    return getUserName();

  }

  /**
   * get the current role in the conference
   * 
   * @return the current role
   */
  public ConferenceRole getCurrentRole() {
    return my_current_role;
  }

}
