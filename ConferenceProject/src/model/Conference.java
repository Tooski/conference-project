
package model;

import java.util.List;

import db.ConferenceUserDB;
import db.FeedbackDB;
import db.UserDB;

/**
 * 
 * The conference class contains all relevant information regarding a
 * conferences.
 * 
 * @author Josef Nosov and Josh Keaton
 * @version 5/31/13
 */
public class Conference {

  /**
   * The database reference id.
   */
  private int my_conference_id;
  /**
   * the name of the conference.
   */
  private String my_conference_name;
  /**
   * The program chair that manages the conference.
   */
  private User program_chair;
  /**
   * the final closing date of the conference.
   */
  private Date my_conference_date;
  /**
   * the deadline, after which no papers can be submitted.
   */
  private Date my_author_deadline;
  /**
   * the deadline, after which no reviews can be submitted.
   */
  private Date my_review_deadline;
  /**
   * the deadline, after which no papers can be assigned.
   */
  private Date my_subprogram_deadline;

  /**
   * Constructs a conference object
   * 
   * @param conference_id the ID of the conference
   * @param conference_name the name of the conference
   * @param programchair_id the ID of the program chair
   * @param conference_date the final date of the conference
   * @param author_deadline the deadline for authors to submit papers
   * @param review_deadline the deadline for reviewers to submit their reviews
   *          of papers
   * @param subprogram_deadline the deadline for subprogram chairs to recommend
   *          a paper to the PC
   */
  public Conference(int conference_id, String conference_name, int programchair_id,
                    Date conference_date, Date author_deadline, Date review_deadline,
                    Date subprogram_deadline) {
    my_conference_id = conference_id;
    program_chair = UserDB.getUser(programchair_id);
    my_conference_date = conference_date;
    my_author_deadline = author_deadline;
    my_review_deadline = review_deadline;
    my_subprogram_deadline = subprogram_deadline;
  }

  /**
   * Constructs a conference object
   * 
   * @param conference_id the ID of the conference
   * @param conference_name the name of the conference
   * @param programchair_id the ID of the program chair
   * @param conference_date the final date of the conference
   * @param author_deadline the deadline for authors to submit papers
   * @param review_deadline the deadline for reviewers to submit their reviews
   *          of papers
   * @param subprogram_deadline the deadline for subprogram chairs to recommend
   *          a paper to the PC
   */
  public Conference(int conference_id, String conference_name, int programchair_id,
                    int conference_date, int author_deadline, int review_deadline,
                    int subprogram_deadline) {
    my_conference_id = conference_id;
    program_chair = UserDB.getUser(programchair_id);
    my_conference_name = conference_name;
    my_conference_date =
        new Date((conference_date % 10000) / 100, conference_date % 100,
                 conference_date / 10000);
    my_author_deadline =
        new Date((author_deadline % 10000) / 100, author_deadline % 100,
                 author_deadline / 10000);
    my_review_deadline =
        new Date((review_deadline % 10000) / 100, review_deadline % 100,
                 review_deadline / 10000);
    my_subprogram_deadline =
        new Date((subprogram_deadline % 10000) / 100, subprogram_deadline % 100,
                 subprogram_deadline / 10000);
  }

  /**
   * Gets the list of users
   * 
   * @return the list of users
   */
  public List<User> getUsers() {
    return ConferenceUserDB.getUserList(my_conference_id);
  }

  /**
   * get the list of feedback objects for the conference
   * 
   * @return the list of feedback objects for this conference
   */
  public List<Feedback> getFeedbackList() {
    return FeedbackDB.getFeedbackList(my_conference_id);
  }

  /**
   * Creates a feedback object for this conference
   * 
   * @param title the title of the paper the feedback is being created for
   * @param author the author of the feedback
   * @param the_paper the actual paper
   * @return the index of this new feedback, should be stored somewhere in
   *         author
   */
  public int createFeedback(String title, String author, String the_paper, int author_id) {
    if (title == null || author == null || the_paper == null) {
      return -1; // null data passed in
    }
    FeedbackDB
        .createFeedback(my_conference_id, title, author, the_paper, author_id, author_id);
    return getFeedbackList().size() - 1;
  }

  /**
   * get the conference date
   * 
   * @return the date of the conference
   */
  public Date getConferenceDate() {
    return my_conference_date;
  }

  /**
   * get the author's deadline to submit papers by
   * 
   * @return the author's deadline
   */
  public Date getAuthorDeadline() {
    return my_author_deadline;
  }

  /**
   * get the deadline for SubPC's to recommend papers
   * 
   * @return the SubPC's deadline
   */
  public Date getSubProgramDeadline() {
    return my_subprogram_deadline;
  }

  /**
   * get the deadline for Reviewers to submit reviews
   * 
   * @return the review deadline
   */
  public Date getReviewerDeadline() {
    return my_review_deadline;
  }

  /**
   * get the name of the conference
   * 
   * @return the conference's name
   */
  public String getConferenceName() {
    return toString();
  }

  /**
   * get the Program Chair
   * 
   * @return the PC user object
   */
  public User getProgramChair() {
    return program_chair;
  }

  /**
   * get the id of the conference
   * 
   * @return the conference's ID
   */
  public int getID() {
    return my_conference_id;
  }

  /**
   * returns the name of the conference as a String
   */
  public String toString() {
    return my_conference_name;
  }
}
