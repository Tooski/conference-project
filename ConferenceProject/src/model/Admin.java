
package model;

import db.ConferenceDB;
import db.ConferenceUserDB;
import db.UserDB;

/**
 * Admin grants a user admin capabilities, namely creating a conference.
 * 
 * @author Josef Nosov and Josh Keaton
 * @version 05/31/13
 * 
 */
public class Admin extends User {

  /**
   * Creates the Admin User
   * 
   * @param my_id the admin's user ID
   * @param username the username of the admin
   * @param name the actual name of the admin
   */
  public Admin(int my_id, String username, String name) {
    super(my_id, username, name);
  }

  /**
   * This method should only be called by an admin user, it may have to be
   * higher level to insert the new conference into the list in Main
   * 
   * @param the_name the name of the conference
   * @param the_date the date the conference closes
   * @param the_PC the name of the program chair
   * @return boolean value on whether or not the method was successful
   */
  public int createConference(String the_name, User the_PC, Date conference_date,
                              Date author_deadline, Date review_deadline,
                              Date subprogram_deadline) {
    if (the_name != null && conference_date != null && author_deadline != null &&
        review_deadline != null && subprogram_deadline != null && the_PC != null)

    {
      int conf_id =
          ConferenceDB.createConference(the_name, the_PC.getID(), conference_date,
                                        author_deadline, review_deadline, subprogram_deadline);
      if (!ConferenceUserDB.promoteUser(conf_id, UserDB.getUser(my_id), the_PC.getID(),
                                        ConferenceRole.PROGRAM_CHAIR))
        ConferenceUserDB.addUser(conf_id, UserDB.getUser(my_id), the_PC.getID(),
                                 ConferenceRole.PROGRAM_CHAIR);
      return conf_id;
    }

    return -1;
  }
}
