package swing.custom;

import java.awt.Color;
import java.awt.Component;
import java.io.Serializable;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

@SuppressWarnings("serial")
public class JCustomTable<T> extends JTable implements Serializable {

	public JCustomTable(Object[] column, Object[][] row) {

		final DefaultTableModel dtm = new DefaultTableModel();

		for (Object r : column) {

			dtm.addColumn(r);
		}
		for (Object[] r : row) {
			dtm.addRow(r);
		}

		setModel(dtm);
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setRowHeight(20);
		setShowGrid(false);

		setDefaultRenderer(Object.class, new TableCellRenderer() {

			@Override
			public Component getTableCellRendererComponent(JTable table,
					Object value, boolean isSelected, boolean hasFocus,
					int row, int column) {
				JLabel label = new JLabel();
				label.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
				label.setForeground(Color.BLACK);
				label.setOpaque(true);
				label.setText(String.valueOf(value));
				label.setFont(new JDefaultFont().getFont(12));

				if (row % 2 == 1) {
					label.setBackground(new Color(243, 246, 250));
				} else {
					label.setBackground(Color.WHITE);
				}

				if (isSelected) {
					label.setBackground(new Color(56, 117, 215));
					label.setForeground(Color.WHITE);
				}

				return label;
			}

		});
	}

	public boolean isCellEditable(int rowIndex, int colIndex) {
		return false;
	}

}
