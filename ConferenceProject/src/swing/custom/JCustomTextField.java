package swing.custom;

import java.awt.GridLayout;
import java.io.Serializable;

import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Sets the editable and non-editable text to MSEE Conference
 * @author Joe
 * @version 5/30/13
 */
@SuppressWarnings("serial")
public class JCustomTextField extends JTextField implements Serializable {

  
  /**
   * The border panel.
   */
  private final JPanel my_panel;
  
  /**
   * Default constructor.
   */
  public JCustomTextField() {
    this(null, true);
  }

  /**
   * @param the_string the text.
   * @param the_option the editable options.
   */
  public JCustomTextField(final String the_string, final boolean the_option) {
    this(the_string, the_option, 0);
  }

  


  /**
   * Sets up the constructor.
   * @param the_string the text that is passed.
   * @param the_option the editing options.
   * @param the_columns in the text.
   */
  public JCustomTextField(final String the_string, final boolean the_option, 
                      final int the_columns) {
    super(the_string, the_columns);
    my_panel = new JPanel();
    my_panel.setLayout(new GridLayout());

    setFont(new JDefaultFont().getFont(12));

    if (!the_option) {
      setBorder(null);
      setEditable(false);
      setFocusable(false);
    } else {
//      setBorder(new EmptyBorder(1, 2, 0, 2));
//      my_panel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
    }
    my_panel.add(this);
  }

  /**
   * @param the_string what text would you like?
   */
  public JCustomTextField(final String the_string) {
    this(the_string, true);
  }

  /**
   * @param the_columns how many columns would you like for your field?
   */
  public JCustomTextField(final int the_columns) {
    this(null, true, the_columns);
  }
  
  public JCustomTextField(final String the_string, final int the_columns) {
    this(the_string, true, the_columns);
  }

  /**
   * @return returns the text with a border.
   */
  public JPanel getPanel() {
    return my_panel;
  }

}
