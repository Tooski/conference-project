package swing.custom;

import java.io.Serializable;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public class JCustomLabel extends JLabel  implements Serializable{

	public JCustomLabel (String str)
	{
		super(str);
		
		setFont(new JDefaultFont().getFont(12));
	}

	public JCustomLabel() {
		this("");
	}
	
	
}
