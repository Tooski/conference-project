package swing.custom;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.Icon;
import javax.swing.JRadioButton;

@SuppressWarnings("serial")
public class JCustomRadioButton extends JRadioButton {

	public enum RadioStatus {
		EMPTY, SELECTED;
	}

	public JCustomRadioButton() {
		this(null);
	}

	public JCustomRadioButton(String the_string) {
		super(the_string);

		setFont(new JDefaultFont().getFont(12));
		setIcon(new CheckBoxIcon(RadioStatus.EMPTY));
		setSelectedIcon(new CheckBoxIcon(RadioStatus.SELECTED));
		setFocusable(false);
	}

	private class CheckBoxIcon implements Icon {

		RadioStatus my_status;

		public CheckBoxIcon(RadioStatus the_status) {
			my_status = the_status;
		}

		public int getIconHeight() {
			return 14;
		}

		public int getIconWidth() {
			return 14;
		}

		public void paintIcon(Component c, Graphics g, int x, int y) {
			final Graphics2D g2d = (Graphics2D) g;

			int w = getIconWidth();
			int h = getIconHeight();

			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.setColor(Color.WHITE);
			g2d.fillRoundRect(x, y, w, h, 4, 4);
			g2d.setColor(Color.LIGHT_GRAY);
			g2d.drawRoundRect(x, y, w, h, 4, 4);

			g2d.setStroke(new BasicStroke(3, BasicStroke.CAP_BUTT,
					BasicStroke.JOIN_ROUND));
			g2d.setColor(Color.GRAY);

			if (my_status == RadioStatus.SELECTED) {
				g2d.setColor(Color.LIGHT_GRAY);
				g2d.fillRoundRect(x + 2, y + 2, w - 3, h - 3, 4, 4);
				g2d.setStroke(new BasicStroke());

				g2d.setColor(Color.GRAY);
				g2d.drawRoundRect(x + 2, y + 2, w - 4, h - 4, 4, 4);

			}
		}
	}

}
