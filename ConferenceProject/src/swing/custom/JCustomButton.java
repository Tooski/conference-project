package swing.custom;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;
import java.io.Serializable;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonUI;

/**
 * Creates a custom button for MSEE conferences.
 * @author Josef Nosov
 * @version 5/30/13
 *
 */
@SuppressWarnings("serial")
public class JCustomButton extends JButton implements Serializable {

	public JCustomButton(String the_string) {
		super(the_string);
		setFont(new JDefaultFont().getFont(12));
		setUI(new RoundedCornerButtonUI());
		setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		setOpaque(false);
	}

	class RoundedCornerButtonUI extends BasicButtonUI {
		private Shape button;
		private Shape bounds;

		@Override
		public void paint(Graphics g, JComponent component) {
			Graphics2D g2d = (Graphics2D) g;
			AbstractButton a_button = (AbstractButton) component;
			ButtonModel model = a_button.getModel();
			createShape(a_button);
			Color border_color = Color.LIGHT_GRAY;
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
			if (model.isArmed()) {
				
				setForeground(Color.WHITE);
				border_color = new Color(48, 108, 178).darker();
				paintButtons(g2d, component, border_color, new Color(110, 169,
						240).darker(), new Color(71, 132, 203).darker());
			} else if (a_button.isRolloverEnabled() && model.isRollover()) {
				setForeground(Color.WHITE);
				border_color = new Color(48, 108, 178);
				paintButtons(g2d, component, new Color(48, 108, 178),
						new Color(110, 169, 240), new Color(71, 132, 203));
			} else {
				border_color = new Color(195, 195, 195);
				setForeground(Color.BLACK);
				paintButtons(g2d, component, new Color(195, 195, 195),
						new Color(244, 244, 244), new Color(215, 215, 215));
			}

			g2d.setPaint(border_color);
			g2d.setStroke(new BasicStroke(2));
			g2d.draw(button);
			g2d.setColor(component.getBackground());
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_OFF);
			super.paint(g2d, component);
		}

		private void createShape(JComponent component) {
			double arc = 20;
			if (!component.getBounds().equals(bounds)) {
				bounds = component.getBounds();
				button = new RoundRectangle2D.Double(0, 0, component.getWidth() - 1,
						component.getHeight() - 1, arc, arc);
			}
		}

		private void paintButtons(Graphics2D g2, JComponent c,
				Color bottom_color, Color top_color1, Color top_color2) {
			g2.setColor(bottom_color);
			g2.fill(button);
			g2.setPaint(new GradientPaint(0, 0, top_color1, 0,
					c.getHeight() / 2, top_color2, true));
			g2.fill(new RoundRectangle2D.Double(1, 0, c.getWidth() - 2, c
					.getHeight() / 2, 20, 20));

		}
	}

}
