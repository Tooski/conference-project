package swing.custom;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.Serializable;

@SuppressWarnings("serial")
public class JDefaultFont implements Serializable {



	public Font getFont(int size) {
		return getFont(Font.PLAIN, size);
	}

	public Font getFont(int type, int size) {
		Font font = null;
		try {
			font = Font.createFont(type, this.getClass()
					.getResourceAsStream("/assets/MyriadPro-Regular.ttf"));
			GraphicsEnvironment.getLocalGraphicsEnvironment()
					.registerFont(font);
			font = font.deriveFont(type, size);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return font;

	}


}
