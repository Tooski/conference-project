package swing.custom;

import java.awt.Color;
import java.awt.Component;
import java.io.Serializable;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;

@SuppressWarnings("serial")
public class JCustomList<T> extends JList<T> implements Serializable {

	public JCustomList() {
		super(new DefaultListModel<T>());

		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		setCellRenderer(new ListCellRenderer<Object>() {

			@Override
			public Component getListCellRendererComponent(JList<?> arg0,
					Object arg1, int arg2, boolean arg3, boolean arg4) {
				JLabel label = new JLabel();
				label.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 0));
				label.setForeground(Color.BLACK);
				label.setOpaque(true);
				label.setText(String.valueOf(arg1));
				label.setFont(new JDefaultFont().getFont(14));

				if (arg2 % 2 == 1) {
					label.setBackground(new Color(243, 246, 250));
				} else {
					label.setBackground(Color.WHITE);
				}

				if (arg3) {
					label.setBackground(new Color(56, 117, 215));
					label.setForeground(Color.WHITE);
				}

				return label;
			}

		});
	}

	public void addElements(Collection<T> coll) {
		for (T c : coll) {
			((DefaultListModel<T>) getModel()).addElement(c);
		}
	}

	public void addElement(T o) {
		((DefaultListModel<T>) getModel()).addElement(o);

	}

	public boolean isCellEditable(int rowIndex, int colIndex) {
		return false;
	}

}
