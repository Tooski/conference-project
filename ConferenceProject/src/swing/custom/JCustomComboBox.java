package swing.custom;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.Serializable;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicComboBoxUI;



/**
 * Creates a custom Combo Box for the MSEE conference.
 * 
 * @author Josef Nosov
 * @version 05/30/13
 */
@SuppressWarnings({ "serial" })
public class JCustomComboBox<T> extends JComboBox<T> implements Serializable {


	/**
	 * No parameters for this combobox.
	 * 
	 * @param the_combobox
	 *            are the objects being passed.
	 * @param the_default_index
	 *            is the index that will be shown first.
	 * @param the_boolean
	 *            checks to see if editable.
	 */

	@SuppressWarnings("unchecked")
	public JCustomComboBox(final Integer the_default_index, boolean the_boolean) {
		this(the_default_index, the_boolean,  (T[]) new Object[0] );
	}

	@SuppressWarnings("unchecked")
  public JCustomComboBox(final Integer the_default_index,
			boolean the_boolean, T[] t) {
		super(t);
		setLayout(new BorderLayout());

		setUI(new ComboBoxUI());
		setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		setFont(new JDefaultFont().getFont(12));
		setFocusable(the_boolean);
		setEditable(the_boolean);
		UIManager.put("ComboBox.background", new ColorUIResource(Color.WHITE));
		setRenderer(customRender((ListCellRenderer<Object>) getRenderer()));

	}

	public JCustomComboBox() {
		this(0, false);
	}

	public JCustomComboBox(T[] t) {
		this(0, false, t);

	}

	private DefaultListCellRenderer customRender(final ListCellRenderer<Object> render) {
		DefaultListCellRenderer my_renderer = new DefaultListCellRenderer() {
			@Override
			public Component getListCellRendererComponent(JList<?> list,
					Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				Component component = render.getListCellRendererComponent(list,
						value, index, isSelected, cellHasFocus);
				component.setForeground(Color.BLACK);
				component.setFont(new JDefaultFont().getFont(12));

				if (index % 2 == 1) {
					component.setBackground(new Color(243, 246, 250));
				} else {
					component.setBackground(Color.WHITE);
				}

				if (isSelected) {
					component.setBackground(new Color(56, 117, 215));
					component.setForeground(Color.WHITE);
				}

				return component;
			}

		};
		return my_renderer;

	};

	public void setPreferredSize(int value) {
		setPreferredSize(new Dimension(10 * value + 15,
				getPreferredSize().height));
	};

	public void setFontSize(int the_font_size) {
		setFont(new JDefaultFont().getFont(the_font_size));
	}

	/**
	 * Creates a new GUI for the combobox.
	 * 
	 * @author Joe
	 * 
	 */
	private static class ComboBoxUI extends BasicComboBoxUI {

		/**
		 * Creates a button that can move left and right.
		 * 
		 * @param the_component
		 *            is the component being passed.
		 * @return returns the new spinner.
		 */
		public static ComponentUI createUI(final JComponent the_component) {
			return new ComboBoxUI();
		}

		/**
		 * Creates the GUI.
		 * 
		 * @param the_component
		 *            the component that is being passed.
		 */
		@SuppressWarnings("unchecked")
    public void installUI(final JComponent the_component) {
			super.installUI(the_component);
			arrowButton.setBorder(BorderFactory.createLineBorder(null));
			arrowButton.setBackground(Color.WHITE);
			final DefaultListCellRenderer cell_renderer = new DefaultListCellRenderer();
			cell_renderer
					.setHorizontalAlignment(DefaultListCellRenderer.CENTER);
			cell_renderer.setForeground(Color.WHITE);

			comboBox.setRenderer(cell_renderer);
			comboBox.setBorder(new EmptyBorder(1, 2, 0, 2));
			comboBox.setBackground(Color.WHITE);

			listBox.setBorder(new EmptyBorder(0, 2, 0, 2));
			listBox.setBackground(Color.WHITE);
		}

	}
}